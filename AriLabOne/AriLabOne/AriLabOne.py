from tkinter import *
from tkinter import filedialog
import time
from typing import overload
from DroneControl.MovementEnum import movement
from DroneControl.droneControl import droneControl
from threading import Thread
from tkinter import scrolledtext
from djitellopy import tello
import cv2
from threading import Thread
import queue
import numpy as np
import cv2 as cv
import imageProcess.imageProcess as imageProcess
import cv2.typing


import subprocess 
from ctypes import *


#libCalc = CDLL("./visionSourceLibrary/my_functions.so")

#print(libCalc.square(10))
class Application():
    def __init__(self):
        

        self.control = [movement.STAY]
        self.isDroneCreated = False
        self.dronebattery = 0
        
       

        self.root = Tk()
        self.frame = Frame(self.root, width=400, height=700)
        self.frame.pack()
        self.textArea = scrolledtext.ScrolledText(self.root, height=15, width=45)
        self.textArea.bind("<Key>", lambda e: "break")
        self.textArea.place(x=5,y=5)
        self.createButton()
        self.root.mainloop()
        
        

    def createButton(self):
        bottomframe = Frame(self.root)
        bottomframe.place( x=10, y=300, width=390, height=150)
        
        rotateLeft = Button(bottomframe, text = 'rotateLeft', fg ='red', width=10, pady=10)
        rotateLeft.place( x=0, y=0 )
        
        forward = Button(bottomframe, text = 'forward', fg='green', width=10, pady=10)
        forward.place( x=100, y=0 )
        
        rotateRight = Button(bottomframe, text ='rotateRight', fg ='red', width=10, pady=10)
        rotateRight.place( x=200, y=0 )
        
        moveUp = Button(bottomframe, text ='moveUp', fg ='blue', width=10, pady=10)
        moveUp.place( x=300, y=0 )

        moveLeft = Button(bottomframe, text ='moveLeft', fg ='green', width=10, pady=10)
        moveLeft.place( x=0, y=100 )
        
        moveBack = Button(bottomframe, text = 'moveBack', fg='green', width=10, pady=10)
        moveBack.place( x=100, y=100 )
        
        moveRigt = Button(bottomframe, text ='moveRigt', fg ='green', width=10, pady=10)
        moveRigt.place( x=200, y=100 )
        
        moveDown = Button(bottomframe, text ='moveDown', fg ='blue', width=10, pady=10)
        moveDown.place( x=300, y=100 )

        takeOffButton = Button(self.frame, text="Takeoff", fg="black", width=53, pady=5, command=self.takeOffDrone)
        takeOffButton.place(x = 10, y = 660)
        
        testvideosButton = Button(self.frame, text="Test videos", fg="black", width=53, pady=5, command=self.popUpwindowForTestVideos)
        testvideosButton.place(x = 10, y = 510)

        connectButton = Button(self.frame, text="Connect", fg="black", width=53, pady=5, command=self.startDrone)
        connectButton.place(x = 10, y = 540)
                
        visionButton = Button(self.frame, text="Open camera", fg="black", width=53, pady=5, command=self.getCameraVision)
        visionButton.place(x = 10, y = 570)
        
        keyboardButton = Button(self.frame, text="Control by Keyboard", fg="black", width=53, pady=5, command=self.controlByKeyboard)
        keyboardButton.place(x = 10, y = 600)

        landButton = Button(self.frame, text="Land", fg="black", width=53, pady=5, command=self.landDrone)
        landButton.place(x = 10, y = 630)
        
        takeOffButton = Button(self.frame, text="Takeoff", fg="black", width=53, pady=5, command=self.takeOffDrone)
        takeOffButton.place(x = 10, y = 660)








    # ========================================================================
    # =========================== ACTION_LISTENERS_START =====================
    # ========================================================================
    # --------------- Encapsulation of Drone interface

    def startDrone(self):
        self.textArea.insert(END, "Attemting to connect to drone\n")
        if not self.isDroneCreated:
            self.drone = droneControl(self.control, self.textArea)
            self.drone.setBatteryButton(self.dronebattery)
            self.isDroneCreated = True
            self.textArea.insert(END, "Drone connected\n")
            Thread(target=self.drone.movementStart, args=(), daemon=True).start()

    def landDrone(self):
        if self.isDroneCreated:
            self.drone.land()
            
    def takeOffDrone(self):
        if self.isDroneCreated:

            self.drone.takeOff()
        
    def controlByKeyboard(self):
        if self.isDroneCreated:
            Thread(target=self.drone.controlDroneByKey, args=(), daemon=True).start()
            
            
            
    # --------------- Vision
    def getCameraVision(self):
        if self.isDroneCreated:
            Thread(target=self.droneFrames, args=(), daemon=True).start()

    def droneFrames(self):
        self.drone.drone.streamon()

        while True:
            #frame_read = self.drone.drone.get_frame_read()
            #cv.imshow("image", frame_read.frame)
            #time.sleep(0.5)

            img = self.drone.drone.get_frame_read().frame
            img = cv2.resize(img, (800, 620))
            cv2.imshow("results", img)
            cv2.waitKey(1)


    # --------------------------- TEST_VIDEOS_ACTION_LISTENERS_START ---------------------
    # ------------------------------------------------------------------------------------
    def popUpwindowForTestVideos(self):
       popUpwindow= Toplevel(self.frame)
       popUpwindow.geometry("350x400")
       popUpwindow.title("Test environment")
       self.filenamePopUpwindowForTestVideos = ""

       
       openVideoButton = Button(popUpwindow, text = 'Open Video', fg ='red', width=45, pady=5, command=lambda :  self.openVideo())
       openVideoButton.place( x=10, y=10 )

       openCameraButton = Button(popUpwindow, text = 'Open Camera', fg ='red', width=45, pady=5, command=lambda :  self.openCameraVideo())
       openCameraButton.place( x=10, y=40 )
       
       playVideoButton = Button(popUpwindow, text = 'play Video', fg ='red', width=45, pady=5, command=lambda :  Thread(target=self.playVideo(), args=(), daemon=True).start() )
       playVideoButton.place( x=10, y=70 )
       
       playMorphologyVideoButton = Button(popUpwindow, text = 'play Morphology: Hit and Miss', fg ='red', width=45, pady=5, command=lambda :  Thread(target=self.playVideoMorphologyHitAndMiss(), args=(), daemon=True).start() )
       playMorphologyVideoButton.place( x=10, y=100 )
       
       playMorphologyRegionBoundariesButton = Button(popUpwindow, text = 'play Morphology: Region Boundaries', fg ='red', width=45, pady=5, command=lambda :  Thread(target=self.playVideoMorphologyRegionBoundaries(), args=(), daemon=True).start() )
       playMorphologyRegionBoundariesButton.place( x=10, y=130 )
       
       playMorphologySoomthingButton = Button(popUpwindow, text = 'play Morphology: Smoothing', fg ='red', width=45, pady=5, command=lambda :  Thread(target=self.playVideoMorphologySoomthing(), args=(), daemon=True).start() )
       playMorphologySoomthingButton.place( x=10, y=160 )
       
       playMorphologyGradientButton = Button(popUpwindow, text = 'play Morphology: Gradient', fg ='red', width=45, pady=5, command=lambda :  Thread(target=self.playVideoMorphologyGradient(), args=(), daemon=True).start() )
       playMorphologyGradientButton.place( x=10, y=190 )

       playMorphologyGradientButton = Button(popUpwindow, text = 'play No idea: Tools', fg ='red', width=45, pady=5, command=lambda :  Thread(target=self.playVideoNoIdeaTool(), args=(), daemon=True).start() )
       playMorphologyGradientButton.place( x=10, y=220 )

       playVideoDistanceButton = Button(popUpwindow, text = 'play Depth Video: Triang', fg ='red', width=45, pady=5, command=lambda :  Thread(target=self.playVideoDistance(), args=(), daemon=True).start() )
       playVideoDistanceButton.place( x=10, y=250 )
              
       cameraCalibrateButton = Button(popUpwindow, text = 'Calibrate Camera: webcamera', fg ='red', width=45, pady=5, command=lambda :  Thread(target=self.calibrateCameraLive(), args=(), daemon=True).start() )
       cameraCalibrateButton.place( x=10, y=280 )
       
       triangulationButton = Button(popUpwindow, text = 'Triangulation Camera: webcamera', fg ='red', width=45, pady=5, command=lambda :  Thread(target=self.triangulation(), args=(), daemon=True).start() )
       triangulationButton.place( x=10, y=310 )
       



    def openVideo(self):
        self.filenamePopUpwindowForTestVideos = filedialog.askopenfilename(initialdir="/", title="Select a File",filetypes=(("JSON files", "*.*"), ("all files", "*.*")))
        self.textArea.insert(END, "Video loaded: "+self.filenamePopUpwindowForTestVideos+"\n")
        
    def openCameraVideo(self):
        video=cv2.VideoCapture(0)

        while True:
            ret, frame=video.read()
            if ret == True:
                time.sleep(0.1)
                img = cv2.resize(frame, (800, 620))

                cv2.imshow("Camera", img)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

        video.release()

    def playVideo(self):
        self.textArea.insert(END, "Video playing: "+self.filenamePopUpwindowForTestVideos+"\n")
        video=cv2.VideoCapture(self.filenamePopUpwindowForTestVideos)
        while True:
            ret, frame=video.read()
            if ret == True:
                time.sleep(0.05)
                img = cv2.resize(frame, (800, 620))

                cv2.imshow("Video", img)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            else:
                break
        video.release()

    def playVideoMorphologyHitAndMiss(self):
        self.textArea.insert(END, "Hit and Miss Video playing: "+self.filenamePopUpwindowForTestVideos+"\n")
        
        if False:
            video=cv2.VideoCapture(0)
        else:
            video=cv2.VideoCapture(self.filenamePopUpwindowForTestVideos,cv.IMREAD_GRAYSCALE)
        
        objectKernel = np.zeros((5, 5), np.uint8)
        objectKernel[0][2] = 1
        objectKernel[1][2] = 1
        objectKernel[1][3] = 1
        
        backgroundKernel=np.zeros((5, 5), np.uint8)
        backgroundKernel[1][1] = 1
        backgroundKernel[1][2] = 1
        backgroundKernel[2][2] = 1

        objectAnchor = np.array([1, 2])
        backgroundAnchor = np.array([1, 2], np.uint8)


        verticalLinesKernel = np.zeros((5, 1), np.uint8)
        verticalLinesKernel[0,0] = 1
        verticalLinesKernel[4,0] = 1
        verticalLinesAnchor = np.array([0, 0], np.uint8)


        while True:
            ret, frame=video.read()
            if ret == True:
                time.sleep(0.05)
                original = cv2.resize(frame, (800, 620))
                greyImg = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
                


                thresh = imageProcess.hitAndMissTransform(
                    img=greyImg, 
                    objectKernel=verticalLinesKernel, 
                    objectAnchor=verticalLinesAnchor, 
                    backgroundKernel = backgroundKernel, 
                    backgroundAnchor = backgroundAnchor, 
                    iteration = 3
                    )
                
               # thresh = imageProcess.closing(thresh, np.ones((5,5), np.uint8), 1)
                thresh = cv2.resize(thresh, (800, 620))

                thresh = cv2.cvtColor(thresh, cv2.COLOR_GRAY2BGR)
                #cv2.imshow("Video", cv.add(thresh,original))
                cv2.imshow("Video", thresh)

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            else:
                break
        video.release()
        

    def playVideoMorphologyRegionBoundaries(self):
        self.textArea.insert(END, "Region Boundaries Video playing: "+self.filenamePopUpwindowForTestVideos+"\n")
        

        objectKernel = np.zeros((5, 5), np.uint8)
        objectKernel[0][2] = 1
        objectKernel[1][2] = 1
        objectKernel[1][3] = 1
        objectAnchor = np.array([1, 2])
        
        if False:
            video=cv2.VideoCapture(0)
        else:
            video=cv2.VideoCapture(self.filenamePopUpwindowForTestVideos,cv.IMREAD_GRAYSCALE)
        while True:
            ret, frame=video.read()
            if ret == True:
                time.sleep(0.05)
                original = cv2.resize(frame, (800, 620))
                greyImg = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
                
                thresh = cv2.erode(
                    src = greyImg,
                    kernel = objectKernel,
                    iterations = 10, 
                    anchor = objectAnchor
                )


                thresh = cv2.resize(thresh, (800, 620))
                thresh = cv2.cvtColor(thresh, cv2.COLOR_GRAY2BGR)
                
                cv2.imshow("Video", cv.subtract(original ,thresh))
                #cv2.imshow("Video", thresh)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            else:
                break
        video.release()


    def playVideoMorphologySoomthing(self):
        self.textArea.insert(END, "Region Boundaries Video playing: "+self.filenamePopUpwindowForTestVideos+"\n")
        
        
        objectKernel = np.zeros((5, 5), np.uint8)
        objectKernel[0][2] = 1
        objectKernel[1][2] = 1
        objectKernel[1][3] = 1
        objectAnchor = np.array([1, 2])
        

        #objectKernel = np.ones((5, 5), np.uint8)
        #objectKernel = np.empty((5, 5), np.uint8)
        #objectKernel.fill(255)
        objectAnchor = np.array([-1, -1])

        if False:
            video=cv2.VideoCapture(0)
        else:
            video=cv2.VideoCapture(self.filenamePopUpwindowForTestVideos,cv.IMREAD_GRAYSCALE)
        while True:
            ret, frame=video.read()
            if ret == True:
                time.sleep(0.05)
                original = cv2.resize(frame, (800, 620))
                greyImg = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
                
                thresh = imageProcess.smoothing(
                    img = greyImg, 
                    objectKernel = objectKernel, 
                    objectAnchor = objectAnchor, 
                    iteration=4
                    )


                thresh = cv2.resize(thresh, (800, 620))
                #thresh = cv2.cvtColor(thresh, cv2.COLOR_GRAY2BGR)
                
                cv2.imshow("Video", thresh)
                #cv2.imshow("Video", thresh)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            else:
                break
        video.release()
        


    def playVideoMorphologyGradient(self):
        self.textArea.insert(END, "Gradient Video playing: "+self.filenamePopUpwindowForTestVideos+"\n")
        

        objectKernel = np.zeros((5, 5), np.uint8)
        objectKernel[0][2] = 1
        objectKernel[1][2] = 1
        objectKernel[1][3] = 1
        objectAnchor = np.array([1, 2])
        
        if False:
            video=cv2.VideoCapture(0)
        else:
            video=cv2.VideoCapture(self.filenamePopUpwindowForTestVideos,cv.IMREAD_GRAYSCALE)
        while True:
            ret, frame=video.read()
            if ret == True:
                time.sleep(0.05)
                original = cv2.resize(frame, (800, 620))
                greyImg = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
                
                thresh = imageProcess.gradient(
                    img = greyImg, 
                    objectKernel = objectKernel, 
                    objectAnchor = objectAnchor, 
                    iteration=5
                    )

                thresh = cv2.resize(thresh, (800, 620))
                thresh = cv2.cvtColor(thresh, cv2.COLOR_GRAY2BGR)
                
                #cv2.imshow("Video", cv.add(original ,thresh))
                cv2.imshow("Video", thresh)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            else:
                break
        video.release()
        
    def playVideoNoIdeaTool(self):
        self.textArea.insert(END, "Gradient Video playing: "+self.filenamePopUpwindowForTestVideos+"\n")
        
        if False:
            video=cv2.VideoCapture(0)
        else:
            video=cv2.VideoCapture(self.filenamePopUpwindowForTestVideos,cv.IMREAD_GRAYSCALE)
        while True:
            ret, frame=video.read()
            if ret == True:
                time.sleep(0.05)
                original = cv2.resize(frame, (800, 620))
                greyImg = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
                
                ret, thresh = cv.threshold(greyImg, 127, 255, cv2.THRESH_BINARY)
                
                contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
                

                
                cv.drawContours(original, contours, -1, (0,255,0), 3)
                cv2.imshow("Video", original)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            else:
                break
        video.release()



    def playVideoDistance(self):
        self.textArea.insert(END, "Video playing: "+self.filenamePopUpwindowForTestVideos+"\n")
        video=cv2.VideoCapture(self.filenamePopUpwindowForTestVideos)
        #video = cv2.VideoCapture(0)  # Use the appropriate camera index (0 for the default camera)
        objectKernel = np.ones((5, 5), np.uint8)
        objectAnchor = np.array([-1, -1])
        
        while True:
            ret, frameOne = video.read()
            time.sleep(0.05)
            ret, frameTwo = video.read()
            if ret == True:
                frameOne = cv2.resize(frameOne, (800, 620))
                frameTwo = cv2.resize(frameTwo, (800, 620))

                gray1 = cv2.cvtColor(frameOne, cv2.IMREAD_GRAYSCALE)
                gray2 = cv2.cvtColor(frameTwo, cv2.IMREAD_GRAYSCALE)
                stereo = cv2.StereoSGBM_create(numDisparities=16, blockSize=15)
                disparity = stereo.compute(gray1, gray2)
                disparity_normalized = cv2.normalize(disparity, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
                

                #cv2.cvtColor(disparity, cv2.COLOR_GRAY2BGR)
                cv2.imshow('Depth Map',disparity_normalized)





                #cv2.imshow("frameOne", frameOne)
                #cv2.imshow("frameTwo", frameTwo)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            else:
                break
        video.release()
        







    def calibrateCameraLive(self):

        # Define the size of the checkerboard (number of inner corners)
        checkerboard_size = (7, 10)  # Change this to match your checkerboard's dimensions
        # Prepare object points (3D coordinates of checkerboard corners)
        objp = np.zeros((checkerboard_size[0] * checkerboard_size[1], 3), np.float32)
        objp[:, :2] = np.mgrid[0:checkerboard_size[0], 0:checkerboard_size[1]].T.reshape(-1, 2)

        # Lists to store object points and image points from all images
        objpoints = []  # 3D points in the real world
        imgpoints = []  # 2D points in the image plane

        # Initialize the camera capture
        #cap = cv2.VideoCapture(0)  # Use the appropriate camera index (0 for the default camera)
        cap=cv2.VideoCapture(self.filenamePopUpwindowForTestVideos)
        while True:
            # Capture a frame from the camera
            ret, frame = cap.read()
            if not ret:
                break

            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            # Find the chessboard corners
            ret, corners = cv2.findChessboardCorners(gray, checkerboard_size, None, cv.CALIB_CB_ADAPTIVE_THRESH+cv.CALIB_CB_NORMALIZE_IMAGE )

            # If corners are found, add object points and image points
            if ret:
                if(len(imgpoints) < 30 and len(objpoints) < 30):
                    objpoints.append(objp)
                    imgpoints.append(corners)
                else:
                    cv2.putText(frame, 'Wait for calibration...', (10,450), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
                # Draw the corners on the image
                cv2.drawChessboardCorners(frame, checkerboard_size, corners, ret)

            # Display the current frame with detected corners
            cv2.imshow('Camera Calibration', frame)

            # Exit the loop when the 'q' key is pressed
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        # Calibrate the camera
        ret, self.mtx, self.dist, self.rvecs, self.tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

        # Print the camera matrix
        print("Camera Matrix:")
        print(self.mtx)

        # Optionally, save the calibration parameters
        #np.savez('camera_calibration.npz', mtx=mtx, dist=dist, rvecs=rvecs, tvecs=tvecs)

        # Release the camera and close the OpenCV windows
        cap.release()
        cv2.destroyAllWindows()



    def triangulation(self):
        self.textArea.insert(END, "Video playing: "+self.filenamePopUpwindowForTestVideos+"\n")
        video=cv2.VideoCapture(self.filenamePopUpwindowForTestVideos)
        
        #video=cv2.VideoCapture(0)
        
        #width = 800
        #height = 620

        width = 400
        height = 310

        objectKernel = np.ones((15, 15), np.uint8)
        objectAnchor = np.array([-1, -1])
        search_range = 10
        
        random_x = np.random.randint(50, 600, size=50)
        random_y = np.random.randint(50, 500, size=50)

        # Create a list of random points as (x, y) pairs
        random_points = np.array([(x, y) for x, y in zip(random_x, random_y)])

        pointGrid =[]

        steps = 30;
        for x in range(steps, width, steps):
            for y in range(steps, height, steps):
                pointGrid.append((x,y))
        pointGrid = np.array(pointGrid)
        while True:
            ret, frameOne = video.read()
            time.sleep(0.05)
            ret, frameTwo = video.read()
            if ret == True:
                frameOne = cv2.resize(frameOne, (width, height))
                frameTwo = cv2.resize(frameTwo, (width, height))

                gray1 = cv2.cvtColor(frameOne, cv2.COLOR_BGR2GRAY)
                gray2 = cv2.cvtColor(frameTwo, cv2.COLOR_BGR2GRAY)
                # Initialize the feature detector
                # Initialize the feature detector (using ORB)
                orb = cv2.ORB_create()

                # Find keypoints and descriptors
                keypoints1, descriptors1 = orb.detectAndCompute(gray1, None)
                keypoints2, descriptors2 = orb.detectAndCompute(gray2, None)

                # Initialize a matcher (using BFMatcher)
                matcher = cv2.BFMatcher()

                # Match descriptors from the two images
                matches = matcher.knnMatch(descriptors1, descriptors2, k=2)

                # Apply a ratio test to filter out good matches
                good_matches = []
                for m, n in matches:
                    if m.distance < 0.75 * n.distance:
                        good_matches.append(m)

                # Extract the coordinates of the matching points
                matching_points1 = np.float32([keypoints1[m.queryIdx].pt for m in good_matches]).reshape(-1, 2)
                matching_points2 = np.float32([keypoints2[m.trainIdx].pt for m in good_matches]).reshape(-1, 2)

                # Calculate the fundamental matrix
                fundamental_matrix, _ = cv2.findFundamentalMat(matching_points1, matching_points2, cv2.FM_RANSAC)
                print(fundamental_matrix)
                # Calculate the essential matrix
                essential_matrix = np.dot(np.dot(self.mtx.T, fundamental_matrix), self.mtx)

                # Perform SVD to extract R and t from the essential matrix
                U, _, Vt = np.linalg.svd(essential_matrix)

                # Ensure proper determinant for R (should be 1, not -1)
                if np.linalg.det(U) < 0:
                    U *= -1

                if np.linalg.det(Vt) < 0:
                    Vt *= -1

                # Extract the rotation matrix (R) and translation vector (t)
                R = np.dot(U, Vt)
                t = U[:, 2]
                P = np.hstack((R, t.reshape(-1, 1)))
                P = np.vstack((P, np.array([0, 0, 0, 1])))
                #epilines = cv.computeCorrespondEpilines(matching_points1.reshape(-1,1,2), 1,fundamental_matrix)
                

                """
                # Triangulate 3D points
                triangulated_points = triangulate_points(fundamental_matrix, matching_points1, matching_points2, self.mtx, self.mtx, Vt)

                # Calculate the depth map
                depth_map = np.zeros_like(gray1, dtype=np.float32)

                for point in triangulated_points:
                    x, y, z = point
                    # Ensure the point is in front of the camera (positive depth)
                    if z > 0:
                        # Project the 3D point onto the image plane
                        projection = np.dot(self.mtx, point)
                        u, v = int(projection[0] / projection[2]), int(projection[1] / projection[2])
                        # Update the depth map at the corresponding pixel
                        if 0 <= u < depth_map.shape[1] and 0 <= v < depth_map.shape[0]:
                            depth_map[v, u] = z

                # Normalize the depth map for visualization
                depth_map_normalized = cv2.normalize(depth_map, None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)

                # Display the depth map
                cv2.imshow('Depth Map', depth_map_normalized)
                """
                epipolarMatchedPoints = []
                #for pt1 in matching_points1:
                for pt1 in pointGrid:
                    color = tuple(np.random.randint(0,255,3).tolist())

                    pt1 = np.array( [pt1[0], pt1[1]]).astype(int)
                    
                    coefficients = np.dot(fundamental_matrix, np.array( [pt1[0], pt1[1], 1]).astype(int) )

                    


                    # Calculate two points on the line using the explicit form (y = mx + b)
                    epiline_x1 = 0
                    epiline_y1 = int((-coefficients[0] / coefficients[1]) * epiline_x1 - (coefficients[2] / coefficients[1]))

                    epiline_x2 = width - 1
                    epiline_y2 = int((-coefficients[0] / coefficients[1]) * epiline_x2 - (coefficients[2] / coefficients[1]))

                    """
                    for x in range(0,width):
                        y = int((-coefficients[0] / coefficients[1]) * x  - (coefficients[2] / coefficients[1]))
                        if y > 0 and y < height:
                            cv.circle(frameTwo, np.array( [x,y]).astype(int) ,5,color,-1)
                            """

                    # =================== EPIPOLAR LINE POINTS NEED TO BE ITTERATED TO MATCH UP WITH TEMPLATE ===================
                    # Create an array of x values from 0 to width
                    x_values = np.arange(search_range, width-search_range-1)

                    # Calculate the corresponding y values for all x values using vectorized operations
                    y_values = np.round((-coefficients[0] / coefficients[1]) * x_values - (coefficients[2] / coefficients[1])).astype(int)

                    # Filter y values to keep only valid coordinates within the height of the frame
                    valid_indices = np.logical_and(y_values >= search_range, y_values < height - search_range)

                    x_values = x_values[valid_indices]
                    y_values = y_values[valid_indices]

                    # Create an array of points as (x, y) pairs
                    epipolarLinePoints = np.column_stack((x_values, y_values))

                        
                    try:
                        #cv2.line(frameTwo, (epiline_x1, epiline_y1), (epiline_x2, epiline_y2), color, 1)
                        brah = 0
                    except Exception:
                       pass

                    roi_x1 = min(epiline_x1, epiline_x2)
                    roi_y1 = min(epiline_y1, epiline_y2)
                    roi_x2 = max(epiline_x1, epiline_x2)
                    roi_y2 = max(epiline_y1, epiline_y2)

                    # Extract the region of interest (epipolar line segment) in the right image
                    #epipolar_line_segment = frameTwo[roi_y1:roi_y2, roi_x1:roi_x2]


                    roi_left = max(0, pt1[0] - search_range)
                    roi_right = min(frameOne.shape[1], pt1[0] + search_range)
                    # Perform template matching along the epipolar line segment
                    template = frameOne[pt1[1] - search_range : pt1[1] + search_range, pt1[0] - search_range:pt1[0] + search_range]


                    resultMin = 100000
                    match_x = 0
                    match_y = 0
                    for i in range(0,len(y_values),2):
                        epipolarSegment = frameTwo[epipolarLinePoints[i][1] - search_range : epipolarLinePoints[i][1] + search_range, epipolarLinePoints[i][0] - search_range:epipolarLinePoints[i][0] + search_range]
                        #currentMin = np.sum(np.subtract(epipolarSegment, template))
                        
                        """if currentMin < resultMin:
                            resultMin = currentMin
                            match_x = epipolarLinePoints[i][0]
                            match_y = epipolarLinePoints[i][1]
                            """
                        matchedSegment = np.mean((epipolarSegment - template) ** 2)
                        if  resultMin > matchedSegment:
                            resultMin = matchedSegment
                            match_x = epipolarLinePoints[i][0]
                            match_y = epipolarLinePoints[i][1]
                        #match_result = cv2.matchTemplate(frameTwo, template, cv2.TM_CCOEFF_NORMED)
                        
                        

                    
                        #match_result = cv2.matchTemplate(frameTwo, template, cv2.TM_CCOEFF_NORMED)

                        #min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(match_result)

                        #best_match_x = roi_left + max_loc[0]
                    #best_match_y = epiline_y1 + max_loc[1]

                    # Display the best matching point in the right image
                    #cv2.circle(frameOne, (x_values[i], y_values[i]), 5, 255, -1)

                    epipolarMatchedPoints.append(np.array([match_x, match_y]) )
                    frameOne = cv.circle(frameOne, pt1 ,5,color,-1)
                    cv2.circle(frameTwo, (match_x, match_y), 5, 255, -1)
                    


                # Triangulate points to estimate 3D coordinates


                epipolarMatchedPoints = np.array(epipolarMatchedPoints)
                #points_3d_homogeneous = cv2.triangulatePoints(P, P, random_points.T, epipolarMatchedPoints.T)

                # Convert homogeneous coordinates to 3D coordinates
                

                try:
                    #points_3d_homogeneous = cv2.triangulatePoints(P, P, random_points, epipolarMatchedPoints)
                    #points_3d = (points_3d_homogeneous[:3] / points_3d_homogeneous[3]).T 
                    x=0
                except Exception:
                    pass
                points3D = triangulate_points_2(P, self.mtx, pointGrid, epipolarMatchedPoints)
                #cv.triangulatePoints((pointGrid, epipolarMatchedPoints), (P, P), (points3D))
                """
                default_depth = 500  # Choose an appropriate default depth value
                depth_map = np.full((height, width), default_depth, dtype=np.float32)

                # Project 3D points onto the depth map
                for point in points3D:
                    x, y, depth_value = point
                    x, y = abs(int(x * width)), abs(int(y * height))
    
                    # Ensure the point is within the image boundaries
                    if 0 <= x < width and 0 <= y < height:
                        # Update the depth map with the depth value
                        depth_map[y, x] = depth_value*100

                # Normalize the depth map for visualization (adjust scaling as needed)
                min_depth = np.min(depth_map)
                max_depth = np.max(depth_map)
                depth_map_normalized = ((depth_map - min_depth) / (max_depth - min_depth) * 255).astype(np.uint8)

                # Display or save the depth map as an image (optional)
                cv2.imshow('Depth Map', depth_map_normalized)
                """
                # random proba
                #depth_map = np.full((np.min(points3D), np.max(points3D)), 0, dtype=np.float32)
                depth_map = np.full((height, width), 0., dtype=np.float32)
                points3D = points3D.astype(np.float64)
                points3D /= np.linalg.norm(points3D)
                for point in points3D:

                    point_2d, _ = cv2.projectPoints(np.array(point), R, t, self.mtx, self.dist)

                    x_2d, y_2d = point_2d[0][0]
                    _, _, depth_value = point
                    try:
                        depth_map[ int(x_2d), int(y_2d) ] = depth_value
                    except Exception:
                       pass


                min_depth = np.min(depth_map)
                max_depth = np.max(depth_map)
                depth_map_normalized = ((depth_map - min_depth) / (max_depth - min_depth) * 255).astype(np.uint8)

                #depth_map_normalized = depth_map
                depth_map_normalized = cv.dilate(depth_map_normalized, objectKernel, 20 )
                cv2.imshow('Depth Map', depth_map_normalized)
                cv2.imshow("points",frameOne)
                cv2.imshow("epilines",frameTwo)


                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            else:
                break
        video.release()
        cv2.destroyAllWindows()

# Triangulate 3D points using the fundamental matrix and camera matrices
def triangulate_points(F, p1, p2, K1, K2, Vt):

    W = np.array([[0, -1, 0],
                  [1, 0, 0],
                  [0, 0, 1]])
    R1 = np.eye(3)
    R2 = np.dot(np.dot(Vt.T, W), Vt)
    t1 = np.array([0, 0, 0])
    t2 = Vt[:, 2]
    R1t = np.hstack((R1, t1.reshape(3, 1)))
    R2t = np.hstack((R2, t2.reshape(3, 1)))
    
    points_4d = cv2.triangulatePoints(R1t, R2t, p1.T, p2.T)
    points_3d = points_4d[:3] / points_4d[3]
    
    return points_3d.T


def triangulate_points_2(P, K, points_1, points_2):
    M = np.hstack((K, np.array([0,0,0]).reshape(-1, 1))) #points_2
    P = np.dot(M,P)
    points3D = []
    testPoint = []
    for i in range(0,len(points_1), 1):
        
        A = np.array(
            [
            [  points_2[i][0]*M[2][0] - M[0][0]  ,  points_2[i][0]*M[2][1] - M[0][1]  ,   points_2[i][0]*M[2][2] - M[0][2]  ],
            [  points_2[i][1]*M[2][0] - M[1][0]  ,  points_2[i][1]*M[2][1] - M[1][1]  ,   points_2[i][1]*M[2][2] - M[1][2]  ],
            [  points_1[i][0]*P[2][0] - P[0][0]  ,  points_1[i][0]*P[2][1] - P[0][1]  ,   points_1[i][0]*P[2][2] - P[0][2]  ],
            [  points_1[i][1]*P[2][0] - P[1][0]  ,  points_1[i][1]*P[2][1] - P[1][1]  ,   points_1[i][1]*P[2][2] - P[1][2]  ] 
            ]
        )
        b = np.array([
                M[0,3] - M[2,3],
                M[1,3] - M[2,3],
                P[0,3] - P[2,3],
                P[1,3] - P[2,3]
            ]
        )
        x, residuals, rank, singular_values = np.linalg.lstsq(A, b, rcond=None)
        testPoint.append(np.array([points_2[i][0], points_2[i][1], abs(x[2])*100 ]).astype(int)) 
        points3D.append(x)
    
    #return np.array(points3D)
    return np.array(testPoint)

    
    

Application()