import numpy as np
import cv2 as cv


def hitAndMissTransform(img , objectKernel, objectAnchor, backgroundKernel, backgroundAnchor, iteration=1):
    objectImg = cv.erode(
        src = img,
        kernel = objectKernel,
        iterations = iteration, 
        anchor = objectAnchor)
    
    backgroundImg = cv.erode(
        src = cv.bitwise_not(img), 
        kernel = backgroundKernel, 
        iterations = iteration,  
        anchor = backgroundAnchor)
    
    return cv.bitwise_and(objectImg, backgroundImg)


def gradient(img , objectKernel, objectAnchor, iteration=1):
    return cv.subtract(
        cv.dilate(
            src = img,
            kernel = objectKernel,
            anchor= objectAnchor,
            iterations= iteration
            )
        ,
        cv.erode(
            src = img,
            kernel = objectKernel,
            anchor= objectAnchor,
            iterations= iteration
            )
    )    

def smoothing(img , objectKernel, objectAnchor, iteration=1):
    objectImg = cv.dilate(
        src = cv.erode(
            src = img,
            kernel = objectKernel,
            anchor= objectAnchor,
            iterations= iteration
            ),
        kernel = objectKernel,
        anchor= objectAnchor,
        iterations= iteration
        )
    
    objectImg = cv.erode(
        src = cv.dilate(
            src = objectImg,
            kernel = objectKernel,
            anchor= objectAnchor,
            iterations= iteration
            ),
        kernel = objectKernel,
        anchor= objectAnchor,
        iterations= iteration
        )
    return objectImg



def closing(img, kernel, iteration = 1):
    img = cv.dilate(img,kernel,iterations = iteration)
    img = cv.erode(img,kernel,iterations = iteration)
    return img
    
def opening(img, kernel, iteration = 1):
    img = cv.erode(img,kernel,iterations = iteration)
    img = cv.dilate(img,kernel,iterations = iteration)
    return img