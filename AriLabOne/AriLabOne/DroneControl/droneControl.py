from djitellopy import tello
import cv2
from threading import Thread
import queue
from DroneControl.MovementEnum import movement
import tkinter as tk
import time
import keyboard
from time import sleep


class droneControl:

    def __init__(self, control, textArea):
        self.dronebattery = None
        self.textArea = textArea

        self.q = queue.Queue()
        self.drone = tello.Tello()
        self.speed = 15
        self.drone.connect()
        self.drone.set_speed(self.speed)
        self.control = control
        self.isMoving = True
        

        

    def setBatteryButton(self, button):
        self.dronebattery = button

    def movementStart(self): #used to have button as a passed parameter
        try:
            #button['state'] = 'disable'
            self.isMoving = True
            self.drone.streamon()
            #while self.isMoving and self.drone.connect():
            while True:
                self.movement()
                """try:
                    if self.drone.get_battery():
                        self.dronebattery["text"] = "Drone Battery: " + self.drone.get_battery()[0:3]
                        self.textArea.see(tk.END)
                except AttributeError:
                    pass"""
                time.sleep(0.5)
        finally:
            #button['state'] = 'active'
            print("anti identication error print not really part of the software")

    def land(self):
        self.drone.land()
        self.textArea.insert(tk.END, "Drone attempts Landing\n")
        self.textArea.see(tk.END)
        self.isMoving = False

    def takeOff(self):
        self.drone.takeoff()
        self.textArea.insert(tk.END, "Drone attempts Takes Off\n")
        self.textArea.see(tk.END)
        self.isMoving = True

    def movement(self):
        if self.control[0] == movement.ROTATE_LEFT:
            self.drone.send_rc_control(0, 0, 0, -self.speed)

        if self.control[0] == movement.FORWARD:
            self.drone.send_rc_control(0, self.speed, 0, 0)

        if self.control[0] == movement.ROTATE_RIGHT:
            self.drone.send_rc_control(0, 0, 0, self.speed)

        if self.control[0] == movement.LEFT:
            self.drone.send_rc_control(-self.speed, 0, 0, 0)

        if self.control[0] == movement.STAY:
            self.drone.send_rc_control(0, 0, 0, 0)

        if self.control[0] == movement.RIGHT:
            self.drone.send_rc_control(self.speed, 0, 0, 0)

        if self.control[0] == movement.UP:
            self.drone.send_rc_control(0, 0, self.speed, 0)

        if self.control[0] == movement.BACK:
            self.drone.send_rc_control(0, -self.speed, 0, 0)

        if self.control[0] == movement.DOWN:
            self.drone.send_rc_control(0, 0, -self.speed, 0)
        print(self.control)
        # self.drone.send_rc_control()

    def controlDroneByKey(self): #used to have button as a passed parameter
        self.textArea.insert(tk.END, "Keyboard module start\n")
        try:
            #button['state'] = 'disable'
            self.textArea.insert(tk.END,
                                 "Use 'WASD' for control, 'q' for up, 'e' for down and 'f' to stay in position\nTo break control by keyboard press 'l'\n")
            self.textArea.see(tk.END)
            while True:
                sleep(0.2)
                if keyboard.is_pressed('w'):
                    self.control[0] = movement.FORWARD
                if keyboard.is_pressed('d'):
                    self.control[0] = movement.ROTATE_LEFT
                if keyboard.is_pressed('a'):
                    self.control[0] = movement.ROTATE_RIGHT
                if keyboard.is_pressed('s'):
                    self.control[0] = movement.BACK
                if keyboard.is_pressed('q'):
                    self.control[0] = movement.UP
                if keyboard.is_pressed('e'):
                    self.control[0] = movement.DOWN
                if keyboard.is_pressed('f'):
                    self.control[0] = movement.STAY
                if keyboard.is_pressed('l'):
                    break
        finally:
            self.land()
            self.control[0] = movement.STAY
            self.textArea.insert(tk.END, "Keyboard module end\n")
            #button['state'] = 'active'

    def movementForDevelopment(self):
        print(self.control)
        if self.control[0] == movement.ROTATE_LEFT:
            # self.drone.send_rc_control(0,0,0,-self.speed)
            temp = 1
        if self.control[0] == movement.FORWARD:
            # self.drone.send_rc_control(0, self.speed, 0, 0)
            temp = 1
        if self.control[0] == movement.ROTATE_RIGHT:
            # self.drone.send_rc_control(0,0,0,self.speed)
            temp = 1

        if self.control[0] == movement.LEFT:
            # self.drone.send_rc_control(-self.speed,0,0,0)
            temp = 1
        if self.control[0] == movement.STAY:
            # print("staying in position")
            temp = 1
        if self.control[0] == movement.RIGHT:
            # self.drone.send_rc_control(self.speed,0,0,0)
            temp = 1

        if self.control[0] == movement.UP:
            # self.drone.send_rc_control(0, 0, self.speed, 0)
            temp = 1
        if self.control[0] == movement.BACK:
            # self.drone.send_rc_control(0,-self.speed, 0, 0)
            temp = 1
        if self.control[0] == movement.DOWN:
            # self.drone.send_rc_control(0, 0, -self.speed, 0)
            temp = 1

        # self.drone.send_rc_control()
