#include "morph.h"
#include <windows.h>
#include "stdio.h"
#include "cv.h"
#include "highgui.h"

long seed = 132531;

/* Small system random number generator */
double drand32 ()
{
	static long a=16807L, m=2147483647L,
		    q=127773L, r = 2836L;
	long lo, hi, test;

	hi = seed / q;
	lo = seed % q;
	test = a*lo -r*hi;
	if (test>0) seed = test;
	else seed = test + m;

	return (double)seed/(double)m;
}

void srand32 (long k)
{
	seed = k;
}

IplImage *toOpenCV (IMAGE x)
{
	IplImage *img;
	int i=0, j=0;
	CvScalar s;
	
	img = cvCreateImage(cvSize(x->info->nc, x->info->nr),8, 1);
	for (i=0; i<x->info->nr; i++)
	{
		for (j=0; j<x->info->nc; j++)
		{
			s.val[0] = x->data[i][j];
			cvSet2D (img, i,j,s);
		}
	}
	return img;
}

IMAGE fromOpenCV (IplImage *x)
{
	IMAGE img;
	int color=0, i=0;
	int k=0, j=0;
	CvScalar s;
	
	if ((x->depth==IPL_DEPTH_8U) &&(x->nChannels==1))								// 1 Pixel (grey) image
		img = newimage (x->height, x->width);
	else if ((x->depth==8) && (x->nChannels==3)) //Color
	{
		color = 1;
		img = newimage (x->height, x->width);
	}
	else return 0;

	for (i=0; i<x->height; i++)
	{
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			if (color) 
			  k = (unsigned char)((s.val[0] + s.val[1] + s.val[2])/3);
			else k = (unsigned char)(s.val[0]);
			img->data[i][j] = k;
		}
	}
	return img;
}

/* Display an image on th escreen */
void display_image (IMAGE x)
{
	IplImage *image = 0;
	char wn[20];
	int i;

	image = toOpenCV (x);
	if (image <= 0) return;

	for (i=0; i<19; i++) wn[i] = (char)((drand32()*26) + 'a');
	wn[19] = '\0';
	cvNamedWindow( wn, CV_WINDOW_AUTOSIZE );
	cvShowImage( wn, image );
	cvWaitKey(0);
	cvReleaseImage( &image );
}

void save_image (IMAGE x, char *name)
{
	IplImage *image = 0;

	image = toOpenCV (x);
	if (image <0) return;

	cvSaveImage( name, image );
	cvReleaseImage( &image );
}

IMAGE get_image (char *name)
{
	IMAGE x=0;
	IplImage *image = 0;

	image = cvLoadImage(name, 0);
	if (image <= 0) return 0;
	x = fromOpenCV (image);
	cvReleaseImage( &image );
	return x;
}

void main (int argc, char *argv[])
{
	SE p;
	IMAGE im;
	int i,j,k;
	char filename[128], sename[128], outname[128], dir[128], x[128];

//	if (argc < 4)
//	{
//	  printf ("BinDil <input> <Structuring element> <output>\n");
//	  exit(1);
//	}

//	GetCurrentDirectoryA( 128L, dir );
	strcpy (dir, "H:\\Documents and Settings\\jp\\Desktop\\EditionTwo\\Chapter 2 Morphology\\CH2");
	printf ("Working in %s\n",  dir);
	printf ("Enter input image file name:         "); scanf ("%s", x);
	sprintf (filename, "%s\\%s", dir, x);			  printf ("Input path name is '%s'", filename);

	printf ("Enter structuring element file name: "); scanf ("%s", x);
	sprintf (sename, "%s\\%s", dir, x);		

	printf ("Enter output filename:               "); scanf ("%s", x);
	sprintf (outname, "%s\\%s", dir, x);		

/* Read the structuring element */
	k = get_se (sename, &p);
	if (!k)
	{
	  printf ("Bad structuring element, file name '%s'.\n", sename);
//	  exit (2);
	} else
	{
		printf ("BinDil: Perform a binary dilation on image '%s'.\n", filename);
		printf ("Structuring element is:\n");
		print_se (p);
	}

/* Read the input image */
	if (read_pbm (filename, &im) == 0) exit(3);

/* Perform the dilation */
	bin_erode (im, p);

/* Write the result to the specified file */
	write_pbm (outname, im);

/* Adjust levels so the image can be displayed */
	for (i=0; i<im->info->nr; i++)
		for (j=0; j<im->info->nc; j++)
			if (im->data[i][j] == 1) im->data[i][j] = 255;
	display_image (im);

}

