/* Stentiford's thinning algorithm */

#include "stdio.h"
#include "cv.h"
#include "highgui.h"
#include <math.h>

/* The image header data structure      */
struct header 
{
	int nr, nc;             /* Rows and columns in the image */
	int oi, oj;             /* Origin */
};

/*      The IMAGE data structure        */
struct image 
{
		struct header *info;            /* Pointer to header */
		unsigned char **data;           /* Pixel values */
};

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

long seed = 132531;
typedef struct image * IMAGE;

struct image  *newimage (int nr, int nc);
void freeimage (struct image  *z);
void srand32 (long k);
double drand32 ();
IplImage *toOpenCV (IMAGE x);
IMAGE fromOpenCV (IplImage *x);
void display_image (IMAGE x);
void save_image (IMAGE x, char *name);
IMAGE get_image (char *name);

#define DEBUG 0

void thnstent (IMAGE im);
int nays8 (IMAGE im, int r, int c);
int Yokoi (IMAGE im, int r, int c);
void pre_smooth (IMAGE im);

void thnstent (IMAGE im)
{
	int i,j, again=1;

/* BLACK = 0, WHITE = 1. */
	for (i=0; i<im->info->nr; i++)
	  for (j=0; j<im->info->nc; j++)
	    if (im->data[i][j] > 0) im->data[i][j] = 1;

/* Mark and delete */
	while (again)
	{
	  again = 0;

/* Matching template M1 - scan top-bottom, left - right */
	  for (i=1; i<im->info->nr-1; i++)
	    for (j=1; j<im->info->nc-1; j++)
	      if (im->data[i][j] == 0)
		if (im->data[i-1][j] == 1 && im->data[i+1][j] != 1)
	          if (nays8(im, i, j) != 1  && Yokoi (im, i, j) == 1)
		    im->data[i][j] = 2;
	  printf ("M1:\n");

/* Template M2 bottom-top, left-right */
	  for (j=1; j<im->info->nc-1; j++)
	    for (i=im->info->nr-2; i>=1; i--)
	      if (im->data[i][j] == 0)
		if (im->data[i][j-1] == 1 && im->data[i][j+1] != 1)
	          if (nays8(im, i, j) != 1  && Yokoi (im, i, j) == 1)
		    im->data[i][j] = 2;
	  printf ("M2:\n");

/* Template M3 right-left, bottom-top */
	  for (i=im->info->nr-2; i>=1; i--)
	    for (j=im->info->nc-2; j>=1; j--)
	      if (im->data[i][j] == 0)
		if (im->data[i-1][j] != 1 && im->data[i+1][j] == 1)
	          if (nays8(im, i, j) != 1  && Yokoi (im, i, j) == 1)
		    im->data[i][j] = 2;
	  printf ("M3:\n");

/* Template M4 */
	  for (j=im->info->nc-2; j>=1; j--)
	    for (i=1; i<im->info->nr-1; i++)
	      if (im->data[i][j] == 0)
		if (im->data[i][j-1] != 1 && im->data[i][j+1] == 1)
	          if (nays8(im, i, j) != 1  && Yokoi (im, i, j) == 1)
		    im->data[i][j] = 2;
	  printf ("M4:\n");

/* Delete pixels that are marked (== 2) */
	  for (i=1; i<im->info->nr-1; i++)
	    for (j=1; j<im->info->nc-1; j++)
	      if (im->data[i][j] == 2)
	      {
	        im->data[i][j] = 1;
	        again = 1;
	      }

	  printf ("Iteration.\n");
	}

	for (i=1; i<im->info->nr-1; i++)
	  for (j=1; j<im->info->nc-1; j++)
	    if (im->data[i][j] > 0) im->data[i][j] = 255;

	/* Display (im); */
}

int nays8 (IMAGE im, int r, int c)
{
	int i,j,k=0;

	for (i=r-1; i<=r+1; i++)
	  for (j=c-1; j<=c+1; j++)
	    if (i!=r || c!=j)
	      if (im->data[i][j] == 0) k++;
	return k;
}

int Yokoi (IMAGE im, int r, int c)
{
	int N[9];
	int i,k, i1, i2;

	N[0] = im->data[r][c]      != 0;
	N[1] = im->data[r][c+1]    != 0;
	N[2] = im->data[r-1][c+1]  != 0;
	N[3] = im->data[r-1][c]    != 0;
	N[4] = im->data[r-1][c-1]  != 0;
	N[5] = im->data[r][c-1]    != 0;
	N[6] = im->data[r+1][c-1]  != 0;
	N[7] = im->data[r+1][c]    != 0;
	N[8] = im->data[r+1][c+1]  != 0;

	k = 0;
	for (i=1; i<=7; i+=2)
	{
	  i1 = i+1; if (i1 > 8) i1 -= 8;
	  i2 = i+2; if (i2 > 8) i2 -= 8;
	  k += (N[i] - N[i]*N[i1]*N[i2]);
	}

	if (DEBUG)
	{
	  printf ("Yokoi: (%d,%d)\n",r, c);
	  printf ("%d %d %d\n", im->data[r-1][c-1], im->data[r-1][c],
			im->data[r-1][c+1]);
	  printf ("%d %d %d\n", im->data[r][c-1], im->data[r][c],
			im->data[r][c+1]);
	  printf ("%d %d %d\n", im->data[r+1][c-1], im->data[r+1][c],
			im->data[r+1][c+1]);
	  for (i=0; i<9; i++) printf ("%2d ", N[i]);
	  printf ("\n");
	  printf ("Y = %d\n", k);
	}

	return k;
}

void pre_smooth (IMAGE im)
{
	int i,j;

	for (i=0; i<im->info->nr; i++)
	  for (j=0; j<im->info->nc; j++)
	    if (im->data[i][j] == 0)
		if (nays8(im, i, j) <= 2 && Yokoi (im, i, j)<2)
		  im->data[i][j] = 2;


	for (i=0; i<im->info->nr; i++)
	  for (j=0; j<im->info->nc; j++)
	    if (im->data[i][j] == 2) im->data[i][j] = 1;
}

void main (int argc, char *argv[])
{
	IMAGE im, data;
	int i,j;
	char filename[128];

	printf ("Thinning. Please enter the input image file name.\n");
	scanf ("%s", filename);

	data = get_image (filename);
	if (data == NULL)
	{
		printf ("Bad input file '%s'\n", filename);
		exit(1);
	}
	display_image (data);


	im = newimage (data->info->nr+2, data->info->nc+2);
	for (i=0; i<data->info->nr; i++)
	  for (j=0; j<data->info->nc; j++)
	    im->data[i+1][j+1] = data->data[i][j];
	for (i=0; i<im->info->nr; i++) 
	{
	  im->data[i][0] = 1;
	  im->data[i][im->info->nc-1] = 1;
	}
	for (j=0; j<im->info->nc; j++)
	{
	  im->data[0][j] = 1;
	  im->data[im->info->nr-1][j] = 1;
	}

	thnstent (im);

	for (i=0; i<data->info->nr; i++)
           for (j=0; j<data->info->nc; j++)
	      data->data[i][j] = im->data[i+1][j+1];


	display_image (data);
	save_image (data, "medialaxis.jpg");
}

/* Small system random number generator */
double drand32 ()
{
	static long a=16807L, m=2147483647L,
		    q=127773L, r = 2836L;
	long lo, hi, test;

	hi = seed / q;
	lo = seed % q;
	test = a*lo -r*hi;
	if (test>0) seed = test;
	else seed = test + m;

	return (double)seed/(double)m;
}

void srand32 (long k)
{
	seed = k;
}

IplImage *toOpenCV (IMAGE x)
{
	IplImage *img;
	int i=0, j=0;
	CvScalar s;
	
	img = cvCreateImage(cvSize(x->info->nc, x->info->nr),8, 1);
	for (i=0; i<x->info->nr; i++)
	{
		for (j=0; j<x->info->nc; j++)
		{
			s.val[0] = x->data[i][j];
			cvSet2D (img, i,j,s);
		}
	}
	return img;
}

IMAGE fromOpenCV (IplImage *x)
{
	IMAGE img;
	int color=0, i=0;
	int k=0, j=0;
	CvScalar s;
	
	if ((x->depth==IPL_DEPTH_8U) &&(x->nChannels==1))								// 1 Pixel (grey) image
		img = newimage (x->height, x->width);
	else if ((x->depth==8) && (x->nChannels==3)) //Color
	{
		color = 1;
		img = newimage (x->height, x->width);
	}
	else return 0;

	for (i=0; i<x->height; i++)
	{
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			if (color) 
			  k = (unsigned char)((s.val[0] + s.val[1] + s.val[2])/3);
			else k = (unsigned char)(s.val[0]);
			img->data[i][j] = k;
		}
	}
	return img;
}

/* Display an image on th escreen */
void display_image (IMAGE x)
{
	IplImage *image = 0;
	char wn[20];
	int i;

	image = toOpenCV (x);
	if (image <= 0) return;

	for (i=0; i<19; i++) wn[i] = (char)((drand32()*26) + 'a');
	wn[19] = '\0';
	cvNamedWindow( wn, CV_WINDOW_AUTOSIZE );
	cvShowImage( wn, image );
	cvWaitKey(0);
	cvReleaseImage( &image );
}

void save_image (IMAGE x, char *name)
{
	IplImage *image = 0;

	image = toOpenCV (x);
	if (image <0) return;

	cvSaveImage( name, image );
	cvReleaseImage( &image );
}

IMAGE get_image (char *name)
{
	IMAGE x=0;
	IplImage *image = 0;

	image = cvLoadImage(name, 0);
	if (image <= 0) return 0;
	x = fromOpenCV (image);
	cvReleaseImage( &image );
	return x;
}

struct image  *newimage (int nr, int nc)
{
	struct image  *x;                /* New image */
	int i;
	unsigned char *p;

	if (nr < 0 || nc < 0) 
	{
		printf ("Error: Bad image size (%d,%d)\n", nr, nc);
		return 0;
	}

/*      Allocate the image structure    */
	x = (struct image  *) malloc( sizeof (struct image) );
	if (!x) {
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}

/*      Allocate and initialize the header      */

	x->info = (struct header *)malloc( sizeof(struct header) );
	if (!(x->info)) 
	{
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}
	x->info->nr = nr;       x->info->nc = nc;
	x->info->oi = x->info->oj = 0;

/*      Allocate the pixel array        */

	x->data = (unsigned char **)malloc(sizeof(unsigned char *)*nr); 

/* Pointers to rows */
	if (!(x->data)) 
	{
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}

	x->data[0] = (unsigned char *)malloc (nr*nc);
	p = x->data[0];
	if (x->data[0]==0)
	  {
		printf ("Out of storage. Newimage  \n");
		exit(1);
	  }

	for (i=1; i<nr; i++) 
	{
	  x->data[i] = (p+i*nc);
	}

	return x;
}

void freeimage (struct image  *z)
{
/*      Free the storage associated with the image Z    */

	if (z != 0) 
	{
	   free (z->data[0]);
	   free (z->info);
	   free (z->data);
	   free (z);
	}
}
