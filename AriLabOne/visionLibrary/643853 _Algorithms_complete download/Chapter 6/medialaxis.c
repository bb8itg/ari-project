/*	Blum medial axis	*/

#include "stdio.h"
#include "cv.h"
#include "highgui.h"
#include <math.h>

#define DISTANCE_4 1
#define DISTANCE_8 2
#define DISTANCE_E 3

/* The image header data structure      */
struct header 
{
	int nr, nc;             /* Rows and columns in the image */
	int oi, oj;             /* Origin */
};

/*      The IMAGE data structure        */
struct image 
{
		struct header *info;            /* Pointer to header */
		unsigned char **data;           /* Pixel values */
};

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

long seed = 132531;
typedef struct image * IMAGE;

#if defined (MAX)
int    PBM_SE_ORIGIN_COL=0, PBM_SE_ORIGIN_ROW=0;
char **arg;
int maxargs;
#else
extern int PBM_SE_ORIGIN_COL, PBM_SE_ORIGIN_ROW;
#endif

int DIST = DISTANCE_E;

struct image  *newimage (int nr, int nc);
void freeimage (struct image  *z);
void srand32 (long k);
double drand32 ();
IplImage *toOpenCV (IMAGE x);
IMAGE fromOpenCV (IplImage *x);
void display_image (IMAGE x);
void save_image (IMAGE x, char *name);
IMAGE get_image (char *name);

float distance_4 (int r1, int c1, int r2, int c2);
float distance_8 (int r1, int c1, int r2, int c2);
float distance_E (int r1, int c1, int r2, int c2);
void dt (IMAGE im);
void thnma (IMAGE im);
int map (IMAGE im, int r, int c);

float distance_4 (int r1, int c1, int r2, int c2)
{
	return (float)(abs(r2-r1) + abs(c2-c1));
}

float distance_8 (int r1, int c1, int r2, int c2)
{
	int a, b;
	
	a = abs(r2-r1);
	b = abs(c2-c1);
	if (a < b) return (float)b;
	else return (float)a;
}

float distance_E (int r1, int c1, int r2, int c2)
{  
	float x;

	x = (float) ( (r2-r1)*(r2-r1) + (c2-c1)*(c2-c1) );
	return (float) sqrt ((double)x);

}

void thnma (IMAGE im)
{
	int i,j;

/* BLACK = 0, WHITE = 255. Mark the boundary. */
	for (i=0; i<im->info->nr; i++)
	  for (j=0; j<im->info->nc; j++)
	    if (im->data[i][j] > 0) im->data[i][j] = 255;

	for (i=1; i<im->info->nr-1; i++)
	  for (j=1; j<im->info->nc-1; j++)
	    if (im->data[i][j] == 0 && (im->data[i-1][j]==255 ||
		im->data[i+1][j]==255 || im->data[i][j-1]==255 ||
		im->data[i][j+1]==255)) im->data[i][j] = 128;

/* Now look at all non boundary pixels, computing the distance to
	the nearest boundary pixel. Keep track of equal distances. */
	for (i=1; i<im->info->nr-1; i++)
	{
	  for (j=1; j<im->info->nc-1; j++)
	    if (im->data[i][j] == 0)
	      im->data[i][j] = (int)(map(im, i, j)+0.5) * 4;
	}

	printf ("Result.\n");
}

int map (IMAGE im, int r, int c)
{
	int i,j, nmind = 0;
	float d, mind;

	mind = (float)(im->info->nr*im->info->nc);
	for (i=1; i<im->info->nr-1; i++)
	  for (j=1; j<im->info->nc-1; j++)
	    if (im->data[i][j] == 128)		/* Boundary pixel */
	    {
	      if (DIST == DISTANCE_4)
	        d = distance_4 (r, c, i, j);
	      else if (DIST == DISTANCE_8)
	        d = distance_8 (r, c, i, j);
	      else if (DIST == DISTANCE_E)
	        d = distance_E (r, c, i, j);

	      if (d < mind)			/* Better distance */
	      {
		mind = d;
		nmind = 1;
	      } 
	    }

	return (int)mind;
}

void dt (IMAGE im)
{
	int i,j,k=1, more=0, ii, jj;
	IMAGE d;

	d = newimage (im->info->nr, im->info->nc);
	for (i=0; i<im->info->nr; i++)
	  for (j=0; j<im->info->nc; j++)
	    d->data[i][j] = 255;

	do {

	more = 0;
	for (i=0; i<im->info->nr; i++)
	  for (j=0; j<im->info->nc; j++)
	  {
	    if (im->data[i][j] == 0)
	    {
	      k = 255;
	      for (ii=i-1; ii<=i+1; ii++)
		for (jj=j-1; jj<=j+1; jj++)
		  if (ii!= i || jj!= j)
		    if (im->data[ii][jj] < k && im->data[ii][jj]>0)
			k = im->data[ii][jj];
	      if (k == 128) { d->data[i][j] = 0; more++; }
		else { d->data[i][j] = k+1; more++; }
	    }
	  }

	for (i=0; i<im->info->nr; i++)
	  for (j=0; j<im->info->nc; j++)
	    im->data[i][j] = d->data[i][j];

	} while (more);

	freeimage (d);
}

void main (int argc, char *argv[])
{
	IMAGE data;
	int i,j;
	char filename[128];
	char c;

	printf ("Thinning. Please enter the input image file name.\n");
	scanf ("%s", filename);

	data = get_image (filename);
	if (data == NULL)
	{
		printf ("Bad input file '%s'\n", filename);
		exit(1);
	}
	display_image (data);

	c = 'e';

	if (c == '4') DIST = DISTANCE_4;
	 else if (c == '8') DIST = DISTANCE_8;
	 else if (c == 'E') DIST = DISTANCE_E;
	 else if (c == 'e') DIST = DISTANCE_E;
	 else {
		printf ("Bad distance measure '%s' specified.\n", c);
		exit (1);
	}

/* Thin */
	thnma (data);  
	for (i=0; i<data->info->nr; i++)
		for (j=0; j<data->info->nc; j++)
			data->data[i][j] *= 10;
	display_image (data);
	save_image (data, "medialaxis.jpg");
}

/* Small system random number generator */
double drand32 ()
{
	static long a=16807L, m=2147483647L,
		    q=127773L, r = 2836L;
	long lo, hi, test;

	hi = seed / q;
	lo = seed % q;
	test = a*lo -r*hi;
	if (test>0) seed = test;
	else seed = test + m;

	return (double)seed/(double)m;
}

void srand32 (long k)
{
	seed = k;
}

IplImage *toOpenCV (IMAGE x)
{
	IplImage *img;
	int i=0, j=0;
	CvScalar s;
	
	img = cvCreateImage(cvSize(x->info->nc, x->info->nr),8, 1);
	for (i=0; i<x->info->nr; i++)
	{
		for (j=0; j<x->info->nc; j++)
		{
			s.val[0] = x->data[i][j];
			cvSet2D (img, i,j,s);
		}
	}
	return img;
}

IMAGE fromOpenCV (IplImage *x)
{
	IMAGE img;
	int color=0, i=0;
	int k=0, j=0;
	CvScalar s;
	
	if ((x->depth==IPL_DEPTH_8U) &&(x->nChannels==1))								// 1 Pixel (grey) image
		img = newimage (x->height, x->width);
	else if ((x->depth==8) && (x->nChannels==3)) //Color
	{
		color = 1;
		img = newimage (x->height, x->width);
	}
	else return 0;

	for (i=0; i<x->height; i++)
	{
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			if (color) 
			  k = (unsigned char)((s.val[0] + s.val[1] + s.val[2])/3);
			else k = (unsigned char)(s.val[0]);
			img->data[i][j] = k;
		}
	}
	return img;
}

/* Display an image on th escreen */
void display_image (IMAGE x)
{
	IplImage *image = 0;
	char wn[20];
	int i;

	image = toOpenCV (x);
	if (image <= 0) return;

	for (i=0; i<19; i++) wn[i] = (char)((drand32()*26) + 'a');
	wn[19] = '\0';
	cvNamedWindow( wn, CV_WINDOW_AUTOSIZE );
	cvShowImage( wn, image );
	cvWaitKey(0);
	cvReleaseImage( &image );
}

void save_image (IMAGE x, char *name)
{
	IplImage *image = 0;

	image = toOpenCV (x);
	if (image <0) return;

	cvSaveImage( name, image );
	cvReleaseImage( &image );
}

IMAGE get_image (char *name)
{
	IMAGE x=0;
	IplImage *image = 0;

	image = cvLoadImage(name, 0);
	if (image <= 0) return 0;
	x = fromOpenCV (image);
	cvReleaseImage( &image );
	return x;
}

struct image  *newimage (int nr, int nc)
{
	struct image  *x;                /* New image */
	int i;
	unsigned char *p;

	if (nr < 0 || nc < 0) 
	{
		printf ("Error: Bad image size (%d,%d)\n", nr, nc);
		return 0;
	}

/*      Allocate the image structure    */
	x = (struct image  *) malloc( sizeof (struct image) );
	if (!x) {
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}

/*      Allocate and initialize the header      */

	x->info = (struct header *)malloc( sizeof(struct header) );
	if (!(x->info)) 
	{
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}
	x->info->nr = nr;       x->info->nc = nc;
	x->info->oi = x->info->oj = 0;

/*      Allocate the pixel array        */

	x->data = (unsigned char **)malloc(sizeof(unsigned char *)*nr); 

/* Pointers to rows */
	if (!(x->data)) 
	{
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}

	x->data[0] = (unsigned char *)malloc (nr*nc);
	p = x->data[0];
	if (x->data[0]==0)
	  {
		printf ("Out of storage. Newimage  \n");
		exit(1);
	  }

	for (i=1; i<nr; i++) 
	{
	  x->data[i] = (p+i*nc);
	}

	return x;
}

void freeimage (struct image  *z)
{
/*      Free the storage associated with the image Z    */

	if (z != 0) 
	{
	   free (z->data[0]);
	   free (z->info);
	   free (z->data);
	   free (z);
	}
}
