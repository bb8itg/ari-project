/* ISEF edge detector */

#include "stdio.h"
#include "cv.h"
#include "highgui.h"

#define OUTLINE 25

/* The image header data structure      */
struct header 
{
	int nr, nc;             /* Rows and columns in the image */
	int oi, oj;             /* Origin */
};

/*      The IMAGE data structure        */
struct image 
{
		struct header *info;            /* Pointer to header */
		unsigned char **data;           /* Pixel values */
};

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

long seed = 132531;
typedef struct image * IMAGE;

#if defined (MAX)
int    PBM_SE_ORIGIN_COL=0, PBM_SE_ORIGIN_ROW=0;
char **arg;
int maxargs;
#else
extern int PBM_SE_ORIGIN_COL, PBM_SE_ORIGIN_ROW;
#endif

int range (IMAGE im, int i, int j);
void print_se (IMAGE p);
IMAGE Input_PBM (char *fn);
IMAGE Output_PBM (IMAGE image, char *filename);
void get_num_pbm (FILE *f, char *b, int *bi, int *res);
void pbm_getln (FILE *f, char *b);
void pbm_param (char *s);
struct image  *newimage (int nr, int nc);
void freeimage (struct image  *z);
void sys_abort (int val, char *mess);
void CopyVarImage (IMAGE *a, IMAGE *b);
void Display (IMAGE x);
float ** f2d (int nr, int nc);
void srand32 (long k);
double drand32 ();
void copy (IMAGE *a, IMAGE b);

void shen(IMAGE im, IMAGE res);
void compute_ISEF (float **x, float **y, int nrows, int ncols);
void apply_ISEF_vertical (float **x, float **y, float **A, float **B, 
			  int nrows, int ncols);
void apply_ISEF_horizontal (float **x, float **y, float **A, float **B, 
			    int nrows, int ncols);
IMAGE compute_bli (float **buff1, float **buff2, int nrows, int ncols);
void locate_zero_crossings (float **orig, float **smoothed, IMAGE bli, 
			    int nrows, int ncols);
void threshold_edges (float **in, IMAGE out, int nrows, int ncols);
int mark_connected (int i, int j,int level);
int is_candidate_edge (IMAGE buff, float **orig, int row, int col);
float compute_adaptive_gradient (IMAGE BLI_buffer, float **orig_buffer, 
				 int row, int col);
void estimate_thresh (double *low, double *hi, int nr, int nc);
void debed (IMAGE im, int width);
void embed (IMAGE im, int width);


/* globals for shen operator*/
double b = 0.9;				/* smoothing factor 0 < b < 1 */
double low_thresh=20, high_thresh=22;	/* threshold for hysteresis */
double ratio = 0.99;
int window_size = 7;
int do_hysteresis = 1;
float **lap;			/* keep track of laplacian of image */
int nr, nc;			/* nrows, ncols */
IMAGE edges;			/* keep track of edge points (thresholded) */
int thinFactor;


void shen (IMAGE im, IMAGE res)
{
	register int i,j;
	float **buffer;	
	float **smoothed_buffer;
	IMAGE bli_buffer;
   
/* Convert the input image to floating point */
	buffer = f2d (im->info->nr, im->info->nc);
	for (i=0; i<im->info->nr; i++)
	  for (j=0; j<im->info->nc; j++)
	    buffer[i][j] = (float)(im->data[i][j]);

/* Smooth input image using recursively implemented ISEF filter */
	smoothed_buffer =  f2d( im->info->nr,  im->info->nc);
	compute_ISEF (buffer, smoothed_buffer, im->info->nr, im->info->nc);
      
/* Compute bli image band-limited laplacian image from smoothed image */
	bli_buffer = compute_bli(smoothed_buffer,
			buffer,im->info->nr,im->info->nc); 
      
/* Perform edge detection using bli and gradient thresholding */
	locate_zero_crossings (buffer, smoothed_buffer, bli_buffer, 
			     im->info->nr, im->info->nc);
      
	free(smoothed_buffer[0]); free(smoothed_buffer);
	freeimage (bli_buffer);
	
	threshold_edges (buffer, res, im->info->nr, im->info->nc);
	for (i=0; i<im->info->nr; i++)
	  for (j=0; j<im->info->nc; j++)
	    if (res->data[i][j] > 0) res->data[i][j] = 0;
	     else res->data[i][j] = 255;
	
	free(buffer[0]); free(buffer);
}

/*	Recursive filter realization of the ISEF 
	(Shen and Castan CVIGP March 1992)	 */
void compute_ISEF (float **x, float **y, int nrows, int ncols)
{
	float **A, **B;
   
	A = f2d(nrows, ncols); /* store causal component */
	B = f2d(nrows, ncols); /* store anti-causal component */
   
/* first apply the filter in the vertical direcion (to the rows) */
	apply_ISEF_vertical (x, y, A, B, nrows, ncols);
   
/* now apply the filter in the horizontal direction (to the columns) and */
/* apply this filter to the results of the previous one */
	apply_ISEF_horizontal (y, y, A, B, nrows, ncols);
   
   /* free up the memory */
	free (B[0]); free(B);
	free (A[0]); free(A);
}

void apply_ISEF_vertical (float **x, float **y, float **A, float **B, 
			  int nrows, int ncols)
{
	register int row, col;
	float b1, b2;
   
	b1 = (1.0 - b)/(1.0 + b);
	b2 = b*b1;
   
/* compute boundary conditions */
	for (col=0; col<ncols; col++)
	{

/* boundary exists for 1st and last column */
	   A[0][col] = b1 * x[0][col];	
	   B[nrows-1][col] = b2 * x[nrows-1][col];
	}
   
/* compute causal component */
	for (row=1; row<nrows; row++)
	  for (col=0; col<ncols; col++)
	    A[row][col] = b1 * x[row][col] + b * A[row-1][col];

/* compute anti-causal component */
	for (row=nrows-2; row>=0; row--)
	  for (col=0; col<ncols; col++)
	    B[row][col] = b2 * x[row][col] + b * B[row+1][col];

/* boundary case for computing output of first filter */
	for (col=0; col<ncols-1; col++)
	  y[nrows-1][col] = A[nrows-1][col]; 

/* now compute the output of the first filter and store in y */
/* this is the sum of the causal and anti-causal components */
	for (row=0; row<nrows-2; row++)
	  for (col=0; col<ncols-1; col++)
	    y[row][col] = A[row][col] + B[row+1][col];
}  


void apply_ISEF_horizontal (float **x, float **y, float **A, float **B, 
			    int nrows, int ncols)
{
	register int row, col;
	float b1, b2;
   
	b1 = (1.0 - b)/(1.0 + b);
	b2 = b*b1;
   
/* compute boundary conditions */
	for (row=0; row<nrows; row++)
	{
	   A[row][0] = b1 * x[row][0];
	   B[row][ncols-1] = b2 * x[row][ncols-1];
	}

/* compute causal component */
	for (col=1; col<ncols; col++)
	  for (row=0; row<nrows; row++)
	    A[row][col] = b1 * x[row][col] + b * A[row][col-1];

/* compute anti-causal component */
	for (col=ncols-2; col>=0; col--)
	  for (row=0; row<nrows;row++)
	    B[row][col] = b2 * x[row][col] + b * B[row][col+1];

/* boundary case for computing output of first filter */     
	for (row=0; row<nrows; row++)
	  y[row][ncols-1] = A[row][ncols-1];

/* now compute the output of the second filter and store in y */
/* this is the sum of the causal and anti-causal components */
	for (row=0; row<nrows; row++)
	  for (col=0; col<ncols-1; col++)
	    y[row][col] = A[row][col] + B[row][col+1];
}  

/* compute the band-limited laplacian of the input image */
IMAGE compute_bli (float **buff1, float **buff2, int nrows, int ncols)
{
	register int row, col;
	IMAGE bli_buffer;
   
	bli_buffer = newimage(nrows, ncols);
	for (row=0; row<nrows; row++)
	  for (col=0; col<ncols; col++)
	    bli_buffer->data[row][col] = 0;
   
/* The bli is computed by taking the difference between the smoothed image */
/* and the original image.  In Shen and Castan's paper this is shown to */
/* approximate the band-limited laplacian of the image.  The bli is then */
/* made by setting all values in the bli to 1 where the laplacian is */
/* positive and 0 otherwise. 						*/
	for (row=0; row<nrows; row++)
	  for (col=0; col<ncols; col++)
	  {
            if (row<OUTLINE || row >= nrows-OUTLINE ||
                  col<OUTLINE || col >= ncols-OUTLINE) continue;
	    bli_buffer->data[row][col] = 
		((buff1[row][col] - buff2[row][col]) > 0.0);
	  }
	return bli_buffer;
}

void locate_zero_crossings (float **orig, float **smoothed, IMAGE bli, 
			    int nrows, int ncols)
{
	register int row, col;
   
	for (row=0; row<nrows; row++)
	{
	   for (col=0; col<ncols; col++)
	   {

/* ignore pixels around the boundary of the image */
	   if (row<OUTLINE || row >= nrows-OUTLINE ||
		 col<OUTLINE || col >= ncols-OUTLINE)
	   {
	     orig[row][col] = 0.0;
	   }

 /* next check if pixel is a zero-crossing of the laplacian  */
	   else if (is_candidate_edge (bli, smoothed, row, col))
	   {

/* now do gradient thresholding  */
	      float grad = compute_adaptive_gradient (bli, smoothed, row, col);
	      orig[row][col] = grad;
	   }
	   else  orig[row][col] = 0.0;		    
	  }
	}
}

void threshold_edges (float **in, IMAGE out, int nrows, int ncols)
{
	register int i, j;
   
	lap = in;
	edges = out;
	nr = nrows;
	nc = ncols;
   
	estimate_thresh (&low_thresh, &high_thresh, nr, nc);
	if (!do_hysteresis)
	  low_thresh = high_thresh;

	for (i=0; i<nrows; i++)
	  for (j=0; j<ncols; j++)
	    edges->data[i][j] = 0;
   
	for (i=0; i<nrows; i++)
	  for (j=0; j<ncols; j++)
	  {
            if (i<OUTLINE || i >= nrows-OUTLINE ||
                  j<OUTLINE || j >= ncols-OUTLINE) continue;

/* only check a contour if it is above high_thresh */
	    if ((lap[i][j]) > high_thresh) 

/* mark all connected points above low thresh */
	      mark_connected (i,j,0);	
	  }
   
	for (i=0; i<nrows; i++)	/* erase all points which were 255 */
	  for (j=0; j<ncols; j++)
	    if (edges->data[i][j] == 255) edges->data[i][j] = 0;
}

/*	return true if it marked something */ 
int mark_connected (int i, int j, int level)
{
	 int notChainEnd;
    
   /* stop if you go off the edge of the image */
	if (i >= nr || i < 0 || j >= nc || j < 0) return 0;
   
   /* stop if the point has already been visited */
	if (edges->data[i][j] != 0) return 0;	
   
   /* stop when you hit an image boundary */
	if (lap[i][j] == 0.0) return 0;
   
	if ((lap[i][j]) > low_thresh)
	{
	   edges->data[i][j] = 1;
	}
	else
	{
	   edges->data[i][j] = 255;
	}
   
	notChainEnd =0;
    
	notChainEnd |= mark_connected(i  ,j+1, level+1);
	notChainEnd |= mark_connected(i  ,j-1, level+1);
	notChainEnd |= mark_connected(i+1,j+1, level+1);
	notChainEnd |= mark_connected(i+1,j  , level+1);
	notChainEnd |= mark_connected(i+1,j-1, level+1);
	notChainEnd |= mark_connected(i-1,j-1, level+1);
	notChainEnd |= mark_connected(i-1,j  , level+1);
	notChainEnd |= mark_connected(i-1,j+1, level+1);

	if (notChainEnd && ( level > 0 ) )
	{
	/* do some contour thinning */
	  if ( thinFactor > 0 )
	  if ( (level%thinFactor) != 0  )
	  {
	    /* delete this point */  
	    edges->data[i][j] = 255;
	  }
	}
    
	return 1;
}

/* finds zero-crossings in laplacian (buff)  orig is the smoothed image */
int is_candidate_edge (IMAGE buff, float **orig, int row, int col)
{
/* test for zero-crossings of laplacian then make sure that zero-crossing */
/* sign correspondence principle is satisfied.  i.e. a positive z-c must */
/* have a positive 1st derivative where positive z-c means the 2nd deriv */
/* goes from positive to negative as we pass through the step edge */
   
	if (buff->data[row][col] == 1 && buff->data[row+1][col] == 0) /* positive z-c */
	{ 
	   if (orig[row+1][col] - orig[row-1][col] > 0) return 1;
	   else return 0;
	}
	else if (buff->data[row][col] == 1 && buff->data[row][col+1] == 0 ) /* positive z-c */
	{
	   if (orig[row][col+1] - orig[row][col-1] > 0) return 1;
	   else return 0;
	}
	else if ( buff->data[row][col] == 1 && buff->data[row-1][col] == 0) /* negative z-c */
	{
	   if (orig[row+1][col] - orig[row-1][col] < 0) return 1;
	   else return 0;
	}
	else if (buff->data[row][col] == 1 && buff->data[row][col-1] == 0 ) /* negative z-c */
	{
	   if (orig[row][col+1] - orig[row][col-1] < 0) return 1;
	   else return 0;
	}
	else			/* not a z-c */
	  return 0;
}

float compute_adaptive_gradient (IMAGE BLI_buffer, float **orig_buffer, 
				 int row, int col)
{
	register int i, j;
	float sum_on, sum_off;
	float avg_on, avg_off;
	int num_on, num_off;
   
	sum_on = sum_off = 0.0;
	num_on = num_off = 0;
   
	for (i= (-window_size/2); i<=(window_size/2); i++)
	{
	   for (j=(-window_size/2); j<=(window_size/2); j++)
	   {
	     if (BLI_buffer->data[row+i][col+j])
	     {
	        sum_on += orig_buffer[row+i][col+j];
	        num_on++;
	     }
	     else
	     {
	        sum_off += orig_buffer[row+i][col+j];
	        num_off++;
	     }
	   }
	}
   
	if (sum_off) avg_off = sum_off / num_off;
	else avg_off = 0;
   
	if (sum_on) avg_on = sum_on / num_on;
	else avg_on = 0;
   
	return (avg_off - avg_on);
}

void estimate_thresh (double *low, double *hi, int nr, int nc)
{
	float vmax, vmin, scale, x;
	int i,j,k, hist[256], count;

/* Build a histogram of the Laplacian image. */
	vmin = vmax = fabs((float)(lap[20][20]));
	for (i=0; i<nr; i++)
	  for (j=0; j<nc; j++)
	  {
            if (i<OUTLINE || i >= nr-OUTLINE ||
                  j<OUTLINE || j >= nc-OUTLINE) continue;
	    x = lap[i][j];
	    if (vmin > x) vmin = x;
	    if (vmax < x) vmax = x;
	  }
	for (k=0; k<256; k++) hist[k] = 0;

	scale = 256.0/(vmax-vmin + 1);

	for (i=0; i<nr; i++)
	  for (j=0; j<nc; j++)
	  {
            if (i<OUTLINE || i >= nr-OUTLINE ||
                  j<OUTLINE || j >= nc-OUTLINE) continue;
	    x = lap[i][j];
	    k = (int)((x - vmin)*scale);
	    hist[k] += 1;
	  }

/* The high threshold should be > 80 or 90% of the pixels */
	k = 255;
	j = (int)(ratio*nr*nc);
	count = hist[255];
	while (count < j)
	{
	  k--;
	  if (k<0) break;
	  count += hist[k];
	}
	*hi = (double)k/scale   + vmin ;
	*low = (*hi)/2;
}

void embed (IMAGE im, int width)
{
	int i,j,I,J;
	IMAGE new;

	width += 2;
	new = newimage (im->info->nr+width+width, im->info->nc+width+width);
	for (i=0; i<new->info->nr; i++)
	  for (j=0; j<new->info->nc; j++)
	  {
	    I = (i-width+im->info->nr)%im->info->nr;
	    J = (j-width+im->info->nc)%im->info->nc;
	    new->data[i][j] = im->data[I][J];
	  }

	free (im->info);
	free(im->data[0]); free(im->data);
	im->info = new->info;
	im->data = new->data;
}

void debed (IMAGE im, int width)
{
	int i,j;
	IMAGE old;

	width +=2;
	old = newimage (im->info->nr-width-width, im->info->nc-width-width);
	for (i=0; i<old->info->nr-1; i++)
	{
	  for (j=1; j<old->info->nc; j++)
	  {
	    old->data[i][j] = im->data[i+width][j+width];
	    old->data[old->info->nr-1][j] = 255;
	  }
	  old->data[i][0] = 255;
	}

	free (im->info);
	free(im->data[0]); free(im->data);
	im->info = old->info;
	im->data = old->data;
}


/*      PRINT_SE - Print a structuring element to the screen    */
void print_se (IMAGE p)
{
	int i,j;

	printf ("\n=====================================================\n");
	if (p == NULL)
	  printf (" Structuring element is NULL.\n");
	else 
	{
	  printf ("Structuring element: %dx%d origin at (%d,%d)\n",
		p->info->nr, p->info->nc, p->info->oi, p->info->oj);
	  for (i=0; i<p->info->nr; i++)
	  {
	    printf ("	");
	    for (j=0; j<p->info->nc; j++)
	      printf ("%4d ", p->data[i][j]);
	    printf ("\n");
	  }
	}
	printf ("\n=====================================================\n");
}

struct image  *newimage (int nr, int nc)
{
	struct image  *x;                /* New image */
	int i;
	unsigned char *p;

	if (nr < 0 || nc < 0) {
		printf ("Error: Bad image size (%d,%d)\n", nr, nc);
		return 0;
	}

/*      Allocate the image structure    */
	x = (struct image  *) malloc( sizeof (struct image) );
	if (!x) {
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}

/*      Allocate and initialize the header      */

	x->info = (struct header *)malloc( sizeof(struct header) );
	if (!(x->info)) 
	{
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}
	x->info->nr = nr;       x->info->nc = nc;
	x->info->oi = x->info->oj = 0;

/*      Allocate the pixel array        */

	x->data = (unsigned char **)malloc(sizeof(unsigned char *)*nr); 

/* Pointers to rows */
	if (!(x->data)) 
	{
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}

	x->data[0] = (unsigned char *)malloc (nr*nc);
	p = x->data[0];
	if (x->data[0]==0)
	  {
		printf ("Out of storage. Newimage  \n");
		exit(1);
	  }

	for (i=1; i<nr; i++) 
	{
	  x->data[i] = (p+i*nc);
	}

	return x;
}

void freeimage (struct image  *z)
{
/*      Free the storage associated with the image Z    */

	if (z != 0) 
	{
	   free (z->data[0]);
	   free (z->info);
	   free (z->data);
	   free (z);
	}
}

void sys_abort (int val, char *mess)
{
	fprintf (stderr, "**** System library ABORT %d: %s ****\n", 
			val, mess);
	exit (2);
}

void copy (IMAGE *a, IMAGE b)
{
	CopyVarImage (a, &b);
}

void CopyVarImage (IMAGE *a, IMAGE *b)
{
	int i,j;

	if (a == b) return;
	if (*a) freeimage (*a);
	*a = newimage ((*b)->info->nr, (*b)->info->nc);
	if (*a == 0) sys_abort (0, "No more storage.\n");

	for (i=0; i<(*b)->info->nr; i++)
	  for (j=0; j< (*b)->info->nc; j++)
	    (*a)->data[i][j] = (*b)->data[i][j];
	(*a)->info->oi = (*b)->info->oi;
	(*a)->info->oj = (*b)->info->oj;
}

float ** f2d (int nr, int nc)
{
	float **x;
	int i;

	x = (float **)calloc ( nr, sizeof (float *) );
	if (x == 0)
	{
	  fprintf (stderr, "Out of storage: F2D.\n");
	  exit (1);
	}

	for (i=0; i<nr; i++)
	{  
	  x[i] = (float *) calloc ( nc, sizeof (float)  );
	  if (x[i] == 0)
	  {
	    fprintf (stderr, "Out of storage: F2D %d.\n", i);
	    exit (1);
	  }
	}
	return x;
}

void free2d (float **x, int nr)
{
	int i;

	for (i=0; i<nr; i++)
		free(x[i]);
	free (x);
}

/* Small system random number generator */
double drand32 ()
{
	static long a=16807L, m=2147483647L,
		    q=127773L, r = 2836L;
	long lo, hi, test;

	hi = seed / q;
	lo = seed % q;
	test = a*lo -r*hi;
	if (test>0) seed = test;
	else seed = test + m;

	return (double)seed/(double)m;
}

void srand32 (long k)
{
	seed = k;
}


IplImage *toOpenCV (IMAGE x)
{
	IplImage *img;
	int i=0, j=0;
	CvScalar s;
	
	img = cvCreateImage(cvSize(x->info->nc, x->info->nr),8, 1);
	for (i=0; i<x->info->nr; i++)
	{
		for (j=0; j<x->info->nc; j++)
		{
			s.val[0] = x->data[i][j];
			cvSet2D (img, i,j,s);
		}
	}
	return img;
}

IMAGE fromOpenCV (IplImage *x)
{
	IMAGE img;
	int color=0, i=0;
	int k=0, j=0;
	CvScalar s;
	
	if ((x->depth==IPL_DEPTH_8U) &&(x->nChannels==1))								// 1 Pixel (grey) image
		img = newimage (x->height, x->width);
	else if ((x->depth==8) && (x->nChannels==3)) //Color
	{
		color = 1;
		img = newimage (x->height, x->width);
	}
	else return 0;

	for (i=0; i<x->height; i++)
	{
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			if (color) 
			  k = (unsigned char)((s.val[0] + s.val[1] + s.val[2])/3);
			else k = (unsigned char)(s.val[0]);
			img->data[i][j] = k;
		}
	}
	return img;
}

/* Display an image on th escreen */
void display_image (IMAGE x)
{
	IplImage *image = 0;
	char wn[20];
	int i;

	image = toOpenCV (x);
	if (image <= 0) return;

	for (i=0; i<19; i++) wn[i] = (char)((drand32()*26) + 'a');
	wn[19] = '\0';
	cvNamedWindow( wn, CV_WINDOW_AUTOSIZE );
	cvShowImage( wn, image );
	cvWaitKey(0);
	cvReleaseImage( &image );
}

void save_image (IMAGE x, char *name)
{
	IplImage *image = 0;

	image = toOpenCV (x);
	if (image <0) return;

	cvSaveImage( name, image );
	cvReleaseImage( &image );
}

IMAGE get_image (char *name)
{
	IMAGE x=0;
	IplImage *image = 0;

	image = cvLoadImage(name, 0);
	if (image <= 0) return 0;
	x = fromOpenCV (image);
	cvReleaseImage( &image );
	return x;
}

IMAGE grab_image ()
{
	CvCapture  *camera = 0;
	IplImage *image = 0;
	IMAGE x;

	camera = cvCaptureFromCAM( CV_CAP_ANY );
	if( !camera )								// Get a camera?
	{
		fprintf(stderr, "Can�t initialize camera\n");
		return 0;
	}
	image = cvQueryFrame( camera );						
	x = fromOpenCV (image);
	cvReleaseCapture( &camera );
	return x;
}

int get_RGB (IMAGE *r, IMAGE *g, IMAGE *b, char *name)
{
	IplImage *image = 0;
	IMAGE i1, i2, i3;
	int i,j;

	// Read the image from a file into Ip1Image
	image = cvLoadImage(name, 1);
	if (image <= 0) return 0;

	// Create three AIPCV images of correct size
	i1 = newimage (image->height, image->width);
	i2 = newimage (image->height, image->width);
	i3 = newimage (image->height, image->width);

	// Copy pixels from Ip1Image to AIPCV images
	for (i=0; i<image->height; i++)
		for (j=0; j<image->width; j++)
		{
		  i1->data[i][j] = (image->imageData+i*image->widthStep)[j*image->nChannels+0];
		  i2->data[i][j] = (image->imageData+i*image->widthStep)[j*image->nChannels+1];
		  i3->data[i][j] = (image->imageData+i*image->widthStep)[j*image->nChannels+2];
		}
	cvReleaseImage(&image);
	*r = i3; 
	*g = i2;
	*b = i1;
	return 1;
}

int save_RGB (IMAGE r, IMAGE g, IMAGE b, char *name)
{
	IplImage *image = 0;
	int i,j,k;

	if ( (r->info->nc != g->info->nc) || (r->info->nr != g->info->nr) ) return 0;
	if ( (r->info->nc != b->info->nc) || (r->info->nr != b->info->nr) ) return 0;

	// Create an  IplImage
	image = cvCreateImage(cvSize(r->info->nc, r->info->nr),IPL_DEPTH_8U,3);
	if (image <= 0) return 0;


	// Copy pixels from AIPCV images into Ip1Image
/*		for (i=0; i<r->info->nr; i++)
		{
			for (j=0; j<r->info->nc; j++)
			{
				s.val[0] = b->data[i][j];
				s.val[1] = g->data[i][j];
				s.val[2] = r->data[i][j];
				cvSet2D (image, i,j,s);
		}
	} */
		
	for (i=0; i<image->height; i++)
		for (j=0; j<image->width; j++)
		{
		  (image->imageData+i*image->widthStep)[j*image->nChannels+0] = b->data[i][j];
		  (image->imageData+i*image->widthStep)[j*image->nChannels+1] = g->data[i][j];
		  (image->imageData+i*image->widthStep)[j*image->nChannels+2] = r->data[i][j];
		} 
	k = cvSaveImage(name, image);
	cvReleaseImage(&image);
	return 1-k;

}

int main ()
{
	IMAGE im, res;
	FILE *params;
	char name[128];

	// Try to read an image
	printf ("Enter path to the image file to be processed: ");
	scanf ("%s", name);
	printf ("Opening file '%s'\n", name);
	im = get_image(name);
	display_image (im);

/* Look for parameter file */
	params = fopen ("shen.par", "r");
	if (params)
	{
	  fscanf (params, "%lf", &ratio);
	  fscanf (params, "%lf", &b);
	  if (b<0) b = 0;
	    else if (b>1.0) b = 1.0;
	  fscanf (params, "%d", &window_size);
	  fscanf (params, "%d", &thinFactor);
	  fscanf (params, "%d", &do_hysteresis);

	  printf ("Parameters:\n");
	  printf (" %% of pixels to be above HIGH threshold: %7.3f\n", ratio);
	  printf (" Size of window for adaptive gradient  :  %3d\n", 
			window_size);
	  printf (" Thinning factor                       : %d\n", thinFactor);
	  printf ("Smoothing factor                       : %7.4f\n", b);
	  if (do_hysteresis) printf ("Hysteresis thresholding turned on.\n");
	    else printf ("Hysteresis thresholding turned off.\n");
	  fclose (params);
	}
	else printf ("Parameter file 'shen.par' does not exist.\n");


	embed (im, OUTLINE);
	res = newimage (im->info->nr, im->info->nc);
	shen (im, res);
	debed (res, OUTLINE);

	display_image (res);
	save_image (res, "shen.jpg");

	return 0;
}