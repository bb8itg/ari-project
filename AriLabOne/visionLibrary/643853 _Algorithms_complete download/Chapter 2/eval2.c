/* Rosenfeld edge evaluation */

#include "stdio.h"
#include "cv.h"
#include "highgui.h"
 
int Nedge = 0;

/* The image header data structure      */
struct header 
{
	int nr, nc;             /* Rows and columns in the image */
	int oi, oj;             /* Origin */
};

/*      The IMAGE data structure        */
struct image 
{
		struct header *info;            /* Pointer to header */
		unsigned char **data;           /* Pixel values */
};

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

long seed = 132531;
typedef struct image * IMAGE;

#if defined (MAX)
int    PBM_SE_ORIGIN_COL=0, PBM_SE_ORIGIN_ROW=0;
char **arg;
int maxargs;
#else
extern int PBM_SE_ORIGIN_COL, PBM_SE_ORIGIN_ROW;
#endif

int range (IMAGE im, int i, int j);
void print_se (IMAGE p);
IMAGE Input_PBM (char *fn);
IMAGE Output_PBM (IMAGE image, char *filename);
void get_num_pbm (FILE *f, char *b, int *bi, int *res);
void pbm_getln (FILE *f, char *b);
void pbm_param (char *s);
struct image  *newimage (int nr, int nc);
void freeimage (struct image  *z);
void sys_abort (int val, char *mess);
void CopyVarImage (IMAGE *a, IMAGE *b);
void Display (IMAGE x);
float ** f2d (int nr, int nc);
void srand32 (long k);
double drand32 ();
void copy (IMAGE *a, IMAGE b);

IMAGE x;
int neighborhood [3][3];
float table[257];
int dtable[257];

double alpha (double a, double b);
void pixdir (int k, int *a, int *b);
double L(int k, int i, int j);
double R(int k, int i, int j);
double C (int i, int j);
double T (int i, int j);
double max3 (double a, double b, double c);
void grad_init ();
int grad_table_index (IMAGE x, int row, int col);
int gdir (IMAGE x, int row, int col);
float gangle (IMAGE x, int row, int col);
void set_neighborhood (int d);


double max3 (double a, double b, double c)
{
	if (a >= b && a >= c) return a;
	if (b>= a && b >= c) return b;
	if (c>=a && c>=b) return c;
}

double alpha (double a, double b)
{
	return fabs ((180.0-fabs(a-b))/180.0);
}

void pixdir (int k, int *a, int *b)
{
	switch (k)
	{
case 0:	*a = 0;   *b =  1; break;
case 1:	*a = -1;  *b =  1; break;
case 2:	*a = -1;  *b =  0; break;
case 3:	*a = -1;  *b = -1; break;
case 4:	*a =  0;  *b = -1; break;
case 5:	*a =  1;  *b = -1; break;
case 6:	*a =  1;  *b =  0; break;
case 7:	*a =  1;  *b =  1; break;
default:	printf ("Bad direction %d. Must be 0-7.\n", k);
		exit(1);
	}
}

double L(int k, int i, int j)
{
	double d, dk, z, y;
	int ii, jj;
	
/* An edge pixel? */
	if (x->data[i][j] == 0) return 0.0;

/* Get directions */
	d = gangle(x, i, j);
	pixdir (k, &ii, &jj);
	ii += i; jj += j;
	if (x->data[ii][jj] == 0) return 0.0;
	dk = gangle (x, ii, jj);
	y = alpha (d, dk);
	z = alpha (k*45.0, d+90.0);	
	return z*y;
}

double R(int k, int i, int j)
{
	double d, dk, z, y;
	int ii, jj;
	
/* An edge pixel? */
	if (x->data[i][j] == 0) return 0.0;

/* Get directions */
	d = gangle(x, i, j);
	pixdir (k, &ii, &jj);
	ii += i; jj += j;
	if (x->data[ii][jj] == 0) return 0.0;
	dk = gangle (x, ii, jj);
	y = alpha (d, dk);
	z = alpha (k*45.0, d-90.0);     
	return z*y;
}

double C (int i, int j)
{
	int ii,jj,k, dd;
	double l1,l2,l3,r1,r2,r3, left, right;

	if (x->data[i][j] == 0) return 0.0;

	for (ii=0; ii<3; ii++)
	  for (jj=0; jj<3; jj++)
	    neighborhood[ii][jj] = 0;
	neighborhood[1][1] = 1;

	dd = gdir(x, i, j);
/* Look right */
	r1 = R((dd+6)%8, i, j);
	r2 = R((dd+5)%8, i, j);
	r3 = R((dd+7)%8, i, j);
	right = max3 (r1, r2, r3);
	if (right == r1) set_neighborhood ((dd+6)%8);
	else if (right == r2) set_neighborhood((dd+5)%8);
	else set_neighborhood ((dd+7)%8);

/* Look left */
	l1 = L((dd+2)%8, i, j);
	l2 = L((dd+3)%8, i, j);
	l3 = L((dd+1)%8, i, j);
	left = max3 (l1, l2, l3);
	if (left == l1) set_neighborhood ((dd+2)%8 );
	else if (left == l2) set_neighborhood ((dd+3)%8 );
	else set_neighborhood ((dd+1)%8 );

	return (right+left)/2.0;
}

void set_neighborhood (int d)
{
	int i, j;

	pixdir (d, &i, &j);
	neighborhood[i+1][j+1] = 1;
}

double T (int i, int j)
{
	int ii, jj, n;
	double v;

	if (x->data[i][j] == 0) return 0.0;
	n = 0;
	for (ii= 0; ii<=2; ii++)
	{
	  for (jj= 0; jj<=2; jj++)
	  {
	    if (neighborhood[ii][jj]) continue;
	    if (x->data[i+ii-1][j+jj-1] > 0) n++;
	  }
	}
	v = (double)(6-n)/6.0;
	return v;
}

float gangle (IMAGE x, int row, int col)
{
	int i;
	float y;

	if (x->data[row][col] == 0) return -1.0;
	i = grad_table_index (x, row, col);
	y =  table[i]-90.0;
	if (y < 0) y += 360.0;
	return y;
}


int gdir (IMAGE x, int row, int col)
{
	int i, j;

	if (x->data[row][col] == 0) return 8;
	i = grad_table_index (x, row, col);
	j = dtable[i] - 2;
	if (j < 0) j += 8;
	return j%8;
}

int grad_table_index (IMAGE x, int row, int col)
{
	int i,j,k,n,m;

/*	Get index to table.					*/

	k = 0; m= 0;
	for (i=row-1; i<=row+1; i++) {
	   for (j=col-1; j<=col+1; j++) {
		if ( (i==row) && (j==col) ) continue; 
		 else if (x->data[i][j] > 0) k += 1<<(m++);
		 else m++;
	   }
	}
	return k;
}

void grad_init ()
{
	int i,j,k;
	double ang, conv, pi;
	double dxtab[256], dytab[256];

	pi = 3.1415926535;
	conv = pi/180.0;
	i=0;
	table[i++] =  0.0;		/* 0 */
	table[i++] =  135.0;
	table[i++] =  90.0;
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] =  0.0;
	table[i++] =  315.0;
	table[i++] =  0.0;
	table[i++] =  0.0;
	table[i++] =  45.0;
	table[i++] =  45.0;		/* 10 */
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] =  0.0;
	table[i++] =  45.0;
	table[i++] =  0.0;
	table[i++] =  315.0;
	table[i++] =  315.0;
	table[i++] =  315.0;
	table[i++] =  315.0;		/* 20 */
	table[i++] =  315.0;
	table[i++] =  315.0;
	table[i++] =  315.0;
	table[i++] =  0.0;
	table[i++] =  0.0;	/* WAS 45 */
	table[i++] =  0.0;
	table[i++] =  45.0;
	table[i++] =  0.0;	/* WAS 315 */
	table[i++] =  0.0;
	table[i++] =  315.0;		/* 30 */
	table[i++] =  0.0;
	table[i++] =  45.0;
	table[i++] =  90.0;
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] =  135.0;
	table[i++] =  45.0;
	table[i++] =  135.0;		/* 40 */
	table[i++] = 135.0;	/* WAS 90 */
	table[i++] =  135.0;
	table[i++] = 45.0;
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] = 270.0;
	table[i++] =  335.0;		/* 50 */
	table[i++] =  45.0;
	table[i++] =  225.0;
	table[i++] = 0.0;
	table[i++] =  0.0;
	table[i++] =  0.0;
	table[i++] =  0.0;	/* WAS 135 */
	table[i++] =  0.0;
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] =  0.0;		/* 60 */
	table[i++] =  45.0;
	table[i++] =  0.0;
	table[i++] =  45.0;
	table[i++] =  90.0;
	table[i++] =  135.0;
	table[i++] =  90.0;		/* 66 */
	table[i++] =  135.0;
	table[i++] =  225.0;
	table[i++] =  180.0;
	table[i++] =  315.0;		/* 70 */
	table[i++] =  90.0;
	table[i++] =  135.0;
	table[i++] =  135.0;
	table[i++] =  90.0;
	table[i++] =  45.0;
	table[i++] =  180.0;
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] =  45.0;
	table[i++] =  225.0;		/* 80*/
	table[i++] =  45.0;
	table[i++] =  270.0;
	table[i++] =  315.0;
	table[i++] =  225.0;
	table[i++] =  315.0;
	table[i++] =  270.0;
	table[i++] =  315.0;
	table[i++] =  180.0;
	table[i++] =  135.0;
	table[i++] =  0.0;		/* 90 */
	table[i++] =  45.0;
	table[i++] =  225.0;
	table[i++] =  180.0;
	table[i++] =  315.0;
	table[i++] = 0.0;
	table[i++] =  135.0;
	table[i++] = 135.0;
	table[i++] =  135.0;
	table[i++] =  90.0;
	table[i++] =  225.0;		/* 100 */
	table[i++] =  45;
	table[i++] =  45;
	table[i++] =  45;
	table[i++] =  135;
	table[i++] =  135;
	table[i++] = 90;
	table[i++] =  90;
	table[i++] =  90;
	table[i++] =  90;
	table[i++] =  90;		/* 110 */
	table[i++] =  45;
	table[i++] =  225;
	table[i++] =  135;
	table[i++] =  225;
	table[i++] = 270.0;
	table[i++] =  225;
	table[i++] =  45;
	table[i++] =  225;
	table[i++] =  270;
	table[i++] = 180;		/* 120 */
	table[i++] =  135;
	table[i++] =  135;
	table[i++] =  90;
	table[i++] =  180;
	table[i++] =  180;
	table[i++] =  225;
	table[i++] =  45;
	table[i++] =  135;
	table[i++] =  135;
	table[i++] =  315;	/* 130 */
	table[i++] =  315;
	table[i++] =  270;
	table[i++] =  315;
	table[i++] =  315;
	table[i++] =  315;
	table[i++] =  135;
	table[i++] =  135;
	table[i++] =  225.0;
	table[i++] =  90;
	table[i++] =  0;	/* 140 */
	table[i++] =  315;
	table[i++] =  315;
	table[i++] =  0;
	table[i++] =  225;
	table[i++] =  315;
	table[i++] =  315;
	table[i++] =  315;
	table[i++] =  270;
	table[i++] =  315;
	table[i++] =  315;	/* 150 */
	table[i++] =  315;
	table[i++] =  0.0;	/* WAS 225 */
	table[i++] =  180;
	table[i++] =  315;
	table[i++] =  0;
	table[i++] =  0;
	table[i++] =  315;
	table[i++] =  315;
	table[i++] =  315;
	table[i++] =  180;	/* 160 */
	table[i++] =  135;
	table[i++] =  0;
	table[i++] =  135;
	table[i++] =  225;
	table[i++] =  0;
	table[i++] =  225;
	table[i++] =  0;
	table[i++] =  135;
	table[i++] =  135;
	table[i++] =  135.0;	/* 170 */
	table[i++] =  90;
	table[i++] =  225;
	table[i++] =  90;
	table[i++] = 225;
	table[i++] =  90;
	table[i++] =  225;
	table[i++] =  135;
	table[i++] = 225;
	table[i++] = 135;
	table[i++] =  225;	/* 180 */
	table[i++] =  270;
	table[i++] =  225;
	table[i++] =  0;
	table[i++] =  180;
	table[i++] =  135;
	table[i++] =  0;
	table[i++] =  0;
	table[i++] =  225;
	table[i++] =  0;
	table[i++] =  0.0;	/* 190 */
	table[i++] =  0;
	table[i++] =  225;
	table[i++] =  135;
	table[i++] =  270;
	table[i++] =  90;
	table[i++] =  225;
	table[i++] =  315;
	table[i++] = 270;
	table[i++] =  90.0;
	table[i++] =  135;	/* 200 */
	table[i++] =  135.0;
	table[i++] =  135;
	table[i++] =  135.0;
	table[i++] =  225;
	table[i++] =  315;
	table[i++] =  90;
	table[i++] =  315;
	table[i++] =  225;
	table[i++] = 180;
	table[i++] =  270;	/* 210 */
	table[i++] =  315;
	table[i++] =  225;
	table[i++] =  315;
	table[i++] =  270;
	table[i++] =  315;
	table[i++] =  180;
	table[i++] =  315;
	table[i++] =  225;
	table[i++] =  315;
	table[i++] =  225;	/* 220 */
	table[i++] =  315;
	table[i++] =  270;
	table[i++] =  315;
	table[i++] =  180.0;
	table[i++] =  135;
	table[i++] =  90;
	table[i++] = 135;
	table[i++] =  225;
	table[i++] =  180;
	table[i++] =  225;	/* 230 */
	table[i++] =  90;
	table[i++] =  135;
	table[i++] =  135;
	table[i++] =  135;
	table[i++] =  135;
	table[i++] =  225;
	table[i++] =  180;
	table[i++] =  225;
	table[i++] =  90;
	table[i++] =  225;	/* 240 */
	table[i++] =  180;
	table[i++] = 225;
	table[i++] =  135;
	table[i++] =  225;
	table[i++] =  270.0;
	table[i++] =  225.0;	/* WAS 270 */
	table[i++] =  270.0;
	table[i++] =  180.0;
	table[i++] =  135.0;
	table[i++] =  180.0;	/* 250 */
	table[i++] =  135.0;
	table[i++] =  225.0;
	table[i++] =  180.0;
	table[i++] =  225.0;
	table[i++] =  0.0;

	for (i=0; i<256; i++) {
	  ang = table[i];

	  if ( (ang<=22.5) || (ang>=337.5) ) dtable[i] = 0; 	  /* 0 sector */
	  else if ( (ang>=22.5) && (ang<=67.5) ) dtable[i] = 1;	  /* 1 sector */
	  else if ( (ang>=67.5) && (ang<=112.5) ) dtable[i] = 2;  /* 2 sector */
	  else if ( (ang>=112.5) && (ang<=157.5) ) dtable[i] = 3; /* 3 sector */
	  else if ( (ang>=157.5) && (ang<=202.5) ) dtable[i] = 4; /* 4 sector */
	  else if ( (ang>=202.5) && (ang<=247.5) ) dtable[i] = 5; /* 5 sector */
	  else if ( (ang>=247.5) && (ang<=292.5) ) dtable[i] = 6; /* 6 sector */
	  else dtable[i] = 7;					  /* 7 sector */
	}
}



/*      PRINT_SE - Print a structuring element to the screen    */
void print_se (IMAGE p)
{
	int i,j;

	printf ("\n=====================================================\n");
	if (p == NULL)
	  printf (" Structuring element is NULL.\n");
	else 
	{
	  printf ("Structuring element: %dx%d origin at (%d,%d)\n",
		p->info->nr, p->info->nc, p->info->oi, p->info->oj);
	  for (i=0; i<p->info->nr; i++)
	  {
	    printf ("	");
	    for (j=0; j<p->info->nc; j++)
	      printf ("%4d ", p->data[i][j]);
	    printf ("\n");
	  }
	}
	printf ("\n=====================================================\n");
}

struct image  *newimage (int nr, int nc)
{
	struct image  *x;                /* New image */
	int i;
	unsigned char *p;

	if (nr < 0 || nc < 0) {
		printf ("Error: Bad image size (%d,%d)\n", nr, nc);
		return 0;
	}

/*      Allocate the image structure    */
	x = (struct image  *) malloc( sizeof (struct image) );
	if (!x) {
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}

/*      Allocate and initialize the header      */

	x->info = (struct header *)malloc( sizeof(struct header) );
	if (!(x->info)) 
	{
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}
	x->info->nr = nr;       x->info->nc = nc;
	x->info->oi = x->info->oj = 0;

/*      Allocate the pixel array        */

	x->data = (unsigned char **)malloc(sizeof(unsigned char *)*nr); 

/* Pointers to rows */
	if (!(x->data)) 
	{
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}

	x->data[0] = (unsigned char *)malloc (nr*nc);
	p = x->data[0];
	if (x->data[0]==0)
	  {
		printf ("Out of storage. Newimage  \n");
		exit(1);
	  }

	for (i=1; i<nr; i++) 
	{
	  x->data[i] = (p+i*nc);
	}

	return x;
}

void freeimage (struct image  *z)
{
/*      Free the storage associated with the image Z    */

	if (z != 0) 
	{
	   free (z->data[0]);
	   free (z->info);
	   free (z->data);
	   free (z);
	}
}

void sys_abort (int val, char *mess)
{
	fprintf (stderr, "**** System library ABORT %d: %s ****\n", 
			val, mess);
	exit (2);
}

void copy (IMAGE *a, IMAGE b)
{
	CopyVarImage (a, &b);
}

void CopyVarImage (IMAGE *a, IMAGE *b)
{
	int i,j;

	if (a == b) return;
	if (*a) freeimage (*a);
	*a = newimage ((*b)->info->nr, (*b)->info->nc);
	if (*a == 0) sys_abort (0, "No more storage.\n");

	for (i=0; i<(*b)->info->nr; i++)
	  for (j=0; j< (*b)->info->nc; j++)
	    (*a)->data[i][j] = (*b)->data[i][j];
	(*a)->info->oi = (*b)->info->oi;
	(*a)->info->oj = (*b)->info->oj;
}

float ** f2d (int nr, int nc)
{
	float **x;
	int i;

	x = (float **)calloc ( nr, sizeof (float *) );
	if (x == 0)
	{
	  fprintf (stderr, "Out of storage: F2D.\n");
	  exit (1);
	}

	for (i=0; i<nr; i++)
	{  
	  x[i] = (float *) calloc ( nc, sizeof (float)  );
	  if (x[i] == 0)
	  {
	    fprintf (stderr, "Out of storage: F2D %d.\n", i);
	    exit (1);
	  }
	}
	return x;
}

void free2d (float **x, int nr)
{
	int i;

	for (i=0; i<nr; i++)
		free(x[i]);
	free (x);
}

/* Small system random number generator */
double drand32 ()
{
	static long a=16807L, m=2147483647L,
		    q=127773L, r = 2836L;
	long lo, hi, test;

	hi = seed / q;
	lo = seed % q;
	test = a*lo -r*hi;
	if (test>0) seed = test;
	else seed = test + m;

	return (double)seed/(double)m;
}

void srand32 (long k)
{
	seed = k;
}


IplImage *toOpenCV (IMAGE x)
{
	IplImage *img;
	int i=0, j=0;
	CvScalar s;
	
	img = cvCreateImage(cvSize(x->info->nc, x->info->nr),8, 1);
	for (i=0; i<x->info->nr; i++)
	{
		for (j=0; j<x->info->nc; j++)
		{
			s.val[0] = x->data[i][j];
			cvSet2D (img, i,j,s);
		}
	}
	return img;
}

IMAGE fromOpenCV (IplImage *x)
{
	IMAGE img;
	int color=0, i=0;
	int k=0, j=0;
	CvScalar s;
	
	if ((x->depth==IPL_DEPTH_8U) &&(x->nChannels==1))								// 1 Pixel (grey) image
		img = newimage (x->height, x->width);
	else if ((x->depth==8) && (x->nChannels==3)) //Color
	{
		color = 1;
		img = newimage (x->height, x->width);
	}
	else return 0;

	for (i=0; i<x->height; i++)
	{
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			if (color) 
			  k = (unsigned char)((s.val[0] + s.val[1] + s.val[2])/3);
			else k = (unsigned char)(s.val[0]);
			img->data[i][j] = k;
		}
	}
	return img;
}

/* Display an image on th escreen */
void display_image (IMAGE x)
{
	IplImage *image = 0;
	char wn[20];
	int i;

	image = toOpenCV (x);
	if (image <= 0) return;

	for (i=0; i<19; i++) wn[i] = (char)((drand32()*26) + 'a');
	wn[19] = '\0';
	cvNamedWindow( wn, CV_WINDOW_AUTOSIZE );
	cvShowImage( wn, image );
	cvWaitKey(0);
	cvReleaseImage( &image );
}

void save_image (IMAGE x, char *name)
{
	IplImage *image = 0;

	image = toOpenCV (x);
	if (image <0) return;

	cvSaveImage( name, image );
	cvReleaseImage( &image );
}

IMAGE get_image (char *name)
{
	IMAGE x=0;
	IplImage *image = 0;

	image = cvLoadImage(name, 0);
	if (image <= 0) return 0;
	x = fromOpenCV (image);
	cvReleaseImage( &image );
	return x;
}

IMAGE grab_image ()
{
	CvCapture  *camera = 0;
	IplImage *image = 0;
	IMAGE x;

	camera = cvCaptureFromCAM( CV_CAP_ANY );
	if( !camera )								// Get a camera?
	{
		fprintf(stderr, "Can�t initialize camera\n");
		return 0;
	}
	image = cvQueryFrame( camera );						
	x = fromOpenCV (image);
	cvReleaseCapture( &camera );
	return x;
}

int get_RGB (IMAGE *r, IMAGE *g, IMAGE *b, char *name)
{
	IplImage *image = 0;
	IMAGE i1, i2, i3;
	int i,j;

	// Read the image from a file into Ip1Image
	image = cvLoadImage(name, 1);
	if (image <= 0) return 0;

	// Create three AIPCV images of correct size
	i1 = newimage (image->height, image->width);
	i2 = newimage (image->height, image->width);
	i3 = newimage (image->height, image->width);

	// Copy pixels from Ip1Image to AIPCV images
	for (i=0; i<image->height; i++)
		for (j=0; j<image->width; j++)
		{
		  i1->data[i][j] = (image->imageData+i*image->widthStep)[j*image->nChannels+0];
		  i2->data[i][j] = (image->imageData+i*image->widthStep)[j*image->nChannels+1];
		  i3->data[i][j] = (image->imageData+i*image->widthStep)[j*image->nChannels+2];
		}
	cvReleaseImage(&image);
	*r = i3; 
	*g = i2;
	*b = i1;
	return 1;
}

int save_RGB (IMAGE r, IMAGE g, IMAGE b, char *name)
{
	IplImage *image = 0;
	int i,j,k;

	if ( (r->info->nc != g->info->nc) || (r->info->nr != g->info->nr) ) return 0;
	if ( (r->info->nc != b->info->nc) || (r->info->nr != b->info->nr) ) return 0;

	// Create an  IplImage
	image = cvCreateImage(cvSize(r->info->nc, r->info->nr),IPL_DEPTH_8U,3);
	if (image <= 0) return 0;


	// Copy pixels from AIPCV images into Ip1Image
/*		for (i=0; i<r->info->nr; i++)
		{
			for (j=0; j<r->info->nc; j++)
			{
				s.val[0] = b->data[i][j];
				s.val[1] = g->data[i][j];
				s.val[2] = r->data[i][j];
				cvSet2D (image, i,j,s);
		}
	} */
		
	for (i=0; i<image->height; i++)
		for (j=0; j<image->width; j++)
		{
		  (image->imageData+i*image->widthStep)[j*image->nChannels+0] = b->data[i][j];
		  (image->imageData+i*image->widthStep)[j*image->nChannels+1] = g->data[i][j];
		  (image->imageData+i*image->widthStep)[j*image->nChannels+2] = r->data[i][j];
		} 
	k = cvSaveImage(name, image);
	cvReleaseImage(&image);
	return 1-k;

}


void main ()
{
	int   i,j, k;
	float gamma = 0.0,	/* Constant for scaling, = 1/9    */
	      E2 = 0.0,		/* Sum of pixel measures          */
	      d = 0.0, 		/* Minimum distance */
	      z = 0.0;
	char  filename[128], *p;
	char  name[128];


/* Open the image file */
	printf ("Eval2 - Rosenfeld edge detector evaluation.\n");
	printf ("Enter image file: ");
	scanf ("%s", name);
	x = get_image(name);

	if (x == 0)
	{
	  printf ("No input image ('%s')\n", name);
	  exit (2);
	}
	display_image (x);

	for (i=0; i<x->info->nr; i++)
	  for (j=0; j<x->info->nc; j++)
	    if (x->data[i][j] > 128) x->data[i][j] = 0;
	    else x->data[i][j] = 1;

		/* Compute gradient direction from the edge image */
	grad_init ();

	gamma = 0.8;

	E2 = 0;  k = 0;
	for (i=2; i<x->info->nr-2; i++)
	  for (j=2; j<x->info->nc-2; j++)
	    if (x->data[i][j] == 1)
	    {
	      z = C(i,j)*gamma + T(i,j)*(1.0-gamma);
	      E2 += z;
	      k++;
	    }

	E2 = E2/(double)k;
	printf ("Eval2 (E2) Edge detector evaluation: E2 = %f\n", E2);

}

