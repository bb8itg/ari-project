// Popularity quantization on hue image.
//

#include "stdafx.h"
#include "cv.h"
#include "highgui.h"

int histo[6][6][6];
int peaksi[32], peaksj[32], peaksk[32], peaksv[32];
int npeaks = 0;
int Nclasses = 4;

void initialize ()
{
	int i,j,k;

		for (i=0; i<6; i++)
			for (j=0; j<6; j++)
				for (k=0; k<6; k++)
					histo[i][i][j] = 0;
}

void binValue (CvScalar p)
{
	int i, j, k=0;
	double a, b, c;

	a = p.val[2]/43.0;	// red
	b = p.val[1]/43.0;	// green
	c = p.val[0]/43.0;
	i = (int)(a); j = (int)(b); k = (int)(c);
	histo[i][j][k] += 1;
}

int isPeak (int h[6][6][6], int I, int J, int K)
{
	int k, s;

	s = k = h[I][J][K];
	return s;

	if ( (I-1>=0) && (h[I-1][J][K] > k)) return 0;
	else if (I-1>=0) s += h[I-1][J][K];
	if ( (I+1<6)  && (h[I+1][J][K] > k)) return 0;
	else if (I+1<6) s += h[I+1][J][K];
	if ( (J-1>=0) && (h[I][J-1][K] > k)) return 0;
	else if (J-1>=0) s += h[I][J-1][K];
	if ( (J+1<6)  && (h[I][J+1][K] > k)) return 0;
	else if (J+1<6) s += h[I][J+1][K];
	if ( (K-1>=0) && (h[I][J][K-1] > k)) return 0;
	else if (K-1>=0) s += h[I][J][K-1];
	if ( (K=1<6)  && (h[I][J][K+1] > k)) return 0;
	else if (K=1<6) s += h[I][J][K+1];
	return s;
}

void savePeak (int h[6][6][6], int I, int J, int K)
{
	int i, k=0, v=0;
	
	v = isPeak (h, I,J,K);

	if (npeaks >= 32)							// Table is full.
	{
		for (i=1; i<32; i++)					// Find smallest
			if (peaksv[i] < peaksv[k]) k = i;
		if (v < peaksv[k]) return;				// Bigger than this one?
		peaksi[k] = I; peaksj[k] = J; peaksk[k] = K; peaksv[k] = v;			// no - replace smallest peak with this one.
	}
	else										// Table is not full
	{
		peaksi[npeaks] = I;						// Add this peak to the end.
		peaksj[npeaks] = J;
		peaksk[npeaks] = K;
		peaksv[npeaks] = v;
		printf ("Added a peak, number %d-%d-%d, value %d\n", peaksi[npeaks], peaksj[npeaks], peaksk[npeaks], peaksv[npeaks]);
		npeaks++;
	}
}

void sortPeaks ()
{
		int i,j,k;
		CvScalar s;

		s.val[0] = s.val[1]=s.val[2] = 0;
		printf ("Before sort, top peaks are:");
		printf ("At %d-%d-%d with value %d\n", peaksi[0], peaksj[0], peaksk[0], peaksv[0]);
		printf ("at %d-%d-%d with value %d\n", peaksi[1], peaksj[1], peaksk[1], peaksv[1]);
		printf ("at %d-%d-%d with value %d\n", peaksi[2], peaksj[2], peaksk[2], peaksv[2]);
		printf ("at %d-%d-%d with value %d\n", peaksi[3], peaksj[3], peaksk[3], peaksv[3]);

		for (i=0; i<npeaks; i++)
			for (j=i+1; j<npeaks; j++)
				if (peaksv[i] < peaksv[j])
				{
					k = peaksv[i]; peaksv[i] = peaksv[j]; peaksv[j] = k;
					k = peaksi[i]; peaksi[i] = peaksi[j]; peaksi[j] = k;
					k = peaksj[i]; peaksj[i] = peaksj[j]; peaksj[j] = k;
					k = peaksk[i]; peaksk[i] = peaksk[j]; peaksk[j] = k;
				}

		printf ("After  sort, top peaks are: -----------\n");
		printf ("At %d-%d-%d with value %d ", peaksi[0], peaksj[0], peaksk[0], peaksv[0]);
		printf ("at %d-%d-%d with value %d ", peaksi[1], peaksj[1], peaksk[1], peaksv[1]);
		printf ("at %d-%d-%d with value %d ", peaksi[2], peaksj[2], peaksk[2], peaksv[2]);
		printf ("at %d-%d-%d with value %d ", peaksi[3], peaksj[3], peaksk[3], peaksv[3]);
}


void histogram (IplImage *image)
{
	int i, j, k;
	CvScalar s;

// Build a histogram
		for (i=0; i<image->height; i++)
			for (j=0; j<image->width; j++)
			{
				s = cvGet2D(image, i, j);
				binValue (s);
			}

// Look for the best peaks
		for (i=0; i<6; i++)
			for (j=0; j<6; j++)
				for (k=0; k<6; k++)
				{
					printf ("%d. %d ", i, histo[i]);
					if (isPeak (histo, i,j,k)) 
					{
						savePeak (histo, i,j,k);
						printf (" is a peak.\n");
					}
				}
		sortPeaks ();

		printf ("Histogram:======================\n");
		for (i=0; i<6; i++)
			for (j=0; j<6; j++)
				for (k=0; k<6; k++)
			printf ("%d.%d.%d    %d\n", i,j,k, histo[i][j][k]);
		printf ("================================\n");
}

int nearestPrototype (CvScalar p)
{
	int i, a, b, c, k=0, n=0;
	float d, dmin=1.0e12;

	for (i=0; i<Nclasses; i++)	// Look at top Nclasses peaks.
	{
		a = peaksi[i]*43+21;
		b = peaksj[1]*43+21;
		c = peaksk[i]*43+21;
		d = (p.val[0]-c)*(p.val[0]-c) + (p.val[1]-b)*(p.val[1]-b)
			      + (p.val[2]-a)*(p.val[2]-a);
		if (d < dmin)
		{
			dmin = d;
			n = i;
		}
	}
	return n;
}

int _tmain(int argc, _TCHAR* argv[])
{
	IplImage *image = 0, *tmp, *hue, *sat, *val;
	CvScalar s;
	CvSize size;
	int i,j,k, depth;

	initialize();

	image = cvLoadImage( "H:/Documents and Settings/jp/Desktop/EditionTwo/Chapter 4 - Texture/science.jpg", 1 );
	if( image )
	{
		cvNamedWindow( "Before", 1 );
		cvShowImage( "Before", image );
		printf( "Press any key to exit\n");
		cvWaitKey(0);
		

		size = cvGetSize(image);
		depth = image->depth;
		tmp = cvCreateImage(size, depth, 3);
		cvSmooth(image, tmp, CV_BLUR, 1, 1);
	
		cvCvtColor( tmp, image, CV_BGR2HSV );

		hue = cvCreateImage(size, depth, 1);
		sat = cvCreateImage(size, depth, 1);
		val = cvCreateImage(size, depth, 1);
		cvZero(hue);
		cvZero(sat);
		cvZero(val);
		cvSplit(image, hue, sat, val, 0);
		cvNamedWindow("hue", CV_WINDOW_AUTOSIZE);
		cvNamedWindow("saturation", CV_WINDOW_AUTOSIZE);
		cvNamedWindow("value", CV_WINDOW_AUTOSIZE);

	    cvShowImage("hue", hue);
	    cvShowImage("saturation", sat);
	    cvShowImage("value", val);
		cvWaitKey(0);

		histogram (image);
		for (i=0; i<image->height; i++)
			for (j=0; j<image->width; j++)
			{
				s = cvGet2D(image, i, j);
				k = nearestPrototype (s);
				s.val[0] = (double)peaksk[k]*43+21;
				s.val[1] = (double)peaksj[k]*43+21;
				s.val[2] = (double)peaksi[k]*43+21;
				cvSet2D(image, i, j, s);
			}
		cvNamedWindow ("After", 1);
		cvShowImage ("After", image);
		cvWaitKey(0);
		cvDestroyWindow("Before");
		cvDestroyWindow("After");
		cvSaveImage ("H:/Documents and Settings/jp/Desktop/EditionTwo/Chapter 4 - Texture/hue.jpg", image);
	}
	else
		fprintf( stderr, "Error reading image\n" );
	return 0;
}