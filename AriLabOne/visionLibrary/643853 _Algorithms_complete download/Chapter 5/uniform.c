// Uniform quantization.
//

//#include "stdafx.h"
#include "cv.h"
#include "highgui.h"
#include <stdio.h>

CvScalar protos[16];

void initialize ()
{
	/*
	protos[0].val[0] = 64;   protos[0].val[1] = 64;   protos[0].val[2] = 64;
	protos[1].val[0] = 64;   protos[1].val[1] = 64;   protos[1].val[2] = 192;
	protos[2].val[0] = 64;   protos[2].val[1] = 192;  protos[2].val[2] = 64;
	protos[3].val[0] = 64;   protos[3].val[1] = 192;  protos[3].val[2] = 192;
	protos[4].val[0] = 192;  protos[4].val[1] = 64;   protos[4].val[2] = 64;
	protos[5].val[0] = 192;  protos[5].val[1] = 64;   protos[5].val[2] = 192;
	protos[6].val[0] = 192;  protos[6].val[1] = 192;  protos[6].val[2] = 64;
	protos[7].val[0] = 192;  protos[7].val[1] = 192;  protos[7].val[2] = 192; */

//	protos[0].val[0] = 64;   protos[0].val[1] = 200;   protos[0].val[2] = 200;   // Yellow  
//	protos[1].val[0] = 64;   protos[1].val[1] = 200;   protos[1].val[2] = 64;    // Green 
//	protos[2].val[0] = 64;   protos[2].val[1] = 64;  protos[2].val[2] = 200;     // Red 
//	protos[3].val[0] = 192;   protos[3].val[1] = 192;  protos[3].val[2] = 192;   // White 

//	protos[0].val[0] = 128;  protos[0].val[1] = 128;   protos[0].val[2] = 128;
//	protos[1].val[0] = 252;  protos[1].val[1] = 246;   protos[1].val[2] = 176;   // Sky
//	protos[2].val[0] = 4;    protos[2].val[1] = 90;   protos[2].val[2] = 51;	 // grass
//	protos[3].val[0] = 23;   protos[3].val[1] = 37;    protos[3].val[2] = 117;   // brick
//	protos[4].val[0] = 192;  protos[4].val[1] = 192;   protos[4].val[2] = 192;   // white

	protos[0].val[0] = 0;  protos[0].val[1] = 0;   protos[0].val[2] = 0;
	protos[1].val[0] = 207;  protos[1].val[1] = 205;   protos[1].val[2] = 239;   // Skin
	protos[2].val[0] = 220;    protos[2].val[1] = 220;   protos[2].val[2] = 220;	 // grass
	protos[3].val[0] = 33;   protos[3].val[1] = 97;    protos[3].val[2] = 169;   // brick
	protos[4].val[0] = 80;  protos[4].val[1] = 80;   protos[4].val[2] = 80;   // white
}

int nearestPrototype (CvScalar p)
{
	int i, k=0;
	float d, dmin=1.0e12f;

	for (i=0; i<=2; i++)
	{
		d = (p.val[0]-protos[i].val[0])*(p.val[0]-protos[i].val[0]) +
			(p.val[1]-protos[i].val[1])*(p.val[1]-protos[i].val[1]) +
			(p.val[2]-protos[i].val[2])*(p.val[2]-protos[i].val[2]);
		if (d < dmin)
		{
			dmin = d; k=i;
		}
	}
	return k;
}

int main(int argc, char* argv[])
{
	IplImage *image = 0;
	CvScalar s;
	int i,j,k;

	initialize();
	image = cvLoadImage( "H:/Documents and Settings/jp/Desktop/EditionTwo/Chapter 4 - Texture/Figure4.13bt.jpg", 1 );
	if( image )
	{
		cvNamedWindow( "Before", 1 );
		cvShowImage( "Before", image );
		printf( "Press any key to exit\n");
		cvWaitKey(0);

		for (i=0; i<image->height; i++)
			for (j=0; j<image->width; j++)
			{
				s = cvGet2D(image, i, j);
				k = nearestPrototype (s);
				cvSet2D(image, i, j, protos[k]);
			}
		cvNamedWindow ("After", 1);
		cvShowImage ("After", image);
		cvWaitKey(0);
		cvDestroyWindow("Before");
		cvDestroyWindow("After");
		cvSaveImage ("H:/Documents and Settings/jp/Desktop/EditionTwo/Chapter 4 - Texture/scienceunif2.jpg", image);
	}
	else
		printf("Error reading image\n" );
	return 0;
}