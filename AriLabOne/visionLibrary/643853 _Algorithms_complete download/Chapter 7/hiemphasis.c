/* High-emphasis filter */
#define MAX
#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <stdlib.h>
#include <cv.h>
#include <highgui.h>
#include <stdlib.h>
int NORMALIZE = 0;
int Nd = 16;
int FMAX = 15;

void saveImage (char *name, IplImage *x)
{
	IplImage *realpart, *savepart;
	CvScalar s;
	int i,j;
	double z, y, m, M;

// Split Fourier in real and imaginary parts
	realpart = cvCreateImage( cvSize(x->height, x->width), IPL_DEPTH_64F, 1);
	savepart = cvCreateImage( cvSize(x->height, x->width), IPL_DEPTH_8U, 1);
		
// Compute the magnitude of the transform  = log((norm) + 1)
	for (i=0; i<x->height; i++)
	{
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			z = s.val[0];
			y = s.val[1];
			z = sqrt(z*z+y*y) + 1.0f;
			s.val[0] = log(z);
			cvSet2D (realpart, i, j, s);
		}
	}

 // scale image for display
    cvMinMaxLoc(realpart, &m, &M, NULL, NULL, NULL);
	printf ("Minimum value is %lf  max value is %lf\n", m, M);

// scale
	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (realpart, i, j);
			s.val[0] = (s.val[0]-m)/(M-m) * 255;
			cvSet2D (savepart, i, j, s);
		} 
		  
// Save image
    cvSaveImage (name, savepart);
	cvReleaseImage( &realpart );
	cvReleaseImage( &savepart );
}

void toGrey (IplImage **y)
{
	IplImage *g, *x;

	x = *y;
	g = cvCreateImage(cvSize(x->width,x->height), x->depth, 1);
	g->origin = x->origin; 
	cvCvtColor(x, g, CV_BGR2GRAY);
	cvReleaseImage(y);
	*y = g;
}

IplImage *align (IplImage *psf, int height, int width)
{
	int i,j, is, js;
	IplImage *a;
	CvScalar s;

	a = cvCreateImage( cvSize(height, width), IPL_DEPTH_64F, 1);
	is = height - psf->height;
	js = width - psf->width;
	if ((is < 0) || (js < 0))
	{
		printf ("PSF is bigger than the image.\n");
		exit (0);
	}

	is = is/2; js = js/2;
	cvSetZero(a);
	for (i=0; i<psf->height; i++)
		for (j=0; j<psf->width; j++)
		{
			s = cvGet2D (psf, i, j);
			cvSet2D (a, i+is, j+js, s);
		}
	return a;
}

IplImage * cvMatToImage (CvMat *x)
{
	IplImage *a;
	double *dp;
	int i,j;
	CvSize s;
	CvScalar S;

	dp = x->data.db;
	s.height = x->rows;
	s.width = x->cols;
	a = cvCreateImage( s, IPL_DEPTH_64F, 2);

	for (i=0; i<s.height; i++)
		for (j=0; j<s.width; j++)
		{
			S.val[0] = *dp++;
			S.val[1] = *dp++;
			cvSet2D (a, i, j, S);
		}
//cvSplit( dft_A, image_Re, image_Im, 0, 0 );
	return a;
}

/* Display a frequency domain image - needs to be made real and grey scaled */
void displayFDimage (IplImage *x, char *title)
{
	IplImage *realpart;
	CvScalar s;
	int i,j;
	double z, y, m, M;

// Split Fourier in real and imaginary parts
	realpart = cvCreateImage( cvSize(x->height, x->width), IPL_DEPTH_64F, 1);
		
// Compute the magnitude of the transform  = log((norm) + 1)
	for (i=0; i<x->height; i++)
	{
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			z = s.val[0];
			y = s.val[1];
			z = sqrt(z*z+y*y) + 1.0f;
			s.val[0] = log(z);
			cvSet2D (realpart, i, j, s);
		}
	}

 // scale image for display
    cvMinMaxLoc(realpart, &m, &M, NULL, NULL, NULL);
	printf ("Minimum value is %lf  max value is %lf\n", m, M);

// scale
	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (realpart, i, j);
			s.val[0] = (s.val[0]-m)/(M-m);
			cvSet2D (realpart, i, j, s);
		} 
		  
// display image in window
	cvNamedWindow(title, 0);
    cvShowImage( title, realpart );
    cvWaitKey (0);
	cvReleaseImage( &realpart );
	cvDestroyWindow (title);
}

void origin_center( IplImage *array )
{
    int  i, j;
	CvScalar s;

     for( i = 0; i < array->height; i++ )
          for( j =0; j < array->width; j++ )
               if( (i + j) % 2 )  
			   {
					s = cvGet2D (array, i, j);
					s.val[0] = -s.val[0];
					s.val[1] = -s.val[1];
					cvSet2D (array, i, j, s);
               }

}

IplImage *fftImage (IplImage *img)
{
	IplImage *realpart, *imgpart, *complexpart, *ret;
	CvMat *ft;
	int sizeM, sizeN;
	CvMat tmp;
	double xmax = 0.0;
	CvScalar s;
	int i,j;

    realpart = cvCreateImage( cvGetSize(img), IPL_DEPTH_64F, 1);
    imgpart = cvCreateImage( cvGetSize(img), IPL_DEPTH_64F, 1);
	complexpart = cvCreateImage( cvGetSize(img), IPL_DEPTH_64F, 2);	
    cvScale(img, realpart, 1.0, 0.0);              // copy grey input image to realpart
	if(NORMALIZE)
	{
		for (i=0; i<img->height; i++)
			for (j=0; j<img->width; j++)
		{
			s = cvGet2D (realpart, i, j);
			if (s.val[0] > xmax) xmax = s.val[0];
		}
		for (i=0; i<img->height; i++)
			for (j=0; j<img->width; j++)
		{
			s = cvGet2D (realpart, i, j);
			s.val[0] /= xmax;
			cvSet2D (realpart, i, j, s);
		}
	}
    cvZero(imgpart);                                   // Set imaginary part to 0
    cvMerge(realpart, imgpart, NULL, NULL, complexpart); // real+imag to complex
		  			  
/* Messy bit - fft needs sizes to be a power of 2, so the images have to be
// embedded into a background of 0 pixels. */
	sizeM = cvGetOptimalDFTSize( img->height - 1 );
	sizeN = cvGetOptimalDFTSize( img->width - 1 );
	printf ("Size of image to be transformed is %dx%d\n", sizeM, sizeN);

	ft = cvCreateMat( sizeM, sizeN, CV_64FC2 );
    origin_center (complexpart);

// copy A to dft_A and pad dft_A with zeros
    cvGetSubRect( ft, &tmp, cvRect(0,0, img->width, img->height));  // tmp points to sub of dft_A
    cvCopy( complexpart, &tmp, NULL );                                       // Copy complex image into sub of dft_A
    cvGetSubRect( ft, &tmp, cvRect(img->width,0, 
		                   ft->cols - img->width, img->height));   // Get sub of dft_A on right side
    if ((ft->cols - img->width) > 0) cvZero( &tmp );                   // Set right margin to zero
			  

    cvDFT( ft, ft, CV_DXT_FORWARD, complexpart->height );

    ret = cvMatToImage (ft);
	cvReleaseMat(&ft);
    cvReleaseImage( &realpart );
    cvReleaseImage( &imgpart );
    cvReleaseImage( &complexpart);
	return ret;
}

IplImage *fftImageInv (IplImage *img)
{
	IplImage *ret;
	CvMat *ft;
	CvMat tmp;

	ft = cvCreateMat( img->height, img->width, CV_64FC2 );
//    origin_center (img);

// copy A to dft_A and pad dft_A with zeros
    cvGetSubRect( ft, &tmp, cvRect(0,0, img->width, img->height));  // tmp points to sub of dft_A
    cvCopy( img, &tmp, NULL );                                       // Copy complex image into sub of dft_A
    cvDFT( ft, ft, (/*CV_DXT_SCALE|*/CV_DXT_INVERSE), img->height );

    ret = cvMatToImage (ft);
	cvReleaseMat(&ft);
	printf ("INVFFT Return image is: Depth=%d channels=%d\n", ret->depth, ret->nChannels);
	return ret;
 }

int main (int argc, char *argv[])
{
	int nr, nc, ia, ib;
	double radius, dist;
	IplImage  *im1;
	IplImage *fft1, *fft2;
	int i,j, hc, vc;
	CvScalar s, s2, sd, s1;
	char filename[128];
	double p, q, a, b, val;
	double d[] = { 0.5, 0.55, 0.6, 0.8, 1.0, 1.3, 1.5, 1.6, 1.7, 
		           1.8, 1.85, 1.9, 1.94, 1.97, 1.99, 2.0, 2.0};

	printf ("High emphasis filter\n");

	printf ("Input file name: ");
	scanf ("%s", filename);
	im1 = cvLoadImage(filename, CV_LOAD_IMAGE_UNCHANGED);
	if (im1 == 0)
	{
	  printf ("No such file as '%s'\n", filename);
	  exit(1);
	}

	cvNamedWindow("Original", 0);
	if (im1->nChannels > 1) toGrey (&im1);
	cvShowImage( "Original", im1 );
	cvWaitKey (0);

	nr = im1->height; nc = im1->width;

	NORMALIZE = 1;
	fft1 = fftImage (im1);
	NORMALIZE = 0;
	displayFDimage (fft1, "FD Before");
	fft2 = cvCloneImage (fft1);
 
/* Clear pixels in the specified radius of the center */
	hc = nc/2; vc = nr/2;
	s2.val[0] = 2.0; s2.val[1] = 0.0;
	s.val[0] = s.val[1] = 0.0;
	sd.val[0] = d[0]; sd.val[1] = 0.0;

	for (i=0; i<nr; i++)
	  for (j=0; j<nc; j++)
	  {
	    dist = sqrt((double)((i-vc)*(i-vc) + (j-hc)*(j-hc)));
		if (dist > FMAX)
		  cvSet2D (fft2, i, j, s2);
		else if (dist > 0.0)
		{
			ia = (int)dist;  ib = ia+1;
			a  = d[ia];      b  = d[ib];
			val = (float)(dist-ia)*a + (float)(ib-dist)*b;
			s.val[0] = val; s.val[1] = 0.0;
			cvSet2D(fft2, i,j, s);
		} else 
			cvSet2D (fft2, i,j, sd);

//	    p[i][j] = fft2[i][j] * 127;
	  }
	displayFDimage (fft2, "HE filter");

	for (i=0; i<nr; i++)
	  for (j=0; j<nc; j++)
	  {
			s = cvGet2D (fft1, i,j);
			s1 = cvGet2D (fft2, i,j);
		  	p = s.val[0]*s1.val[0] - s.val[1]*s1.val[1];
			q = s.val[0]*s1.val[1] + s.val[1]*s1.val[0];
			s.val[0] = p; s.val[1] = q;
			cvSet2D (fft1, i,j,s);
	  }


	fft2 = fftImageInv (fft1);
	displayFDimage (fft2, "Filtered Image");
}