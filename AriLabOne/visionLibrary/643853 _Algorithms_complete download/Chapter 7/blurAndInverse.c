// Inverse filter
#define MAX
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <stdlib.h>

int NORMALIZE = 0;

void cvShiftDFT(CvArr * src_arr, CvArr * dst_arr );
void origin_center( IplImage *array );
void displayFD (CvMat *x, char *name);
void displayFDimage (IplImage *x, char *name);
IplImage * cvMatToImage (CvMat *x);
IplImage *fftImage (IplImage *img);
IplImage *align (IplImage *psf, int height, int width);
IplImage *fftImageInv (IplImage *img);
void saveImage (char *name, IplImage *x); 

double norm (double a, double b)
{
	return  ((a*a + b*b));
}

void extract (IplImage *dest, IplImage *source)
{
	
}

void cdiv (double a, double b, double c, double d, double *rr, double *ri)
{                               /* res = c1/c2 ... Complex */
	double z,real, imag;

	z = (c*c + d*d);
	if (z != 0.0) 
	{
		real = (a*c + b*d)/z;
		imag = (b*c - a*d)/z;
		*rr = real;
		*ri = imag;
		return;
	}
	*rr = 0.0;     *ri = 0.0;
}

int main( int argc, char** argv )
{
  IplImage* img = NULL;       
  IplImage *psf, *psfa, *fftpsf;
  IplImage *grayImg, *z, *zz, *ft, *result, *resulti;
  int i,j;
  CvScalar s, s1;
  double p, q, rad=10.0, rr, ri;

/* Read in the image to be processed */
	img = cvLoadImage("h:\\aipcv\\ch6\\face.pgm", CV_LOAD_IMAGE_UNCHANGED);
	if (img == 0) 
	{
		printf ("Input file not open.\n");
		return 0;
	}

/* Convert the (possibly) color image into grey */
	grayImg = cvCreateImage(cvSize(img->width,img->height), img->depth, 1);
	grayImg->origin = img->origin;
	if (img->nChannels > 1) 
	  cvCvtColor(img, grayImg, CV_BGR2GRAY);
	else
	  grayImg = img;
	cvNamedWindow("Original", 0);		
	cvShowImage( "Original", grayImg );
	cvWaitKey (0);

 // Computer the Fourier transform
	z = fftImage (grayImg);
	displayFDimage (z, "Transform");

// Read the PSF image
	psf = cvLoadImage("h:\\aipcv\\ch6\\psf1.pgm", CV_LOAD_IMAGE_UNCHANGED);
	psfa = align (psf, img->height, img->width);   // Make PSF the same size, zero fill.

// Fourier transform of the PSF
	zz = fftImage (psfa);
	fftpsf = fftImage (psfa);
	displayFDimage (zz, "FFT of PSF");

// Now do the work - multiply corresponding elements
	for (i=0; i<z->height; i++)
	{
		for (j=0; j<z->width; j++)
		{
			s = cvGet2D (z, i, j);
			s1 = cvGet2D (zz, i, j);
			p = s.val[0]*s1.val[0] - s.val[1]*s1.val[1];
			q = s.val[0]*s1.val[1] + s.val[1]*s1.val[0];
			s.val[0] = p; s.val[1] = q;
			cvSet2D (z, i, j, s);
		}
	}
	cvReleaseImage(&psf);
	cvReleaseImage(&psfa);

// Inverse transform
	zz = fftImageInv (z);
	result = fftImageInv (z);
	displayFDimage (zz, "Result");
	saveImage ("h:\\aipcv\\ch6\\faceblur2010.tif",result);
	// At this point, zz if the blurred image; fftpsf is the fourier transform of the psf.
	// and z if the fourier transform of the blurred image.
	do
	{
		printf ("Radius is now %lf /n.", rad);

// Now do the work - multiply corresponding elements
	for (i=0; i<z->height; i++)
	{
		for (j=0; j<z->width; j++)
		{
			s = cvGet2D (z, i, j);
			s1 = cvGet2D (fftpsf, i, j);
			if (norm(s1.val[0], s1.val[1]) >= rad)
			{
				cdiv (s.val[0], s.val[1], s1.val[0], s1.val[1], &rr, &ri);
				s1.val[0] = rr; s1.val[1] = ri;
			}
			else
			{
 				s1.val[0] = 0.0; s1.val[1] = 0.0;
//				s1.val[0] = s.val[0]; s1.val[1] = s .val[1];
			}
			cvSet2D (result, i, j, s1);
		}
	}

//	At this point, fftpsf is the fourier transform of the psf.

// Inverse transform
	origin_center (result);
	resulti = fftImageInv (result);
	displayFDimage (resulti, "Inverse");
	saveImage ("h:\\aipcv\\ch6\\facerestinv.pgm", resulti);
	cvReleaseImage(&resulti);
	rad /= 2.0;
	} while (rad >= 0);
	return 0;

}

/* Display a frequency domain image - needs to be made real and grey scaled */
void displayFD (CvMat *x, char *title)
{
	IplImage *realpart, *imgpart;
	CvScalar s;
	int i,j;
	double z, y, m, M;

// Split Fourier in real and imaginary parts
	realpart = cvCreateImage( cvSize(x->rows, x->cols), IPL_DEPTH_64F, 1);
    imgpart  = cvCreateImage( cvSize(x->rows, x->cols), IPL_DEPTH_64F, 1);
	cvSplit( x, realpart, imgpart, 0, 0 );
		
// Compute the magnitude of the spectrum Mag = sqrt(Re^2 + Im^2)
	for (i=0; i<realpart->height; i++)
		for (j=0; j<realpart->width; j++)
		{
			s = cvGet2D (realpart, i, j);
			z = s.val[0];
			s = cvGet2D (imgpart, i, j);
			y = s.val[0];
			z = sqrt(z*z+y*y) + 1.0f;
			s.val[0] = log(z);
			cvSet2D (realpart, i, j, s);
		}

 // scale image for display
    cvMinMaxLoc(realpart, &m, &M, NULL, NULL, NULL);
	printf ("Minimum value is %lf  max value is %lf\n", m, M);

// scale
	for (i=0; i<realpart->height; i++)
		for (j=0; j<realpart->width; j++)
		{
			s = cvGet2D (realpart, i, j);
			s.val[0] = (s.val[0]-m)/(M-m);
			cvSet2D (realpart, i, j, s);
		} 
		  
// display image in window
	cvNamedWindow(title, 0);
    cvShowImage( title, realpart );
    cvWaitKey (0);
	cvReleaseImage( &realpart );
    cvReleaseImage( &imgpart );
	cvDestroyWindow ( title );
}

IplImage *align (IplImage *psf, int height, int width)
{
	int i,j, is, js;
	IplImage *a;
	CvScalar s;

	a = cvCreateImage( cvSize(height, width), IPL_DEPTH_64F, 1);
	is = height - psf->height;
	js = width - psf->width;
	if ((is < 0) || (js < 0))
	{
		printf ("PSF is bigger than the image.\n");
		exit (0);
	}

	is = is/2; js = js/2;
	cvSetZero(a);
	for (i=0; i<psf->height; i++)
		for (j=0; j<psf->width; j++)
		{
			s = cvGet2D (psf, i, j);
			cvSet2D (a, i+is, j+js, s);
		}
	return a;
}

IplImage * cvMatToImage (CvMat *x)
{
	IplImage *a;
	double *dp;
	int i,j;
	CvSize s;
	CvScalar S;

	dp = x->data.db;
	s.height = x->rows;
	s.width = x->cols;
	a = cvCreateImage( s, IPL_DEPTH_64F, 2);

	for (i=0; i<s.height; i++)
		for (j=0; j<s.width; j++)
		{
			S.val[0] = *dp++;
			S.val[1] = *dp++;
			cvSet2D (a, i, j, S);
		}
//cvSplit( dft_A, image_Re, image_Im, 0, 0 );
	return a;
}

/* Display a frequency domain image - needs to be made real and grey scaled */
void displayFDimage (IplImage *x, char *title)
{
	IplImage *realpart;
	CvScalar s;
	int i,j;
	double z, y, m, M;

// Split Fourier in real and imaginary parts
	realpart = cvCreateImage( cvSize(x->height, x->width), IPL_DEPTH_64F, 1);
		
// Compute the magnitude of the transform  = log((norm) + 1)
	for (i=0; i<x->height; i++)
	{
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			z = s.val[0];
			y = s.val[1];
			z = sqrt(z*z+y*y) + 1.0f;
			s.val[0] = log(z);
			cvSet2D (realpart, i, j, s);
		}
	}

 // scale image for display
    cvMinMaxLoc(realpart, &m, &M, NULL, NULL, NULL);
	printf ("Minimum value is %lf  max value is %lf\n", m, M);

// scale
	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (realpart, i, j);
			s.val[0] = (s.val[0]-m)/(M-m);
			cvSet2D (realpart, i, j, s);
		} 
		  
// display image in window
	cvNamedWindow(title, 0);
    cvShowImage( title, realpart );
    cvWaitKey (0);
	cvReleaseImage( &realpart );
	cvDestroyWindow (title);
}

void origin_center( IplImage *array )
{
    int  i, j;
	CvScalar s;

     for( i = 0; i < array->height; i++ )
          for( j =0; j < array->width; j++ )
               if( (i + j) % 2 )  
			   {
					s = cvGet2D (array, i, j);
					s.val[0] = -s.val[0];
					s.val[1] = -s.val[1];
					cvSet2D (array, i, j, s);
               }

}

IplImage *fftImage (IplImage *img)
{
	IplImage *realpart, *imgpart, *complexpart, *ret;
	CvMat *ft;
	int sizeM, sizeN;
	CvMat tmp;
	double xmax = 0.0;
	CvScalar s;
	int i,j;

    realpart = cvCreateImage( cvGetSize(img), IPL_DEPTH_64F, 1);
    imgpart = cvCreateImage( cvGetSize(img), IPL_DEPTH_64F, 1);
	complexpart = cvCreateImage( cvGetSize(img), IPL_DEPTH_64F, 2);	
    cvScale(img, realpart, 1.0, 0.0);              // copy grey input image to realpart
	if(NORMALIZE)
	{
		for (i=0; i<img->height; i++)
			for (j=0; j<img->width; j++)
		{
			s = cvGet2D (realpart, i, j);
			if (s.val[0] > xmax) xmax = s.val[0];
		}
		for (i=0; i<img->height; i++)
			for (j=0; j<img->width; j++)
		{
			s = cvGet2D (realpart, i, j);
			s.val[0] /= xmax;
			cvSet2D (realpart, i, j, s);
		}
	}
    cvZero(imgpart);                                   // Set imaginary part to 0
    cvMerge(realpart, imgpart, NULL, NULL, complexpart); // real+imag to complex
		  			  
/* Messy bit - fft needs sizes to be a power of 2, so the images have to be
// embedded into a background of 0 pixels. */
	sizeM = cvGetOptimalDFTSize( img->height - 1 );
	sizeN = cvGetOptimalDFTSize( img->width - 1 );
	printf ("Size of image to be transformed is %dx%d\n", sizeM, sizeN);

	ft = cvCreateMat( sizeM, sizeN, CV_64FC2 );
    origin_center (complexpart);

// copy A to dft_A and pad dft_A with zeros
    cvGetSubRect( ft, &tmp, cvRect(0,0, img->width, img->height));  // tmp points to sub of dft_A
    cvCopy( complexpart, &tmp, NULL );                                       // Copy complex image into sub of dft_A
    cvGetSubRect( ft, &tmp, cvRect(img->width,0, 
		                   ft->cols - img->width, img->height));   // Get sub of dft_A on right side
    if ((ft->cols - img->width) > 0) cvZero( &tmp );                   // Set right margin to zero
			  

    cvDFT( ft, ft, CV_DXT_FORWARD, complexpart->height );

    ret = cvMatToImage (ft);
	cvReleaseMat(&ft);
    cvReleaseImage( &realpart );
    cvReleaseImage( &imgpart );
    cvReleaseImage( &complexpart);
	return ret;
}

IplImage *fftImageInv (IplImage *img)
{
	IplImage *ret;
	CvMat *ft;
	CvMat tmp;

	ft = cvCreateMat( img->height, img->width, CV_64FC2 );
    origin_center (img);

// copy A to dft_A and pad dft_A with zeros
    cvGetSubRect( ft, &tmp, cvRect(0,0, img->width, img->height));  // tmp points to sub of dft_A
    cvCopy( img, &tmp, NULL );                                       // Copy complex image into sub of dft_A
    cvDFT( ft, ft, (/*CV_DXT_SCALE|*/CV_DXT_INVERSE), img->height );

    ret = cvMatToImage (ft);
	cvReleaseMat(&ft);
	printf ("INVFFT Return image is: Depth=%d channels=%d\n", ret->depth, ret->nChannels);
	origin_center (img);
	return ret;
 }

int vlog2 (int x)
{
	int k,j;

	j = 0; k=1;
	while (k<x) {
		k *= 2;
		j += 1;
	}
	return j;
}

void saveImage (char *name, IplImage *x)
{
	IplImage *realpart, *savepart;
	CvScalar s;
	int i,j;
	double z, y, m, M;

// Split Fourier in real and imaginary parts
	realpart = cvCreateImage( cvSize(x->height, x->width), IPL_DEPTH_64F, 1);
	savepart = cvCreateImage( cvSize(x->height, x->width), IPL_DEPTH_8U, 1);
		
// Compute the magnitude of the transform  = log((norm) + 1)
	for (i=0; i<x->height; i++)
	{
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			z = s.val[0];
			y = s.val[1];
			z = sqrt(z*z+y*y) + 1.0f;
			s.val[0] = log(z);
			cvSet2D (realpart, i, j, s);
		}
	}

 // scale image for display
    cvMinMaxLoc(realpart, &m, &M, NULL, NULL, NULL);
	printf ("Minimum value is %lf  max value is %lf\n", m, M);

// scale
	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (realpart, i, j);
			s.val[0] = (s.val[0]-m)/(M-m) * 255;
			cvSet2D (savepart, i, j, s);
		} 
		  
// Save image
    cvSaveImage (name, savepart);
	cvReleaseImage( &realpart );
	cvReleaseImage( &savepart );

}


