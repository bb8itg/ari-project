int main( int argc, char** argv )
{
  IplImage* img = NULL;       
  IplImage *psf, *psfa;
  IplImage *grayImg, *z, *zz;
  int i,j;
  CvScalar s, s1;
  double p, q;
  CvMat* a;

  /* Initialize data array - data[k] = 2 sin (2*pi*k/128) */
  a = cvCreateMat( 1024, 1, CV_32FC2 );

    for (i=0; i<1024; i++)
	{
		s.val[0] = 2.0 * sin (2.0*PI*i/128);
		s.val[1] = 0.0;
		cvSet1D(a, i, s);
	}

	cvDFT(a,a,CV_DXT_FORWARD,0);
	for (i=0; i<256; i++)
	{
		s  = cvGet1D (a, i);
		s1 = cvGet1D (a, i+256);
		printf ("%d %f   %d %f    ", i, s.val[0], i*2, s1.val[0]);
		s  = cvGet1D (a, i+512);
		s1 = cvGet1D (a, i+768);
		printf ("%d %f   %d %f\n", i, s.val[0], i*2, s1.val[0]);
	}
    scanf ("%d", &j);
	cvDFT(a,a,CV_DXT_INVERSE_SCALE,0); 

    for (i=0; i<1024; i++)
	{
	  s = cvGet1D(a, i);
	  p = 2.0 * sin (2.0*PI*i/128);
	  printf ("%d  %lf  %lf  %lf\n", i, p, s.val[0], p-s.val[0]);
	}
	
	return 0;
}