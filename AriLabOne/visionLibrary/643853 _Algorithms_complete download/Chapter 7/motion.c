/*
			    Motion Blur Removal
*/
#define MAX
#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <stdlib.h>
#include <cv.h>
#include <highgui.h>
#include <stdlib.h>

float ** f2d (int nr, int nc);

void saveImage (char *name, IplImage *x)
{
	IplImage *realpart, *savepart;
	CvScalar s;
	int i,j;
	double z, y, m, M;

// Split Fourier in real and imaginary parts
	realpart = cvCreateImage( cvSize(x->height, x->width), IPL_DEPTH_64F, 1);
	savepart = cvCreateImage( cvSize(x->height, x->width), IPL_DEPTH_8U, 1);
		
// Compute the magnitude of the transform  = log((norm) + 1)
	for (i=0; i<x->height; i++)
	{
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			z = s.val[0];
			y = s.val[1];
			z = sqrt(z*z+y*y) + 1.0f;
			s.val[0] = log(z);
			cvSet2D (realpart, i, j, s);
		}
	}

 // scale image for display
    cvMinMaxLoc(realpart, &m, &M, NULL, NULL, NULL);
	printf ("Minimum value is %lf  max value is %lf\n", m, M);

// scale
	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (realpart, i, j);
			s.val[0] = (s.val[0]-m)/(M-m) * 255;
			cvSet2D (savepart, i, j, s);
		} 
		  
// Save image
    cvSaveImage (name, savepart);
	cvReleaseImage( &realpart );
	cvReleaseImage( &savepart );
}

void toGrey (IplImage **y)
{
	IplImage *g, *x;

	x = *y;
	g = cvCreateImage(cvSize(x->width,x->height), x->depth, 1);
	g->origin = x->origin; 
	cvCvtColor(x, g, CV_BGR2GRAY);
	cvReleaseImage(y);
	*y = g;
}



/* H:\AIPCV\CH6\SKYM.PGM */
int main (int argc, char *argv[])
{
	int n=0, nr, nc;
	IplImage *im1, *im2;
	float **p;
	float xspeed;
	double mean_val=0.0, sum1=0.0, sum2=0.0, xmax=0.0, xmin=0.0, xd=0.0;
	int i,j,k, x, m, y, jind, K, a, iter = 0;
	char filename[128];
	CvScalar s, t;

	printf ("Image Restoration System module 11 - Motion Blur\n");

// Try to open standard image files.  
	printf ("Input file name: ");
	scanf ("%s", filename);
	im1 = cvLoadImage(filename, CV_LOAD_IMAGE_UNCHANGED);
	if (im1 == 0)
	{
	  printf ("No such file as '%s'\n", filename);
	  exit(1);
	}

	printf ("Enter speed: ");
	scanf ("%f", &xspeed);

	cvNamedWindow("Original", 0);
	cvShowImage( "Original", im1 );
	cvWaitKey (0);
	if (im1->nChannels > 1) toGrey (&im1);
	im2 = cvCloneImage (im1);

	nr = im1->height; nc = im1->width;
	p = f2d (nr, nc);
	mean_val = 0.0;
	xspeed = xspeed - 10;

	do
	{
		iter += 1;
		for (i=0; i<nr; i++)
			for (j=0; j<nc; j++)
			{
				p[i][j] = 0;
				s = cvGet2D (im1, i, j);
				mean_val += s.val[0]; 
			}
		mean_val = mean_val/(float)(nc*nr);

/* Restore */
		a = (int)(xspeed+0.1);
		K = nc/a;
		xmax = -1000.0; xmin = -xmax;
		for (y=0; y<nr; y++)
			for (x=0; x<nc; x++)
			{
				m = (int)(x/a);
				if (m > K-1) continue;

				sum1 = 0.0;
				for (k=0; k<=K-1; k++)
					for (j=0; j<=k; j++)
					{
						jind = x-m*a + (k-j)*a;
						if (jind < nc && jind > 0)
						{
							s = cvGet2D (im1, y, jind);
							t = cvGet2D (im1, y, jind-1);
							sum1 = sum1 + (s.val[0] - t.val[0]); 
						}
					}
				sum1 = sum1/(float)K;

				sum2 = 0.0;
				for (j = 0; j<=m; j++)
				{
					jind = x-j*a;
					if (jind < nc && jind > 0)
					{
						s = cvGet2D (im1, y, jind);
						t = cvGet2D (im1, y, jind-1);
						sum2 += (s.val[0] - t.val[0]);  
					}
				}

			sum1  = mean_val - sum1 + sum2; 
			p[y][x] = (float)sum1; 
			if (xmax < sum1) xmax = sum1;
			if (xmin > sum1) xmin = sum1;
		}
		xd = xmax - xmin;

		for (y=0; y<nr; y++)
			for (x=0; x<nc; x++)
			{
				s.val[0] = ( ((p[y][x]-xmin)/xd) * 255 );
				cvSet2D (im1, y, x, s);
			}

//	Output_PBM (im1, argv[3]);
			printf ("This iteration: xspeed = %f\n", xspeed);
		cvShowImage( "Original", im1 );
	    cvWaitKey (0);
		cvCopyImage( im2, im1 );
		xspeed += 1.0;
	} while (iter < 20);

	saveImage ("h:\\aipcv\\ch6\\motion.pgm", im1);
}

float ** f2d (int nr, int nc)
{
	float **x, *y;
	int i;

	x = (float **)calloc ( nr, sizeof (float *) );
	y = (float *) calloc ( nr*nc, sizeof (float)  );
	if ( (x==0) || (y==0) )
	{
	  fprintf (stderr, "Out of storage: F2D.\n");
	  exit (1);
	}
	for (i=0; i<nr; i++)
	  x[i] = y+i*nc;
	return x;
}

