/* Identify the skew angle by connecting the bottom center points
   on the bounding boxes of the connected components.             */

#include <stdlib.h>
#include <stdio.h>
#include <cv.h>
#include <highgui.h>

#define OBJECT 1
#define BACK 0
#define SIZE 20

void bbox (IplImage *x, int I, int J, int val, int *ulr,
	int *ulc, int *lrr, int *lrc)
{
	int i,j,ip1,jp1,ip2,jp2;
	int is, ie, js, je;
	CvScalar s;
	 
	ip1 = 10000;    jp1 = 10000;
	ip2 = -1;       jp2 = -1;
 
	if (I-SIZE < 0) is = 0;
	  else is = I-SIZE;
	if (I+SIZE>=x->height) ie = x->height-1;
	  else ie = I+SIZE;
	if (J-SIZE < 0) js = 0;
	  else js = J-SIZE;
	if (J+SIZE >= x->width) je = x->width-1;
	  else je = J+SIZE;

 /* Find the min and max coordinates, both row and column */
	 for (i=is; i<=ie; i++)
	   for(j=js; j<=je; j++)
	   {
		 s = cvGet2D (x, i,j);
	     if (s.val[0] == val) 
	     {
	       if (i < ip1) ip1 = i;
	       if (i > ip2) ip2 = i;
	       if (j < jp1) jp1 = j;
	       if (j > jp2) jp2 = j;
	     }
	   }

	if (jp2 < 0) return;
 
	*ulr = ip1; *ulc = jp1;
	*lrr = ip2; *lrc = jp2;
 }

void unmark (IplImage *im, int is, int js, int ie, int je, int val)
{
	int i,j;
	CvScalar s;

	for (i=is; i<=ie; i++)
	  for (j=js; j<=je; j++)
	  {
		  s = cvGet2D( im, i, j);
		  if (s.val[0] == val)
		  {
			  s.val[0] = BACK;
			  cvSet2D (im, i, j, s);
		  }
	  }
}

void mark (IplImage *im, int row, int col, int MARK)
{
	CvScalar s, t;
	s.val[0] = MARK;
	cvSet2D (im, row, col, s);

	if (row-1>=0 && col-1>=0)
	{
		t = cvGet2D (im, row-1, col-1);
		if (t.val[0] == OBJECT) mark (im, row-1,col-1, MARK);  
	}

	if (row-1>=0)
	{
		t = cvGet2D (im, row-1, col);
		if (t.val[0] == OBJECT) mark (im, row-1,col, MARK);  
	}

	if (row-1>=0 && col+1<im->width)
	{
		t = cvGet2D (im, row-1, col+1);
		if (t.val[0] == OBJECT) mark (im, row-1,col+1, MARK);  
	}

	if (col-1>=0)
	{
		t = cvGet2D (im, row, col-1);
		if (t.val[0] == OBJECT) mark (im, row,col-1, MARK);  
	}

	if (col+1<im->width)
	{
		t = cvGet2D (im, row, col+1);
		if (t.val[0] == OBJECT) mark (im, row,col+1, MARK);  
	}

	if (row+1<im->height && col-1>=0)
	{
		t = cvGet2D (im, row+1, col-1);
		if (t.val[0] == OBJECT) mark (im, row+1,col-1, MARK);  
	}

	if (row+1<im->height)
	{
		t = cvGet2D (im, row+1, col);
		if (t.val[0] == OBJECT) mark (im, row+1,col, MARK);  
	}

	if (row+1<im->height && col+1<im->width)
	{
		t = cvGet2D (im, row+1, col+1);
		if (t.val[0] == OBJECT) mark (im, row+1,col+1, MARK);  
	}
}

void markcc (IplImage *im, IplImage *im2)
{
	int i,j, ulr, ulc, lrr, lrc;
	CvScalar s;

	for (i=0; i<im->height; i++)
	{
	  for (j=0; j<im->width; j++)
	  {
		  s = cvGet2D (im, i, j);
		  if (s.val[0] == OBJECT)
		  {
			  mark (im, i, j, 2);
			  bbox (im, i, j, 2, &ulr, &ulc, &lrr, &lrc);
			  s.val[0] = 1;
			  cvSet2D (im2, lrr, (lrc+ulc)/2, s);
			  unmark (im, ulr, ulc, lrr, lrc, 2);
		  }
	  }
	}
}

int main (int argc, char *argv[])
{
	char filename[128], path[128];
	IplImage *img, *img2;
	int i, j;
	CvScalar s, t;

	printf ("Baird algorithm for skew angle detection.\n Enter image file name: ");
	scanf ("%s", filename);
	sprintf (path, "C:\\AIPCV\\ch9\\%s", filename);
	cvNamedWindow("Baird", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("Baird", 100, 100);

// Display the image
	img = cvLoadImage (path, 0);
	if (img == NULL)
	{
	  printf ("Usage: baird <image> \n");
	  exit (1);
	}
	cvShowImage("Baird", img );
	cvWaitKey(0);

	// Convert levels. Create a copy for the working image.

	img2 = cvCloneImage(img);

	// Set to all black.
	s.val[0] = s.val[1] = s.val[2] = s.val[3] = 0;
	for (i=0; i<img->height; i++)
	  for (j=0; j<img->width; j++)
	  {
		  t = cvGet2D (img, i,j);
		  if (t.val[0] < 128) {t.val[0] = 1; cvSet2D(img, i, j, t); }
		  else {t.val[0] = 0; cvSet2D(img, i,j, t); }
		  cvSet2D (img2, i, j, s);
	  }

	// Now find and mark all characters.
	markcc (img, img2);

	for (i=0; i<img->height; i++)
	  for (j=0; j<img->width; j++)
	  {
		  s = cvGet2D (img2, i,j);
		  if (s.val[0] >= 1)
			  s.val[0] = 255;
		  cvSet2D (img2, i, j, s);
	  }


	printf ("Saving the output in 'baird.tif'.\n");
	cvSaveImage ("C:\\AIPCV\\ch9\\baird.tif", img2);
	cvShowImage ("Baird", img2);
	cvWaitKey(0);

	printf ("Now find the skew angle using 'hskew'\n");

	cvDestroyWindow("Baird");
    cvReleaseImage(&img);
	cvReleaseImage(&img2);
}
