/* Vectorization of a glyph for later recognition */

#define MAX
#include "c:\aipcv\ch9\lib.h"
#include <stdlib.h>
#include <stdio.h>
#include <cv.h>
#include <highgui.h>


#define OBJECT 1
#define BACK 0
#define MAXPL 12
#define MAXVL 12
#define VDT 0

struct pixlist 
{
	int N, n;
	int **list;
};
typedef struct pixlist * PIXLIST;
PIXLIST allpix[MAXPL], plist;

struct vle 
{
	int r1,c1,r2,c2;
	struct vle *next;
};
typedef struct vle * VECLIST;
VECLIST allvecs[MAXVL], vlist;
int NEXTV = 1;
IplImage *mim;

void bound (IplImage *im);
void extract (IplImage *im, PIXLIST *p);
void vectorize (PIXLIST p, VECLIST *v);
void confirm (VECLIST v, IplImage *im, IplImage *im2);
PIXLIST newp (int N);
void chain (IplImage *x, int I, int J, PIXLIST c, int pmax, int *nn);
int nay4 (IplImage *im, int I, int J);
VECLIST newv ();
void outlist (VECLIST p);
float checkd (PIXLIST p, int start, int end);
int ptldist (int ax, int ay, int bx, int by, int px, int py);
void vmark (PIXLIST p, int st, int en);

int main (int argc, char *argv[])
{
	IplImage *im;

	int i, j, k;

	CvScalar s, w;

	cvNamedWindow("Vector Templates", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("Vector Templates", 100, 100);

// Display the image
	im = cvLoadImage ("C:\\AIPCV\\ch9\\B1.pbm", 0);
	if (im == NULL)
	{
		printf ("Can't open the Skew image file 'B1.pbm'.\n");
	    return (1);
	}
	cvShowImage("Vector Templates", im );
	cvWaitKey(0);

	s.val[0] = 0; w.val[0] = 1;
	mim = cvCloneImage (im);
	for (i=0; i<im->height; i++)
	  for (j=0; j<im->width; j++)
	  {
		  cvSet2D(mim, i,j,s);
		  if (cvGet2D(im, i,j).val[0] < 128) 
			  cvSet2D (im, i,j, w);
		  else
			  cvSet2D(im,i,j,s);
	  }

/* Identify the boundary */
	bound (im);

	k = 0;
	do 
	{
	  printf ("GROUP %d:\n\n", k);

/* Extract the boundary */
	  extract (im, &plist);
	  if (plist->n <= 0) break;

	  printf ("Boundary:\n");
	  for (i=0; plist->list[0][i]>=0; i++)
	    printf ("[%3d, %3d]\n", plist->list[0][i], plist->list[1][i]);

/* Vectorize */
	  printf ("Vector segments:\n");
	  vectorize (plist, &vlist);
	  outlist (vlist);

	  cvShowImage ("Vector Templates", im );
	  cvWaitKey(0);
	  cvShowImage("Vector Templates", mim );
	  cvWaitKey(0);
	  allvecs[k++] = vlist;
	  break;
	} while (1);


}


/* Delete pixels that are NOT on an object outline (boundary) */
void bound (IplImage *im)
{
	int i,j;
	IplImage *x;
	CvScalar s;

	x = cvCloneImage (im);
//	for (i=0; i<im->info->nr; i++)
//	  for (j=0; j<im->info->nc; j++)
//	    x->data[i][j] = im->data[i][j];

	s.val[0] = BACK;
	for (i=0; i<x->height; i++)
	  for (j=0; j<x->width; j++)
	    if (cvGet2D(x, i,j).val[0] == OBJECT && nay4(x, i, j) == 4)
		  cvSet2D (im, i,j,s);

	cvReleaseImage (&x);
}

int nay4 (IplImage *im, int I, int J)
{
	int n=0;

	if ( (I-1 >= 0) && cvGet2D(im, I-1, J).val[0] == OBJECT) n++;
	if ( (J-1 >= 0) && cvGet2D(im, I, J-1).val[0] == OBJECT) n++;
	if ( (I+1 < im->height) && cvGet2D(im, I+1, J).val[0] == OBJECT) n++;
	if ( (J+1 < im->width)  && cvGet2D(im, I, J+1).val[0] == OBJECT) n++;
	return n;
}

/* Convert the boundary image into a list of pixels */
void extract (IplImage *im, PIXLIST *p)
{
	int i, j, k, nn;
	PIXLIST q;

	k = im->height * im->width;
	q = newp (k);

	for (i=0; i<im->height; i++)
	  for (j=0; j<im->width; j++)
	    if (cvGet2D(im, i, j).val[0] == OBJECT)
	    {
		 chain (im, i, j, q, k, &nn);
		 *p = q;
		 q->n = nn;
		 return;
	    }
}

/* Convert the list of pixels into a set of line segments */
void vectorize (PIXLIST p,  VECLIST *v)
{
	int i, si;
	VECLIST w, lw;

	lw = w = newv ();
	
	i = 0;
	while (i<p->n)
	{
	  si = i++;
	  if (p->list[0][i] < 0) break;
	  do
	  {
	    i++;
	    if (p->list[0][i]<0 || checkd (p, si, i) > VDT)
	    {
	      i--;
	      w->r1 = p->list[0][si]; w->c1 = p->list[1][si];
	      w->r2 = p->list[0][i];  w->c2 = p->list[1][i];
	      vmark (p, si, i);

	      printf ("Vector: [%3d,%3d]->[%3d,%3d].\n",w->r1,w->c1,w->r2,w->c2);

	      w->next = newv();
	      w = w->next;
	      w->next = 0; w->r1=w->r2=w->c1=w->c2 = -1;
	      break;
	    }
	  } while (i<p->n);
	}
	*v = lw;
}

void vmark (PIXLIST p, int st, int en)
{
	int i;
	CvScalar s;

	s.val[0] = NEXTV;
	for (i=st; i<=en; i++)
	  cvSet2D (mim, p->list[0][i], p->list[1][i], s);

	NEXTV++;
}

float checkd (PIXLIST p, int start, int end)
{
	int i;
	float d, dmax=0.0;

	printf ("Checking points %d thru %d. Line [%d,%d]->[%d,%d]\n",
		start, end, p->list[0][start], p->list[1][start],
		p->list[0][end], p->list[1][end]);

	for (i=start+1; i<end; i++)
	{
	  d = (float)ptldist (p->list[0][start], p->list[1][start], 
		      p->list[0][end], p->list[1][end],
		      p->list[0][i], p->list[1][i]);
	  if (d > dmax) dmax = d;
	}
	printf ("Max distance seen was %f\n", dmax);

	return dmax;
}

/* Perpendicular distance of the point (px,py) from the line between the
   two points (ax,ay) and (bx, by).					*/

int ptldist (int ax, int ay, int bx, int by, int px, int py)
{
	int vp[2], vl[2],t,d,vb[2],s;
	int vr[2];

	vp[0] = px-ax;  vp[1] = py-ay;
	vl[0] = bx-ax;  vl[1] = by-ay;
	t = vp[0]*vl[0] + vp[1]*vl[1];
	if (t<0) d = vp[0]*vp[0] + vp[1]*vp[1];
	else {
          s = vl[0]*vl[0] + vl[1]*vl[1];
	  if (t>s || s<= 0) {
	    vb[0] = px-bx;      vb[1] = py-by;
	    d = vb[0]*vb[0] + vb[1]*vb[1];
	  } else {
	    vr[0] = vp[0] - t*vl[0]/s;  
	    vr[1] = vp[1] - t*vl[1]/s;
	    vr[0] = vp[0]*vp[0] -(2*vp[0]*t*vl[0])/s + 
		(t*t*vl[0]*vl[0])/(s*s);
	    vr[1] = vp[1]*vp[1] -(2*vp[1]*t*vl[1])/s + 
		(t*t*vl[1]*vl[1])/(s*s);
	    return vr[0]+vr[1];
	  }
	}
	return d;
}

int range (IplImage *im, int i, int j)
{
	if (i<0 || i>=im->height) return 0;
	if (j<0 || j>=im->width) return 0;
	return 1;
}


/*	Compute the chain code of the object beginning at pixel (i,j).
	Return the code as NN integers in the array C.			*/
void chain (IplImage *x, int I, int J, PIXLIST c, int pmax, int *nn)
{
	int val,n,m,q,r, di[9],dj[9],ii, d, dii;
	int lastdir, jj, i;
	CvScalar s;

/*	Table given index offset for each of the 8 directions.		*/
	di[0] = 0;	di[1] = -1;	di[2] = -1;	di[3] = -1;
	dj[0] = 1;	dj[1] = 1;	dj[2] = 0;	dj[3] = -1;
	di[4] = 0;	di[5] = 1;	di[6] = 1;	di[7] = 1;
	dj[4] = -1;	dj[5] = -1;	dj[6] = 0;	dj[7] = 1;

	for (ii=0; ii<pmax; ii++)
	{
	  c->list[0][ii] = -1;
	  c->list[1][ii] = -1;
	}
	val = (int)cvGet2D(x, I, J).val[0]; // x->data[I][J];
	q = I;	r = J;  lastdir = 4;

	c->list[0][0] = q;
	c->list[1][0] = r;
	n = 1;

	do
	{
	  m = 0;
	  dii = -1;	d = 100;
	  for (ii=lastdir+1; ii<lastdir+8; ii++) 
	  {	/* Look for next */
	    jj = ii%8;
	    if (range(x,di[jj]+q, dj[jj]+r))
	      if ( cvGet2D(x, di[jj]+q, dj[jj]+r).val[0] == val) 
	      {
		   dii = jj;	m = 1;
		   break;
		} 
	  }

	  if (m) 	/* Found a next pixel ... */
	  {
	    q += di[dii];	r += dj[dii];
	    if (n<pmax)
	    {
		c->list[0][n] = q;
		c->list[1][n] = r;
		n++;
	    }
	    lastdir = (dii+5)%8;
	  } else break;	/* NO next pixel */
	  if (n>pmax) break;
	} while ( (q!=I) || (r!=J) );	/* Stop when next to start pixel */

	s.val[0] = BACK;
	for (i=0; i<n; i++)
	  cvSet2D (x, c->list[0][i], c->list[1][i], s);

	*nn = n;
}

PIXLIST newp (int N)
{
	PIXLIST x;

	x = (PIXLIST)malloc (sizeof (struct pixlist));
	x->N = N;
	x->n = 0;
	x->list = (int **)malloc (sizeof(int *) * 2);
	x->list[0] = (int *)malloc (sizeof(int)*N);
	x->list[1] = (int *)malloc (sizeof(int)*N);
	return x;
}

VECLIST newv ()
{
	VECLIST x;

	x = (VECLIST) malloc (sizeof(struct vle));
	x->next = 0; x->r1 = x->r2 = x->c1 = x->c2 = -1;
	return x;
}

void outlist (VECLIST p)
{
	VECLIST q;

	q = p;
	while (q->next)
	{
	  printf ("[%3d,%3d] -> [%3d, %3d]\n", q->r1,q->c1,q->r2,q->c2);
	  q = q->next;
	}
}
