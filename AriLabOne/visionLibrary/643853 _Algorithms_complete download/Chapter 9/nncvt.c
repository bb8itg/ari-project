/*              Convert a glyph to 8x6 size for use by NNCLASS          */
#define MAX
#include "c:\aipcv\ch9\lib.h"
#include <stdlib.h>
#include <stdio.h>
#include <cv.h>
#include <highgui.h>
#include <math.h>

void scale_down (IplImage *z, int newr, int newc, IplImage **newim);
double bigger(double x);
void bbox (IplImage **x);                   
void normalize (IplImage *x);  

int main (int argc, char *argv[])
{
	int i,j;
	FILE *f;
	IplImage *im, *newim;
	char name[128], path[128];

	  printf ("  Convert a glyph image into a neural net input file.\n");
	  printf ("  This program converts the input image to one that\n");
	  printf ("  has the size 8 rows by 6 columns.\n\n");

	  printf ("Enter the image file name: ");
	  scanf ("%s", &name);
	  sprintf (path, "c:\\aipcv\\ch9\\%s", name);

	  // Display the image
	im = cvLoadImage (path, 0);
	if (im == NULL)
	{
		printf ("Can't open the input image file '%s'.\n", path);
	    return 1;
	}
	cvNamedWindow("nn", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("nn", 100, 100);
	cvShowImage("nn", im );
	cvWaitKey(0);

	f = fopen ("c:\\aipcv\\ch9\\nndataout", "w");         /* Open the required output file */
	if (f == NULL)
	{
	  printf ("Can't open the file 'nndataout' for output.\n", argv[2]);
	  return 1;
	}

	normalize(im);                  /* Convert grey levels to 0 - 255 */
	bbox (&im);                           /* Reduce input to min size */
	scale_down (im, 8, 6, &newim);

	for (i=0; i<8; i++)
	{
	  for (j=0; j<6; j++)
	    fprintf (f, "%f ", (float)(cvGet2D(newim, i, j).val[0])/255.0);
	  fprintf (f, "\n");
	}

	return 0;
}

void scale_down (IplImage *z, int newr, int newc, IplImage **newim)
{
	IplImage *y, *x;
	int i,j,ii,jj, k,colk,rowk, rs, cs;
	double rfactor, cfactor, accum, rw[25], cw[25], area, xf;
	CvScalar s;
	CvSize sz;

/* Create a new, smaller, image */
	sz.height = newr; sz.width = newc;
	y = cvCreateImage (sz, z->depth, z->nChannels);
	x = z;

/* Compute scale factor for rows and columns */
	rfactor = (double)newr/(double)(x->height);
	cfactor = (double)newc/(double)(x->width);
	rfactor = 1.0/rfactor;
	cfactor = 1.0/cfactor;
	area = cfactor*rfactor;

/* For each pixel in the new image compute a grey level 
   based on interpolation from the original image       */
	for (i=0; i<newr; i++)
	   for (j=0; j<newc; j++) 
	   {
/* Set up the row re-scale */
		rw[0] = bigger(i*rfactor) - i*rfactor;
		rs = (int) (floor(i*rfactor)+0.001);  k=1;
		xf = rfactor - rw[0];

		while (xf >= 1.0) 
		{
			rw[k++] = 1.0;
			xf = xf - 1.0;
		}
		if (xf < 0.0001) k--; 
		 else rw[k] = xf;
		rowk = k;

/* Set up the column re-scale */
		cw[0] = bigger(j*cfactor) - j*cfactor;
		cs = (int) (floor(j*cfactor)+0.001);  k=1;
		xf = cfactor - cw[0];

		while (xf >= 1.0) 
		{
			cw[k++] = 1.0;
			xf = xf - 1.0;
		}
		if (xf < 0.0001) k--; 
		 else cw[k] = xf;
		colk = k;

/* Collect and weight pixels from the original into the new pixel */
		accum = 0.0;
		for (ii=0; ii <= rowk; ii++) 
		{
		   if (ii+rs >= x->height) continue;
		   for (jj=0; jj<= colk; jj++) 
		   {
		      if (jj+cs >= x->width) continue;
		      accum += rw[ii]*cw[jj]*(cvGet2D(x, rs+ii, cs+jj).val[0]);
		   }
		}
		accum = accum/area;
		if (accum > 255.0) printf ("%lf at (%d,%d)\n",accum,i,j);

		s.val[0] = (accum + 0.5);
		cvSet2D(y, i,j,s);
	}
	*newim = y;
}

/*      Return the next bigger integer to the number X  */
double bigger(double x)
{
	double y;

	y = ceil(x);
	if (y == x) y = y + 1.0;
	return y;
}

void bbox (IplImage **x)
{
	int i, j, sr=10000, lr=0, sc=10000, lc=0, n, m;
	IplImage *im, *y;
	CvScalar s;
	CvSize sz;

	im = *x;
	for (i=0; i<im->height; i++)
	{
	  for (j=0; j<im->width; j++)
	  {
	     if (cvGet2D(im, i, j).val[0] == 0)
	     {
	       lr = i;
	       if (lc < j) lc = j;
	       if (sr > i) sr = i;
	       if (sc > j) sc = j;
	     }
	  }
	}
	
	n = lr-sr+1;    m = lc-sc+1;
	sz.height = n; sz.width = m;
	y = cvCreateImage (sz, im->depth, im->nChannels);

	for (i=sr; i<=lr; i++)
	  for (j=sc; j<=lc; j++)
	  {
		s = cvGet2D (im, i,j);
	    cvSet2D (y, i-sr, j-sc, s);
	  }
	cvReleaseImage (x);
	*x = y;
}

void normalize (IplImage *x)
{
	int i,j,vmin=256, vmax=0, k;
	float scale = 1.0;
	CvScalar s;

	for (i=0; i<x->height; i++)
	  for (j=0; j<x->width; j++)
	  {
	    if (cvGet2D(x, i,j).val[0] < vmin) vmin = cvGet2D(x, i,j).val[0];
	    if (cvGet2D(x, i,j).val[0] > vmax) vmax = cvGet2D(x, i,j).val[0];
	  }

	k = vmax-vmin;
	scale = (float)k;

	for (i=0; i<x->height; i++)
	  for (j=0; j<x->width; j++)
	  {
		  s.val[0] = 
		   (unsigned char)(((cvGet2D(x, i,j).val[0]-vmin)/scale)*255.0);
		  cvSet2D(x, i,j,s);
	  }
}

