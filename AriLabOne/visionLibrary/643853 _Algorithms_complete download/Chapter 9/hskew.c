/* Estimate the skew angle: Hough transform of low resolution smoothed image */

#include <stdlib.h>
#include <stdio.h>
#include <cv.h>
#include <highgui.h>
#include <math.h>

#define OBJECT 1
#define BACK 0

void hough (IplImage *x, float *theta);

void main (int argc, char *argv[])
{
	IplImage *img;
	float theta;

	cvNamedWindow("Skew", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("Skew", 100, 100);

// Display the image
	img = cvLoadImage ("C:\\AIPCV\\ch9\\baird.tif", 0);
	if (img == NULL)
	{
		printf ("Can't open the Skew image file: run 'baird.exe' first.\n");
	    exit (1);
	}
	cvShowImage("Skew", img );
	cvWaitKey(0);

	hough (img, &theta);
	printf ("Approximate skew angle is %10.5f\n", theta-90);
}

void hough (IplImage *x, float *theta)
{
//	float **z;
	IplImage *z;
	int center_x, center_y, r, omega, i, j, rmax, tmax;
	double conv;
	float tmval;
	CvScalar s;

	conv = 3.1415926535/180.0;
	center_x = x->width/2;       center_y = x->height/2;
	rmax = (int)(sqrt((double)(x->width*x->width+x->height*x->height))/2.0);

/* Create an image for the Hough space - choose your own sampling */
	// Allocate a 3-channel float image
	z = cvCreateImage(cvSize(2*rmax+1, 180),IPL_DEPTH_32F,1);

//	z = f2d (180, 2*rmax+1);
	s.val[0] = 0.0f;
	for (r = 0; r < 2 * rmax+1; r++)
	   for (omega = 0; omega < 180; omega++)
		cvSet2D (z, omega, r, s); // z[omega][r] = 0;

	tmax = 0; tmval = 0;
	for (i = 0; i < x->height; i++)
	  for (j = 0; j < x->width; j++)
		if (cvGet2D(x, i,j).val[0]!= 0 /*x->data[i][j]*/)
		   for (omega = 0; omega < 180; ++omega) 
		   {
			r = (i - center_y) * sin((double)(omega*conv)) 
			   + (j - center_x) * cos((double)(omega*conv));
/*                      if (r == 0) continue;  */
			s = cvGet2D(z, omega, rmax+r); //z[omega][rmax+r] += 1;
			s.val[0] += 1;
			cvSet2D(z, omega, rmax+r, s);
		   }

	for (i=0; i<180; i++)
	  for (j=0; j<2*rmax+1; j++)
	    if (cvGet2D(z, i,j).val[0] > tmval) // z[i][j] > tmval)
	    {
			tmval = cvGet2D(z, i,j).val[0]; // z[i][j];
			tmax = i;
	    }
	*theta = tmax;
	 cvReleaseImage(&z);
}

