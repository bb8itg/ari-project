/* kFill algorithm for noise reduction */

#include <stdlib.h>
#include <stdio.h>
#include <cv.h>
#include <highgui.h>
#include <math.h>

#define OBJECT 1
#define BACK 0
#define DEBUG 1

/* Prototypes - all in this file, alphabetical */
void kFill (IplImage *im, int K);
int kfiter (IplImage *im, int K, int val, IplImage *x);
int kfstep (IplImage *im, int K, int val, int I, int J, IplImage *x);
void mclear (IplImage *im, int val);
void kmark (IplImage *im, int I, int J, int val);

int main (int argc, char *argv[])
{
	IplImage *img;
	char name[128], path[128];
	int K, i,j;
	CvScalar s;

	cvNamedWindow("kFill", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("kFill", 100, 100);

// Get/display the image
	printf ("---- kFill:   Enter the name of the file: --------\n:: ");
	scanf ("%s", &name);
	sprintf (path, "C:\\AIPCV\\ch9\\%s", name);
	img = cvLoadImage (path, 0);
	if (img == NULL)
	{
		printf ("Can't open the image file '%s'.\n", path);
	    return 0;
	}
	cvShowImage("kFill", img );
	cvWaitKey(0);
	printf ("Enter the parameter K: ");
	scanf ("%d", &K);

// Convert 0/255 to 0/1
	s.val[0] = 1.0;
	for (i=0; i<img->height; i++)
		for (j=0; j<img->width; j++)
			if (cvGet2D(img, i,j).val[0] > 0)
				cvSet2D (img, i,j,s);

	kFill (img, K);

// Back to 0/255 so it can be displayed.
	s.val[0] = 255.0;
	for (i=0; i<img->height; i++)
		for (j=0; j<img->width; j++)
			if (cvGet2D(img, i,j).val[0] > 0)
				cvSet2D (img, i,j,s);

	cvShowImage("kFill", img );
	cvWaitKey(0);
	cvSaveImage ("C:\\AIPCV\\ch9\\kfill.jpg", img);
}


void kFill (IplImage *im, int K)
{
	int i,j,n;
	IplImage *x;


	x = cvCloneImage (im);
	do
	{
	  n =  kfiter (im, K, 0, x);
	  n += kfiter (im, K, 1, x);
	
	  for (i=0; i<im->height; i++)
	    for (j=0; j<im->width; j++)
			cvSet2D (im, i, j, cvGet2D (x, i, j));
	} while (n);

	cvReleaseImage(&x);
}

int kfiter (IplImage *im, int K, int val, IplImage *x)
{
	int i,j,k;

	k = 0;
	for (i=0; i<im->height-K; i++)
	{
	  for (j=0; j<im->width-K; j++)
	  {
	    k += kfstep (im, K, val, i, j, x);
	  }
	}
	return k;
}

int kfstep (IplImage *im, int K, int val, int I, int J, IplImage *x)
{
	int i,j, r, c, m, n;
	CvScalar s;

	m = 0;
/* Are all CORE pixels the opposite of VAL? */
	for (i=I+2; i<I+K-2; i++)
	  for (j = J+2; j<J+K-2; j++)
	    if (cvGet2D(im, i, j).val[0] == val) return 0;

/* Compute neighborhood parameters n, r, and c */
/* n is the number of VAL pixels */
	n = 0;
	for (i=I; i<I+K; i++)
	  for (j=J; j<J+K; j++)
	    if (i-I<2 || j-J<2 || i-I>=K-2 || j-J>=K-2)
		if (cvGet2D(im, i, j).val[0] == val) n++;

/* r is the number of VAL corner pixels */
	r = 0;
	if (cvGet2D(im, I, J).val[0] == val) r++;
	if (cvGet2D(im, I, J+K-1).val[0] == val) r++;
	if (cvGet2D(im, I+K-1, J).val[0] == val) r++;
	if (cvGet2D(im, I+K-1, J+K-1).val[0] == val) r++;

/* c is the number of connected regions of value VAL */

	c = 2;
	for (i=I; i<I+K; i++)
	  for (j=J; j<J+K; j++)
	    if (i-I<2 || j-J<2 || i-I>=K-2 || j-J>=K-2)
	      if (cvGet2D(im, i, j).val[0] == val)
	      {
			kmark (im, i, j, c);
			c++;
	      }
	c -= 2;
	mclear (im, val);

	if (c == 1 && ( (n>3*K-K/3) || (n==3*K-K/3 && (r==2)) ))
	{
	  for (i=I+2; i<I+K-2; i++)
	    for (j = J+2; j<J+K-2; j++)
		{
			s.val[0] = val;
			cvSet2D (x, i,j, s);
		}
	  m = 1;
	}

	return m;
}

void mclear (IplImage *im, int val)
{
	int i,j,f,q;
	CvScalar s;

	s.val[0] = val;
	q = 0;
	for (i=0; i<im->height; i++)
	{
	  f = 0;
	  for (j=0; j<im->width; j++)
	    if (cvGet2D(im, i, j).val[0] > 1)
	    {
			cvSet2D (im, i, j, s);
			q = 1;
			f = 1;
	    }
	  if (f == 0 && q!= 0) return;
	}
}

void kmark (IplImage *im, int I, int J, int val)
{
	int k;
	CvScalar s;

	k = (int)cvGet2D(im, I, J).val[0];
	s.val[0] = val;
	cvSet2D (im, I, J, s);

	if (I+1<im->height && cvGet2D(im, I+1, J).val[0]==k) kmark (im,I+1,J,val);
	if (I-1>=0         && cvGet2D(im, I-1,J).val[0]==k) kmark(im,I-1,J,val);
	if (J+1<im->width  && cvGet2D (im, I, J+1).val[0] ==k) kmark (im, I, J+1, val);
	if (J-1>=0 && cvGet2D(im, I, J-1).val[0] ==k) kmark (im, I, J-1, val);
}
