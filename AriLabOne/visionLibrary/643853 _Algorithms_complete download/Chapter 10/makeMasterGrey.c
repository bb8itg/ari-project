// Create a master file of grey features.
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <dirent.h>

#define PI 3.1415926535F

CvScalar rainbow[10];
int THRESHOLD = 40;
count = 0;
FILE *master;
char name[256];

struct quadtree
{
	struct quadtree *next[4];
	int uli, ulj, lri, lrj;
	float r, g, b;
};
typedef struct quadtree * qtptr;

struct features
{
	char name[128];		// Image file path
	float mean, sd, skew;	// Moments
	float qvec[4];		// Quad tree 4 
	float hvec[16];		// Quad tree 16
	float ed[25], bed[25], edir[25];	// Edge feaures
	float histo[256];		// Histogram
};
struct features fVector;
struct features * fptr;

// Maintain the master image collection
void initializeMaster ()
{
	master = fopen ("C:\\AIPCV\\greyFiles.txt", "r");
}

IplImage *nextImage ()
{
	char filename[128];
	IplImage *img = 0;
	int k=0;

	while (img == 0)
	{
		k = fscanf (master, "%s", name);
		k = fscanf (master, "%s", filename);
		if (k <= 0) return 0;

		strcat (name, "\\");
		strcat (name, filename);
		printf ("Image is %s%s\n", name, filename);
		img = cvLoadImage(name, 0);
		if (img) return img;
		printf ("----------------------Failed! ------------------\n");
	}
	return 0;
}

void freeImage (IplImage **p)
{
	cvReleaseImage(p);
}

// Compute the mean color over all pixels. Avoid black.
void moments (IplImage *x, CvScalar *t)
{
	double mean=0.0, sd=0.0, b=0.0;
	int i,j,k=0;
	CvScalar s;

// Mean
	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			mean += s.val[0];
			k++;
		}
		mean = t->val[0] = b/k;


// Standard deviation
	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			sd += (s.val[0]- mean)*(s.val[0]- mean);
			k++;
		}
		k--;
		sd = t->val[1] = sqrt(sd/k);

// Skewness
	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			b += (s.val[0]- sd)*(s.val[0]- sd)*(s.val[0]- sd);
			k++;
		}
		k--;

		t->val[2] = b/(k*sd*sd*sd);
}

qtptr newQuad (int uli, int ulj, int lri, int lrj)
{
	qtptr p;

	p = (qtptr)malloc (sizeof (struct quadtree));
	p->lri = lri; p->lrj = lrj;
	p->uli = uli; p->ulj = ulj;
	p->r = p->b = p->g = 0.0;
	p->next[0] = p->next[1] = p->next[2] = p->next[3] = (qtptr)0;
	return p;
}

void freeQuadTree (qtptr p)
{
	if (p->next[0]) freeQuadTree (p->next[0]);
	if (p->next[1]) freeQuadTree (p->next[1]);
	if (p->next[2]) freeQuadTree (p->next[2]);
	if (p->next[3]) freeQuadTree (p->next[3]);
	free (p);
}

void displayQuadTree (IplImage *im, int level, qtptr p)
{
	int i,j;
	CvScalar s;

	if (p == 0) return;

	if (level == 0)		// Display this level.
	{
		s.val[0] = p->b; s.val[1] = p->g; s.val[2] = p->r;
		for (i=p->uli; i<=p->lri; i++)
			for (j=p->ulj; j<=p->lrj; j++)
				cvSet2D (im, i, j, s);
		return;
	}
	displayQuadTree (im, level-1, p->next[0]);
	displayQuadTree (im, level-1, p->next[1]);
	displayQuadTree (im, level-1, p->next[2]);
	displayQuadTree (im, level-1, p->next[3]);
}

qtptr makeQuadTree (IplImage *im, int uli, int ulj, int lri, int lrj)
{
	qtptr a, b, c, d;		// Quads: a b
							//        c d
	CvScalar s;
	int di, dj;
	qtptr p;

	// A single pixel - bottom of the tree.
//	printf ("Making quad tree (%d,%d) to (%d,%d)\n", uli, ulj, lri, lrj);
	di = lri - uli; dj = lrj - ulj;
	if ( (uli==lri) && (ulj==lrj) )
	{
		a = newQuad (uli, ulj, lri, lrj);
		s = cvGet2D (im, uli, ulj);
		a->r = (float)s.val[2]; a->g = (float)s.val[1]; a->b = (float)s.val[0];
		a->uli = a->lri = uli;	a->lrj = a->ulj = ulj;
//		printf ("Single pixel: (%lf,%lf, %lf)\n", s.val[2], s.val[1], s.val[0]);
		return a;
	}

// 2x1 pixel groups
	if ((uli==lri) && (dj==1))	// Horizontal
	{
		a = newQuad (uli, ulj, uli, ulj);
		s = cvGet2D (im, uli, ulj);
		a->r = (float)s.val[2]; a->g = (float)s.val[1]; a->b = (float)s.val[0];
		a->uli = a->lri = uli;	a->lrj = a->ulj = ulj;

		b = newQuad (uli, lrj, uli, lrj);
		s = cvGet2D (im, uli, lrj);
		b->r = (float)s.val[2]; b->g = (float)s.val[1]; b->b = (float)s.val[0];
		b->uli = b->lri = uli;	b->lrj = b->ulj = lrj;

		p = newQuad (uli, ulj, lri, lrj);
		p->next[0] = a; p->next[1] = b;
		p->r = (a->r+b->r)/2.0f;  p->g = (a->g+b->g)/2.0f; p->b = (a->b+b->b)/2.0f;
		return p;
	}

	if ((di==1) && (ulj==lrj))  // Vertical
	{
		a = newQuad (uli, ulj, uli, ulj);
		s = cvGet2D (im, uli, ulj);
		a->r = (float)s.val[2]; a->g = (float)s.val[1]; a->b = (float)s.val[0];
		a->uli = a->lri = uli;	a->lrj = a->ulj = ulj;

		c = newQuad (lri, lrj, lri, lrj);
		s = cvGet2D (im, lri, lrj);
		c->r = (float)s.val[2]; c->g = (float)s.val[1]; c->b = (float)s.val[0];
		c->uli = a->lri = uli;	c->lrj = c->ulj = ulj;

		p = newQuad (uli, ulj, lri, lrj);
		p->next[0] = a; p->next[2] = c;
		p->r = (a->r+c->r)/2.0f;  p->g = (a->g+c->g)/2.0f; p->b = (a->b+c->b)/2.0f;
		return p;
	}

	di /= 2; dj /= 2;
	a = makeQuadTree (im, uli, ulj, uli+di, ulj+dj);
	b = makeQuadTree (im, uli, ulj+dj+1, uli+di, lrj);
	c = makeQuadTree (im, uli+di+1, ulj, lri, ulj+dj);
	d = makeQuadTree (im, uli+di+1, ulj+dj+1, lri, lrj);
	p = newQuad (uli, ulj, lri, lrj);
	p->next[0] = a; p->next[1] = b; p->next[2] = c; p->next[3] = d;
	p->r = (float)(a->r+b->r+c->r+d->r)/4.0f;
	p->g = (float)(a->g+b->g+c->g+d->g)/4.0f;
	p->b = (float)(a->b+b->b+c->b+d->b)/4.0f;
	return p;
}

qtptr colorQuadTree (IplImage *img)
{
	qtptr p;

	p = makeQuadTree (img, 0, 0, img->height-1, img->width-1);
	return p;
}

void quadColors (qtptr qt, float *qvec)
{
	int i = 0;

	qvec[i++] = qt->next[0]->b;
	qvec[i++] = qt->next[1]->b;
	qvec[i++] = qt->next[2]->b;
	qvec[i++] = qt->next[3]->b;

}

void hexColors (qtptr qt, float *hvec)
{
	quadColors (qt->next[0], &(hvec[0])  );
	quadColors (qt->next[1], &(hvec[4]) );
	quadColors (qt->next[2], &(hvec[8]) );
	quadColors (qt->next[3], &(hvec[12]) );
}

void greyHistogram (IplImage *img, float *histo)
{
//	IplImage *x;
	int i,j, k, n=0;
	CvScalar s;

	for (i=0; i<256; i++) histo[i] = 0;
	for (i=0; i<img->height; i++)
		for (j=0; j<img->width; j++)
		{
			s = cvGet2D (img, i, j);
			k = (int)s.val[0];
			histo[k] += 1; n++;
		}
	for (i=0; i<256; i++) histo[i] /= n;
}

#define pixel(x, i, j) cvGet2D(x,i,j).val[0]

void edgeDensity (IplImage *img, float *emean, float *bedmean, float *edmean)
{
	int i,j,di, dj;
	float i1,i2,i4,i6;
	float i0, i3, i5, i7, xx, y, mag, dir;
	float n[25];

	for (i=0; i<25; i++)
	{
		edmean[i] = 0.0f;
		bedmean[i] = 0.0f;
		emean[i] = 0.0f;
		n[i] = 0;
	}

	// break image into 5 x 5 regions
	di = img->height/5;  dj = img->width/5;

	for (i=1; i<img->height-1; i++)
	{
	  for (j=1; j<img->width-1; j++)
	  {
		i0 = (float)pixel(img, i-1, j-1); i1 = (float)pixel(img, i-1, j);
		i2 = (float)pixel(img, i-1, j+1); i3 = (float)pixel(img, i, j+1);
		i4 = (float)pixel(img, i+1, j+1); i5 = (float)pixel(img, i+1, j);
		i6 = (float)pixel(img, i+1, j-1); i7 = (float)pixel(img, i, j-1);
		xx = ((i2+2*i3+i4) - (i0+2*i7+i6));
		y =  ((i0+2*i1+i2) - (i6+2*i5+i4));
		mag = (float) sqrt((double)(xx*xx + y*y));
		if (xx > 255.0) mag = 255;
		 else if (xx<0)  mag = 0;
		emean[(i/di)*5 + j/dj] += mag; 
		n[(i/di)*5 + j/dj] += 1;

		if (mag > THRESHOLD) 
			bedmean[(i*5)/di + j/dj] += 1;

		 if (fabs(y) != 0.0)
			dir = (float)atan((double)xx/y);
		else
			dir = (float)(PI/2.0);
		dir = (dir + (PI/2.0)) / PI;
		dir *= 255.0F;
		if (dir > 255.0F) dir = 255.0F;
		  else if (dir < 0) dir = 0;
		edmean[(i/di)*5 + j/dj] +=  dir;
	  }
	}

	for (i=0; i<25; i++)
	{
		edmean[i] /= n[i];
		emean[i] /= n[i];
		bedmean[i] /=  n[i];
	}
}

int main(int argc, char *argv[])
{
	IplImage *img=0; 
	int count=0;
	CvScalar moms;
	qtptr qt;

	FILE *master;
	
	initializeMaster ();
	master = fopen ("C:\\AIPCV\\mastergrey.x", "wb");

   // create a window
	cvNamedWindow("win1", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("win1", 100, 100);

  while (img  = nextImage())
  {
// Measure color features
	  strcpy (fVector.name, name);

	  moments (img, &moms);			// Overall image moments
	  fVector.mean = (float)moms.val[0];
	  fVector.sd   = (float)moms.val[1];
	  fVector.skew = (float)moms.val[2];

	  qt = colorQuadTree (img);			// Quad tree
	  quadColors (qt, &(fVector.qvec));		// four levels of level 1
	  hexColors  (qt, &(fVector.hvec));		// 16 levels of level 2
	  freeQuadTree (qt);

	  edgeDensity (img, &(fVector.ed[0]), &(fVector.bed[0]), &(fVector.edir[0]));	// Edge features.
//	  fVector.ed = ed;
//	  fVector.bed = bed;
//	  fVector.edir = edir;

	  greyHistogram (img, fVector.histo);	// Grey level histogram 

	  cvShowImage("win1", img );
	  cvWaitKey(1);

	  fwrite (&(fVector), sizeof(struct features), 1, master); 
	  cvReleaseImage (&img);
  }
  fclose (master);
  return 0;
}