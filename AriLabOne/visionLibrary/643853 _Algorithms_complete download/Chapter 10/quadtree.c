// Quadtrees.
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <dirent.h>

CvScalar rainbow[10];
int THRESHOLD = 15;
count = 0;
FILE *master;
char name[256];

struct quadtree
{
	struct quadtree *next[4];
	int uli, ulj, lri, lrj;
	float r, g, b;
};
typedef struct quadtree * qtptr;

qtptr newQuad (int uli, int ulj, int lri, int lrj)
{
	qtptr p;

	p = (qtptr)malloc (sizeof (struct quadtree));
	p->lri = lri; p->lrj = lrj;
	p->uli = uli; p->ulj = ulj;
	p->r = p->b = p->g = 0.0;
	p->next[0] = p->next[1] = p->next[2] = p->next[3] = (qtptr)0;
	return p;
}

void freeQuadTree (qtptr p)
{
	if (p->next[0]) freeQuadTree (p->next[0]);
	if (p->next[1]) freeQuadTree (p->next[1]);
	if (p->next[2]) freeQuadTree (p->next[2]);
	if (p->next[3]) freeQuadTree (p->next[3]);
	free (p);
}

int range (IplImage *im, int i, int j)
{
	if ( (i>=0) && (i<im->height) && (j>=0) && (j<im->width) ) return 1;
	return 0;
}

void displayQuadTree (IplImage *im, int level, qtptr p)
{
	int i,j;
	CvScalar s;

	if (p == 0) return;

	if (level == 0)		// Display this level.
	{
		s.val[0] = p->b; s.val[1] = p->g; s.val[2] = p->r;
		for (i=p->uli; i<=p->lri; i++)
			for (j=p->ulj; j<=p->lrj; j++)
				if (range(im, i, j)) cvSet2D (im, i, j, s);
		return;
	}
	displayQuadTree (im, level-1, p->next[0]);
	displayQuadTree (im, level-1, p->next[1]);
	displayQuadTree (im, level-1, p->next[2]);
	displayQuadTree (im, level-1, p->next[3]);
}

qtptr makeQuadTree (IplImage *im, int uli, int ulj, int lri, int lrj)
{
	qtptr a, b, c, d;		// Quads: a b
							//        c d
	CvScalar s;
	int di, dj;
	qtptr p;

	// A single pixel - bottom of the tree.
//	printf ("Making quad tree (%d,%d) to (%d,%d)\n", uli, ulj, lri, lrj);
	di = lri - uli; dj = lrj - ulj;
	if ( (uli==lri) && (ulj==lrj) )
	{
		a = newQuad (uli, ulj, lri, lrj);
		s = cvGet2D (im, uli, ulj);
		a->r = (float)s.val[2]; a->g = (float)s.val[1]; a->b = (float)s.val[0];
		a->uli = a->lri = uli;	a->lrj = a->ulj = ulj;
//		printf ("Single pixel: (%lf,%lf, %lf)\n", s.val[2], s.val[1], s.val[0]);
		return a;
	}

// 2x1 pixel groups
	if ((uli==lri) && (dj==1))	// Horizontal
	{
		a = newQuad (uli, ulj, uli, ulj);
		s = cvGet2D (im, uli, ulj);
		a->r = (float)s.val[2]; a->g = (float)s.val[1]; a->b = (float)s.val[0];
		a->uli = a->lri = uli;	a->lrj = a->ulj = ulj;

		b = newQuad (uli, lrj, uli, lrj);
		s = cvGet2D (im, uli, lrj);
		b->r = (float)s.val[2]; b->g = (float)s.val[1]; b->b = (float)s.val[0];
		b->uli = b->lri = uli;	b->lrj = b->ulj = lrj;

		p = newQuad (uli, ulj, lri, lrj);
		p->next[0] = a; p->next[1] = b;
		p->r = (a->r+b->r)/2.0f;  p->g = (a->g+b->g)/2.0f; p->b = (a->b+b->b)/2.0f;
		return p;
	}

	if ((di==1) && (ulj==lrj))  // Vertical
	{
		a = newQuad (uli, ulj, uli, ulj);
		s = cvGet2D (im, uli, ulj);
		a->r = (float)s.val[2]; a->g = (float)s.val[1]; a->b = (float)s.val[0];
		a->uli = a->lri = uli;	a->lrj = a->ulj = ulj;

		c = newQuad (lri, lrj, lri, lrj);
		s = cvGet2D (im, lri, lrj);
		c->r = (float)s.val[2]; c->g = (float)s.val[1]; c->b = (float)s.val[0];
		c->uli = a->lri = uli;	c->lrj = c->ulj = ulj;

		p = newQuad (uli, ulj, lri, lrj);
		p->next[0] = a; p->next[2] = c;
		p->r = (a->r+c->r)/2.0f;  p->g = (a->g+c->g)/2.0f; p->b = (a->b+c->b)/2.0f;
		return p;
	}

	di /= 2; dj /= 2;
	a = makeQuadTree (im, uli, ulj, uli+di, ulj+dj);
	b = makeQuadTree (im, uli, ulj+dj+1, uli+di, lrj);
	c = makeQuadTree (im, uli+di+1, ulj, lri, ulj+dj);
	d = makeQuadTree (im, uli+di+1, ulj+dj+1, lri, lrj);
	p = newQuad (uli, ulj, lri, lrj);
	p->next[0] = a; p->next[1] = b; p->next[2] = c; p->next[3] = d;
	p->r = (float)(a->r+b->r+c->r+d->r)/4.0f;
	p->g = (float)(a->g+b->g+c->g+d->g)/4.0f;
	p->b = (float)(a->b+b->b+c->b+d->b)/4.0f;
	return p;
}

qtptr colorQuadTree (IplImage *img)
{
	qtptr p;

	p = makeQuadTree (img, 0, 0, img->height-1, img->width-1);
	return p;
}

void quadColors (qtptr qt, float *qvec)
{
	int i = 0;

	qvec[i++] = qt->next[0]->r;
	qvec[i++] = qt->next[0]->g;
	qvec[i++] = qt->next[0]->b;

	qvec[i++] = qt->next[1]->r;
	qvec[i++] = qt->next[1]->g;
	qvec[i++] = qt->next[1]->b;

	qvec[i++] = qt->next[2]->r;
	qvec[i++] = qt->next[2]->g;
	qvec[i++] = qt->next[2]->b;

	qvec[i++] = qt->next[3]->r;
	qvec[i++] = qt->next[3]->g;
	qvec[i++] = qt->next[3]->b;

}

void hexColors (qtptr qt, float *hvec)
{
	quadColors (qt->next[0], &(hvec[0])  );
	quadColors (qt->next[1], &(hvec[12]) );
	quadColors (qt->next[2], &(hvec[24]) );
	quadColors (qt->next[3], &(hvec[36]) );
}

int main(int argc, char *argv[])
{
	IplImage *img=0,*img2=0; 
	int level;
	char filename[256];
	qtptr qt;

	printf ("------ Quad tree display -----------\n");
	printf ("-- Enter image file name: ");
	scanf ("%s", filename);
	img = cvLoadImage (filename, 1);
	if (img == NULL)
	{
		printf (":: Error - can't open the file '%s'\n", filename);
		return 0;
	}

   // create a window, display image
	cvNamedWindow("Input", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("Input", 100, 100);
	cvShowImage ("Input", img);
	cvWaitKey (0);

	// Build the tree
	qt = colorQuadTree (img);

	// Build a new, empy image for the display.
 	img2 = cvCreateImage( cvSize(img->width, img->height), IPL_DEPTH_8U, 3);

	printf ("Enter the level for the display (0=root, max=pixels. - to exit): ");
	scanf ("%d", &level);

	while (level >= 0)
	{
	  displayQuadTree (img2, level, qt);	// Display the tree.
	  cvNamedWindow("Quad tree", CV_WINDOW_AUTOSIZE); 
	  cvMoveWindow("Quad tree", 100, 100);
	  cvShowImage ("Quad tree", img2);
	  cvWaitKey (0);

	  printf ("Enter the level for the display (0=root, max=pixels. - to exit): ");
	  scanf ("%d", &level);

	}
	freeQuadTree (qt);
	return 0;
}