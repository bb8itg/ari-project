/* check the master 3 text file against the binary binary/record file. */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <dirent.h>

struct featureVector 
{
	char name[256];		// image file name;
	double mean[3];		// Mean colors
	float qvec[12];		// First level quad tree
	float hvec[48];		// Second level quad tree
	float pvec[30];		// Prototype color histogram
	float hhisto[180];	// Hue histogram
	float chisto[16];
};
typedef struct featureVector * fptr;

FILE *g, *master;
char name[256];
int height=0, width=0;
int currentBin = -1;		// Which image directory (object) are we searching?

struct featureVector g_fv, g_bv;
fptr getMasterRecord (FILE *f)
{
	int i, k;

		k = fscanf (f, "%s", g_fv.name); if (k<=0) return 0;
		for (i=0; i<3; i++)
		  k = fscanf (f, "%lf", &(g_fv.mean[i]));			// Mean
		if (k<=0) return 0;

		for (i=0; i<12; i++)                      
		  k = fscanf (f, "%f", &(g_fv.qvec[i]));	// Quad colors
		if (k<=0) return 0;

		for (i=0; i<48; i++)
			k = fscanf (f, "%f", &(g_fv.hvec[i]));	// Hex colors
		if (k<=0) return 0;

		for (i=0; i<30; i++)
			k = fscanf (f, "%f", &(g_fv.pvec[i]));	// Prototype colors
		if (k<=0) return 0;

		for (i=0; i<180; i++)
			k = fscanf (f, "%f", &(g_fv.hhisto[i])); // Hue histogram
		if (k<=0) return 0;

		for (i=0; i<16; i++)
			k = fscanf (f, "%f", &(g_fv.chisto[i])); // Chroma histogram

		return &g_fv;
}

int compare ()
{
	int i;

	if (strcmp (g_fv.name, g_bv.name) != 0) return 1;
	for (i=0; i<3; i++)
		if (g_fv.mean[i] != g_bv.mean[i]) return 1;

	for (i=0; i<12; i++)                      
		if (g_fv.qvec[i] != g_bv.qvec[i]) return 1;

	for (i=0; i<48; i++)
		if (g_fv.hvec[i] != g_bv.hvec[i]) return 1;

	for (i=0; i<30; i++)
		if (g_fv.pvec[i] != g_bv.pvec[i]) return 1;

	for (i=0; i<180; i++)
		if (g_fv.hhisto[i] != g_bv.hhisto[i]) return 1;

	for (i=0; i<16; i++)
		if (g_fv.chisto[i] != g_bv.chisto[i]) return 1;
	return 0;
}

int main(int argc, char *argv[])
{
	int count=0, more=1, k=0, ii=0;
	FILE *f;

	printf ("------ Image Based Query (AIPCV 2010) ------\n");
	printf ("--------  Check binary master file ---------\n");

	// All file names in the data set lie on mater.txt
	f = fopen ("c:\\aipcv\\master4.txt", "r");
	if (f == NULL)
	{
		printf ("-- ERROR: can't open the master name file.\n");
		return 1;
	}
	g = fopen ("c:\\aipcv\\master4.bin", "rb");

	do
	{
		if (getMasterRecord (f))
		{
		  ii = fread ((&g_bv), sizeof(struct featureVector), 1, g);
		  if (ii < 1) break;
		  k = compare (g_fv, g_bv);
		  if (k) printf ("Error on record %d\n", count);
		  count++;
		}
		else break;
	} while (more);
	printf ("Count of records is %d\n", count);

	return 0;
}

