/* Convert the master 3 text file into a binary/record file. */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <dirent.h>

struct featureVector 
{
	char name[256];		// image file name;
	double mean[3];		// Mean colors
	float qvec[12];		// First level quad tree
	float hvec[48];		// Second level quad tree
	float pvec[30];		// Prototype color histogram
	float hhisto[180];	// Hue histogram
	float chisto[16];	// Chroma Histogram
};
typedef struct featureVector * fptr;

FILE *out, *master;
char name[256];
int height=0, width=0;
int currentBin = -1;		// Which image directory (object) are we searching?
int meanSuccess=0, quadSuccess=0, hexSuccess=0, protoSuccess=0, hueSuccess=0;
int overallSuccess = 0;


struct featureVector g_fv;
fptr getMasterRecord (FILE *f)
{
	int i, k;

		k = fscanf (f, "%s", g_fv.name); if (k<=0) return 0;
		for (i=0; i<3; i++)
		  k = fscanf (f, "%lf", &(g_fv.mean[i]));			// Mean
		if (k<=0) return 0;

		for (i=0; i<12; i++)                      
		  k = fscanf (f, "%f", &(g_fv.qvec[i]));	// Quad colors
		if (k<=0) return 0;

		for (i=0; i<48; i++)
			k = fscanf (f, "%f", &(g_fv.hvec[i]));	// Hex colors
		if (k<=0) return 0;

		for (i=0; i<30; i++)
			k = fscanf (f, "%f", &(g_fv.pvec[i]));	// Prototype colors
		if (k<=0) return 0;

		for (i=0; i<180; i++)
			k = fscanf (f, "%f", &(g_fv.hhisto[i])); // Hue histogram
		if (k<=0) return 0;

		for (i=0; i<16; i++)
			k = fscanf (f, "%f", &(g_fv.chisto[i])); // Chroma histogram

		return &g_fv;
}

int main(int argc, char *argv[])
{
	int count=0, more=1, nsamples=0, ii=0;
	FILE *f;

	printf ("------ Image Based Query (AIPCV 2010) ------\n");
	printf ("------  Convert master file to binary ------\n");

	// All file names in the data set lie on master.txt
	f = fopen ("c:\\aipcv\\master4.txt", "r");
	if (f == NULL)
	{
		printf ("-- ERROR: can't open the master name file.\n");
		return 1;
	}
	out = fopen ("c:\\aipcv\\master4.bin", "wb");

	do
	{
		if (more <= 0) break;
		if (getMasterRecord (f))
		 more = fwrite ((char *)(&g_fv), sizeof(struct featureVector), 1, out);
		else break;
	} while (more);

	return 0;
}

