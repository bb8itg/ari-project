// Search for all grey images and collect statistics.
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <dirent.h>

// Types of color feature. Index into feature array.
#define MEAN 0
#define QUAD 1
#define HEX  2
#define PROTO 3
#define HUE  4
#define CHROMA 5

int THRESHOLD = 15;			// RGB within this value of each other are grey
count = 0;
FILE *out, *master;
char name[256];
int height=0, width=0;
int currentBin = -1;		// Which image directory (object) are we searching?
int meanSuccess=0, quadSuccess=0, hexSuccess=0, 
    protoSuccess=0, hueSuccess=0, chromaSuccess=0;
int overallSuccess = 0;
int N=0;

struct features
{
	char name[128];		// Image file path
	float mean, sd, skew;	// Moments
	float qvec[4];		// Quad tree 4 
	float hvec[16];		// Quad tree 16
	float ed[25], bed[25], edir[25];	// Edge feaures
	float histo[256];		// Histogram
};
struct features fVector;
typedef struct features * fptr;


// A bucket data structure. Names and floats are added to the bucket, which
// keeps the best N values (best being smallest in this case)
struct bucketStructure 
{
	int N, n;			// Maximum Size, current size;
	float *value;		// Contents: in this case, a set of distances from a target.
	char **names;		// Names of files that correspond to values. Images.
};
typedef struct bucketStructure *bucket;   // pointer to a bucket

// Read one record from the master* file, containing an image file 
// and and a feature vector.
struct features local;
fptr readRecord (FILE *f)
{
	int k;

		k = fread (&local, sizeof(struct features), 1, f);
		if (k) return &local;
		else return 0;
}

// Euclidean distance between two 3 dimensional double precision vectors
float distance3d (double *a, double *b)
{
	int i;
	double d=0.0;

	for (i=0; i<3; i++)
		d += (a[i]-b[i])*(a[i]-b[i]);
	return (float)sqrt (d);
}

// Euclidean distance between two N dimenstional floating point vectors
float distanceNf (float *a, float *b, int N)
{
	int i;
	float d = 0.0f;

	for (i=0; i<N; i++)
		d += ((a[i]-b[i])*(a[i]-b[i]));
	return (float)sqrt((double)d);
}

// Compare two feature sets 'a' and 'b'; compute distances by feature type
float compareFV (fptr a, fptr b)
{
	double av[256], bv[256], d1=99999, d2=99999;
	int i;

	// Moments
	/*
	av[0] = a->mean/255.0; 
	bv[0] = b->mean/255.0;
	av[1] = a->sd/90.0; 
	bv[1] = b->sd/90.0;
	av[2] = a->skew/10.0; 
	bv[2] = b->skew/10.0;
	d1 = distance3d (av, bv);
	*/

	d2 = distanceNf (a->edir, b->edir, 25);
	return (float)(d2);
/*
	av[0] = a->histo[0];
	bv[0] = b->histo[0];
	for (i=1; i<256; i++)
	{
		av[i] = av[i-1] + a->histo[i];
		bv[i] = bv[i-1] + b->histo[i]; 
	}
	d2 = distanceNf (a->histo, b->histo, 256);
	return (float)(d2); */

//	av[0] = a->ed/40;		bv[0] = b->ed/40;
//	av[1] = a->edir/180;	bv[1] = b->edir/180;
//	av[2] = bv[2] = 0.0;
// 	printf ("%f %f\n", bv[0], bv[1]);

//	d1 = (a->bed-b->bed)*(a->bed-b->bed);
//	return d1;

}

// Allocate and initialize a new bucket data structure
bucket newBucket (int N)
{
	bucket b;
	int i;

	b = (bucket)malloc (sizeof(struct bucketStructure));
	b->N = N; b->n = 0;
	b->value = (float *)malloc(sizeof(float)*N);
	b->names = (char **)malloc (sizeof(char *) * N);
	for (i=0; i<N; i++)
	{
		b->value[i] = -1.0e4F;
		b->names[i] = (char *)malloc (256);
	}
	return b;
}

void clearBucket (bucket b)
{
	int i;

	b->N = 10; b->n = 0;
	for (i=0; i<10; i++)
	{
		b->value[i] = -1.0e4F;
		b->names[i][0] = (char)0;
	}
}

// Save the best (smallest) N values into this bucket
void keepBest (float value, bucket b, char *name)
{
	int i, j;
	float t;
	char  s[256];

	if (b->n < b->N)  // In this case, the bucket is not full. Keep the new item.
	{
		b->value[b->n] = value; 
		strcpy (b->names[b->n], name);
		b->n++;
		if (b->n == b->N)		// This filled the bucket. Sort it by 'value'.
		{
			for (i=0; i<b->N; i++)
			{
				for (j=i+1; j<b->N; j++)
				{
					if (b->value[j] < b->value[i]) // Swap
					{
						t = b->value[i]; b->value[i] = b->value[j]; b->value[j] = t;
						strcpy(s, b->names[i]); strcpy(b->names[i],b->names[j]); strcpy(b->names[j],s);
					}
				}
			}
		}
		return;
	}

	for (i=0; i<b->N; i++)		// Look at all items in this bucket
	{
		if (value < b->value[i])	// Insert new item here
		{
			for (j=b->N-1; j>i; j--)
			{
				b->value[j] = b->value[j-1]; 
				strcpy(b->names[j], b->names[j-1]);
			}
			b->value[i] = value; 
			strcpy (b->names[i], name);

			return;
		}
	}
}

int whichBin (char *s)
{
	int i, j=0, k=0, n=0;
	char sn[24];

	for (i=strlen(s)-1; i>0; i--)
		if (s[i] == '\\') { j = i; break; }

	for (i=j-1; i>0; i--)
		if (s[i] == '\\') { k = i; break; }
	k = k+1;
	for (i=k; i<j; i++) sn[n++] = s[i];
	sn[n] = (char)0;
	sscanf (sn, "%d", &i);
	return i;
}

void search (fptr a)
{
	struct features *b;
	FILE *f;
	static bucket meanBucket=0;
	int i, k, vote=0, means=0, more=1;
	double d=0.0;

	f = fopen ("C:\\AIPCV\\mastergrey.x", "rb");
	if (f == 0)
	{
		printf ("Master file cannot be found.   C:\\AIPCV\\master3.txt\n\n");
		exit (0);
	}

	if (meanBucket == 0)
		meanBucket = newBucket (10);		// Buckets contain the top 10 matches.

	b = readRecord (f);
	while (b)
	{
		d = compareFV (a, b);
		keepBest ((float)d,  meanBucket,  b->name);
		b = readRecord (f);
	}
	fclose (f);

// Look at MEAN bucket results
		vote = 0;
		for (i=0; i<10; i++)			// For each item in the bucket...
		{
			k = whichBin (meanBucket->names[i]);
			if (i==0 && meanBucket->value[i] < 0.0001)  // First entry is the original?
				fprintf (out, "Y ");
			else fprintf (out, "n ");
			if (k == currentBin) vote += 1;		// Count successes
			else fprintf (out, "(%d/%d) ", i, k);	// Print errors
		}
		fprintf (out, "Votes (of 10)=%d  ", vote);
		if (vote >= 5) 
		{ 
			means = 1; 
			meanSuccess += 1; 
			printf (" Success.\n");
		}
		else means = 0;
		N++;
		fprintf (out, "\n");

	clearBucket(meanBucket);
}

int main(int argc, char *argv[])
{
	IplImage *img=0, *hsv=0, *img2=0; 
	int count=0, more=1, nsamples=0, ii=0;
	char filename[256];
	struct features b;
	FILE *f;

	printf ("----------- Image Based Query (AIPCV 2010) ------------------\n");
	printf ("------ Searching for each image in the data collection ------\n");

	// All file names in the data set lie on master.txt
	f = fopen ("c:\\aipcv\\mastergrey.x", "rb");
	if (f == NULL)
	{
		printf ("-- ERROR: can't open the master name file.\n");
		return 1;
	}
	out = fopen ("c:\\aipcv\\alloutGrey.txt", "w");

	do
	{
		for (ii=0; ii<110; ii++)		// Skip some.
		  more = fread (&b, sizeof(struct features), 1, f);
		if (more != 1) break;

//		sprintf (filename, "%s\\%s", path, file);	// Build the complete name.
//		fprintf (out, "%s ", file);
		currentBin = whichBin (b.name);
		printf ("File %s bin %d\n", b.name, currentBin);

		// Calculate target's color features
			search (&b);						// Match against the master file
	} while (more == 1);

	fprintf (out, "Successes %d of %d\n", meanSuccess, N);
  return 0;
}
