// Create a master list of image files.
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <dirent.h>

CvScalar rainbow[10];
int THRESHOLD = 15;
count = 0;
FILE *master;
char name[256];

struct quadtree
{
	struct quadtree *next[4];
	int uli, ulj, lri, lrj;
	float r, g, b;
};
typedef struct quadtree * qtptr;

// Maintain the master image collection
void initializeMaster ()
{
	master = fopen ("C:\\AIPCV\\master.txt", "r");
}

IplImage *nextImage ()
{
	char filename[128];
	IplImage *img = 0;
	int k=0;

	while (img == 0)
	{
		k = fscanf (master, "%s", name);
		k = fscanf (master, "%s", filename);
		if (k <= 0) return 0;

		strcat (name, "\\");
		strcat (name, filename);
		printf ("Image is %s%s\n", name, filename);
		img = cvLoadImage(name, 1);
		if (img) return img;
		printf ("----------------------Failed! ------------------\n");
//	img = cvLoadImage("C:\\AIPCV\\png4\\35\\35_r45.png", 1);
	}
	return 0;
}

void freeImage (IplImage **p)
{
	cvReleaseImage(p);
}

isGrey (CvScalar s)
{
	int d1, d2, d3;

	d1 = (int)(s.val[0] - s.val[1]); if (d1<0) d1 = -d1;
	d2 = (int)(s.val[1] - s.val[2]); if (d2<0) d2 = -d2;
	d3 = (int)(s.val[0] - s.val[2]); if (d3<0) d3 = -d3;
	if (d1 < d2) d1 = d2;
	if (d1 < d3) d1 = d3;
	if (d3 < THRESHOLD) return 1;
	else return 0;
}

void lowerCase (char *s)
{
	while (*s)
	{
		if ( (*s > 'A') && (*s <= 'Z') ) *s = 'a' + (*s-'A');
		s++;
	}
}

// Scan a directory within the main path, looking for image file. Insert them into 
// the master list when one is seen.
void doSubdir (char *name, DIR *dir)
{
	  struct dirent *ent;
	  char str[12], *strx;

  while ((ent = readdir (dir)) != NULL) 
  {

	    if (ent->d_name[strlen(ent->d_name)-4] == '.')
	    {
	      strx = (char *)(&ent->d_name[strlen(ent->d_name)-3]);
		  strcpy (str, strx);
	      lowerCase (str);

		  if ( strcmp(str, "jpg")==0 ) printf ("File %s '%s' is JPEG.\n", name, ent->d_name);
		  else if ( strcmp(str, "gif")==0 ) printf ("File %s '%s' is GIF.\n", name, ent->d_name);
		  else if ( strcmp(str, "png")==0 ) printf ("File %s '%s' is PNG.\n", name, ent->d_name);
		  else if ( strcmp(str, "bmp")==0 ) printf ("File %s '%s' is BMP.\n", name, ent->d_name);
		  else if ( strcmp(str, "ppm")==0 ) printf ("File %s '%s' is PPM.\n", name, ent->d_name);
		  else if ( strcmp(str, "pgm")==0 ) printf ("File %s '%s' is PGM.\n", name, ent->d_name);
		  else if ( strcmp(str, "pbm")==0 ) printf ("File %s '%s' is PBM.\n", name, ent->d_name);
		  else if ( strcmp(str, "tga")==0 ) printf ("File %s '%s' is Targa.\n", name, ent->d_name);
		  else if ( strcmp(str, "tif")==0 ) printf ("File %s '%s' is TIFF.\n", name, ent->d_name);
		  else if ( strcmp(str, "pcx")==0 ) printf ("File %s '%s' is PCX.\n", name, ent->d_name);
		  else continue;

//		  insert (name, ent->d_name);
		  count++;
	    }
  }
}

// Compute the mean color over all pixels. Avoid black.
void meanColor (IplImage *x, CvScalar *t)
{
	double r=0.0, g=0.0, b=0.0;
	int i,j,k=0;
	CvScalar s;

	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			if ( isGrey(s) && s.val[1] < 20 ) continue;
			r += s.val[2];
			g += s.val[1];
			b += s.val[0];
			k++;
		}
		t->val[0] = b/k;
		t->val[1] = g/k;
		t->val[2] = r/k;
}

qtptr newQuad (int uli, int ulj, int lri, int lrj)
{
	qtptr p;

	p = (qtptr)malloc (sizeof (struct quadtree));
	p->lri = lri; p->lrj = lrj;
	p->uli = uli; p->ulj = ulj;
	p->r = p->b = p->g = 0.0;
	p->next[0] = p->next[1] = p->next[2] = p->next[3] = (qtptr)0;
	return p;
}

void freeQuadTree (qtptr p)
{
	if (p->next[0]) freeQuadTree (p->next[0]);
	if (p->next[1]) freeQuadTree (p->next[1]);
	if (p->next[2]) freeQuadTree (p->next[2]);
	if (p->next[3]) freeQuadTree (p->next[3]);
	free (p);
}

void displayQuadTree (IplImage *im, int level, qtptr p)
{
	int i,j;
	CvScalar s;

	if (p == 0) return;

	if (level == 0)		// Display this level.
	{
		s.val[0] = p->b; s.val[1] = p->g; s.val[2] = p->r;
		for (i=p->uli; i<=p->lri; i++)
			for (j=p->ulj; j<=p->lrj; j++)
				cvSet2D (im, i, j, s);
		return;
	}
	displayQuadTree (im, level-1, p->next[0]);
	displayQuadTree (im, level-1, p->next[1]);
	displayQuadTree (im, level-1, p->next[2]);
	displayQuadTree (im, level-1, p->next[3]);
}

qtptr makeQuadTree (IplImage *im, int uli, int ulj, int lri, int lrj)
{
	qtptr a, b, c, d;		// Quads: a b
							//        c d
	CvScalar s;
	int di, dj;
	qtptr p;

	// A single pixel - bottom of the tree.
//	printf ("Making quad tree (%d,%d) to (%d,%d)\n", uli, ulj, lri, lrj);
	di = lri - uli; dj = lrj - ulj;
	if ( (uli==lri) && (ulj==lrj) )
	{
		a = newQuad (uli, ulj, lri, lrj);
		s = cvGet2D (im, uli, ulj);
		a->r = (float)s.val[2]; a->g = (float)s.val[1]; a->b = (float)s.val[0];
		a->uli = a->lri = uli;	a->lrj = a->ulj = ulj;
//		printf ("Single pixel: (%lf,%lf, %lf)\n", s.val[2], s.val[1], s.val[0]);
		return a;
	}

// 2x1 pixel groups
	if ((uli==lri) && (dj==1))	// Horizontal
	{
		a = newQuad (uli, ulj, uli, ulj);
		s = cvGet2D (im, uli, ulj);
		a->r = (float)s.val[2]; a->g = (float)s.val[1]; a->b = (float)s.val[0];
		a->uli = a->lri = uli;	a->lrj = a->ulj = ulj;

		b = newQuad (uli, lrj, uli, lrj);
		s = cvGet2D (im, uli, lrj);
		b->r = (float)s.val[2]; b->g = (float)s.val[1]; b->b = (float)s.val[0];
		b->uli = b->lri = uli;	b->lrj = b->ulj = lrj;

		p = newQuad (uli, ulj, lri, lrj);
		p->next[0] = a; p->next[1] = b;
		p->r = (a->r+b->r)/2.0f;  p->g = (a->g+b->g)/2.0f; p->b = (a->b+b->b)/2.0f;
		return p;
	}

	if ((di==1) && (ulj==lrj))  // Vertical
	{
		a = newQuad (uli, ulj, uli, ulj);
		s = cvGet2D (im, uli, ulj);
		a->r = (float)s.val[2]; a->g = (float)s.val[1]; a->b = (float)s.val[0];
		a->uli = a->lri = uli;	a->lrj = a->ulj = ulj;

		c = newQuad (lri, lrj, lri, lrj);
		s = cvGet2D (im, lri, lrj);
		c->r = (float)s.val[2]; c->g = (float)s.val[1]; c->b = (float)s.val[0];
		c->uli = a->lri = uli;	c->lrj = c->ulj = ulj;

		p = newQuad (uli, ulj, lri, lrj);
		p->next[0] = a; p->next[2] = c;
		p->r = (a->r+c->r)/2.0f;  p->g = (a->g+c->g)/2.0f; p->b = (a->b+c->b)/2.0f;
		return p;
	}

	di /= 2; dj /= 2;
	a = makeQuadTree (im, uli, ulj, uli+di, ulj+dj);
	b = makeQuadTree (im, uli, ulj+dj+1, uli+di, lrj);
	c = makeQuadTree (im, uli+di+1, ulj, lri, ulj+dj);
	d = makeQuadTree (im, uli+di+1, ulj+dj+1, lri, lrj);
	p = newQuad (uli, ulj, lri, lrj);
	p->next[0] = a; p->next[1] = b; p->next[2] = c; p->next[3] = d;
	p->r = (float)(a->r+b->r+c->r+d->r)/4.0f;
	p->g = (float)(a->g+b->g+c->g+d->g)/4.0f;
	p->b = (float)(a->b+b->b+c->b+d->b)/4.0f;
	return p;
}

qtptr colorQuadTree (IplImage *img)
{
	qtptr p;

	p = makeQuadTree (img, 0, 0, img->height-1, img->width-1);
	return p;
}

void quadColors (qtptr qt, float *qvec)
{
	int i = 0;

	qvec[i++] = qt->next[0]->r;
	qvec[i++] = qt->next[0]->g;
	qvec[i++] = qt->next[0]->b;

	qvec[i++] = qt->next[1]->r;
	qvec[i++] = qt->next[1]->g;
	qvec[i++] = qt->next[1]->b;

	qvec[i++] = qt->next[2]->r;
	qvec[i++] = qt->next[2]->g;
	qvec[i++] = qt->next[2]->b;

	qvec[i++] = qt->next[3]->r;
	qvec[i++] = qt->next[3]->g;
	qvec[i++] = qt->next[3]->b;

}

void hexColors (qtptr qt, float *hvec)
{
	quadColors (qt->next[0], &(hvec[0])  );
	quadColors (qt->next[1], &(hvec[12]) );
	quadColors (qt->next[2], &(hvec[24]) );
	quadColors (qt->next[3], &(hvec[36]) );
}

int match (CvScalar s)
{
	int i, k=-1;
	double v1, v2, v3;
	double d = 1.0e32, dmin = 1.0e32;

		for (i=0; i<10; i++)
		{
			v1 = s.val[0] - rainbow[i].val[0];
			v2 = s.val[1] - rainbow[i].val[1];
			v3 = s.val[2] - rainbow[i].val[2];
			d = v1*v1 + v2*v2 + v3*v3;
			if (d < dmin)
			{
				dmin = d;
				k = i;
			}
		}
		return k;
}

void protoColor (IplImage *img, float *pvec)
{
	CvScalar  s;
	int i, j, k, n=0;

	for (i=0; i<48; i++) pvec[i] = 0;

	for (i=0; i<img->height; i++)
		for (j=0; j<img->width; j++)
		{
			s = cvGet2D (img, i, j);
			if (isGrey (s))
			{
				if (s.val[0] < 15) 
				{
					s.val[0] = s.val[1] = s.val[2] = 0;
					continue;  // Avoid black
				} else if (s.val[0] >= 240)
				{
					s.val[0] = s.val[1] = s.val[2] = 255;
				} else
				{
				   s.val[0] = s.val[1] = s.val[2] = 128;
				}
			} 

				k = match (s);
				pvec[k] += 1; n++;
				s.val[0] = rainbow[k].val[0];
				s.val[1] = rainbow[k].val[1];
				s.val[2] = rainbow[k].val[2];

			cvSet2D (img, i, j, s);
		}
		for (i=0; i<10; i++) pvec[i] /= (float)n;
}

void hueHistogram (IplImage *img, float *hhisto)
{
	CvSize size;
	int depth;
	IplImage *hsv;
	int i,j, k, n=0;
	CvScalar s;

	size = cvGetSize(img);
	depth = img->depth;
	hsv = cvCreateImage(size, depth, 3);
	cvZero(hsv);
 
    // Convert from Red-Green-Blue to Hue-Saturation-Value 
   	cvCvtColor( img, hsv, CV_BGR2HSV );

	for (i=0; i<180; i++) hhisto[i] = 0;
	for (i=0; i<hsv->height; i++)
		for (j=0; j<hsv->width; j++)
		{
			s = cvGet2D (hsv, i, j);
			if (isGrey(s)) continue;  // no hue
			k = (int)s.val[0];
			hhisto[k] += 1; n++;
		}
	for (i=0; i<180; i++) hhisto[i] /= n;
	freeImage (&hsv);
}

double chrominance (CvScalar s)
{
	double m=0.0f, a, b, c, sd=0.0f, res, g=0.0, gg=0.0;
	int i;

	// Find standard deviation of the rgb values.
	m = (s.val[0] +s.val[1] +s.val[2])/3.0;
	sd = sqrt( ( (s.val[0]-m)*(s.val[0]-m) + 
		        (s.val[1]-m)*(s.val[1]-m) + 
				(s.val[2]-m)*(s.val[2]-m) ) / 2.0 );
	sd = sd/147.2244;

// Piecewise linear function of sd, a, and b
	a = 0.05; b = 0.8;
	c = (a+b)/2.0;
	g = 2.0*(sd-a)/(b-a)*(sd-a)/(b-a);
	gg = 1.0-2.0*(sd-b)/(b-a)*(sd-b)/(b-a);

	if (sd < a) res = 0.0;
	else if (sd < (a+b)/2.0) res = g;
	else if (sd < b) res = gg;
	else res = 0.99;

	return res;
}

void chrominanceHisto (IplImage *img, float *h)
{
	int i,j,k, n=0;
	CvScalar s;
	float c, nn=0.0f;

	s.val[0] = 0.0; s.val[1] = 255.0; s.val[2] = 255.0;
	chrominance (s);

	for (i=0; i<16; i++) h[i] = 0;
	for (i=0; i<img->height; i++)
		for (j=0; j<img->width; j++)
		{
			s = cvGet2D (img, i, j);
			c = (float)chrominance (s);
			k = (int)(c*16);
			h[k] += 1; n++;
		}
		nn = (float)n;
		for (i=0; i<16; i++) h[i] /= n;
}

int main(int argc, char *argv[])
{
	IplImage *img=0, *hsv=0, *img2=0; 
	int count=0, i;
	CvScalar mean;
	float qvec[12], hvec[48], pvec[48], hhisto[256], chisto[16];
	qtptr qt;

	FILE *master2;
	
	initializeMaster ();
	master2 = fopen ("C:\\AIPCV\\master4.txt", "w");

	rainbow[0].val[0] = 0;   rainbow[0].val[1] = 0;   rainbow[0].val[2] = 170;  // Red
	rainbow[1].val[0] = 0;   rainbow[1].val[1] = 85;  rainbow[1].val[2] = 170;  // Orange
	rainbow[2].val[0] = 0;   rainbow[2].val[1] = 170; rainbow[2].val[2] = 170;  // Yellow
	rainbow[3].val[0] = 0;   rainbow[3].val[1] = 170; rainbow[3].val[2] = 0;    // Green
	rainbow[4].val[0] = 170; rainbow[4].val[1] = 0;   rainbow[4].val[2] = 0;    // Blue
	rainbow[5].val[0] = 170; rainbow[5].val[1] = 0;   rainbow[5].val[2] = 85;   // indigo
	rainbow[6].val[0] = 0;   rainbow[6].val[1] = 170; rainbow[6].val[2] = 85;  // Magenta
	rainbow[7].val[0] = 25;   rainbow[7].val[1] = 25;   rainbow[7].val[2] = 25;    // Black
	rainbow[8].val[0] = 240; rainbow[8].val[1] = 240; rainbow[8].val[2] = 240;  // White
	rainbow[9].val[0] = 128; rainbow[9].val[0] = 128; rainbow[9].val[0] = 128;  // Grey

   // create a window
	cvNamedWindow("win1", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("win1", 100, 100);

 	img2 = cvCreateImage( cvSize(256, 256), IPL_DEPTH_8U, 3);

  while (img  = nextImage())
  {
// Measure color features
	  meanColor (img, &mean);			// Overall average color

	  qt = colorQuadTree (img);			// Quad tree
	  quadColors (qt, qvec);			// four colors of level 1
	  hexColors (qt, hvec);				// 16 colors of level 2

	  freeQuadTree (qt);

	  protoColor (img, pvec);			// Histogram based on prototype colors.

	  hueHistogram (img, hhisto);		// Histogram of hue only

	  chrominanceHisto (img, chisto);

//	  cvShowImage("win1", img );
//	  cvWaitKey(0);

	  // Save results to a file: mean color
	  fprintf (master2, "%s %lf %lf %lf\n", name,mean.val[0], mean.val[1], mean.val[2]);
	  for (i=0; i<12; i++)                      // Quad colors
		  fprintf (master2, "%f ", qvec[i]);
	  fprintf (master2, "\n");
	  for (i=0; i<48; i++)						// Hex colors
		fprintf (master2, "%f ", hvec[i]);
	  fprintf (master2, "\n");
	  for (i=0; i<30; i++)						// Prototype colors
		fprintf (master2, "%f ", pvec[i]);
	  fprintf (master2, "\n");
		for (i=0; i<180; i++)
		{
			if ( ((i+1)%10) == 0) fprintf (master2, "\n");
			fprintf (master2, "%f ", hhisto[i]);
		}
		fprintf (master2, "\n");
	  for (i=0; i<16; i++)						// Chrominance histogram
		fprintf (master2, "%f ", chisto[i]);
	  fprintf (master2, "\n");

		cvReleaseImage (&img);
  }
	fclose (master);
  return 0;
}