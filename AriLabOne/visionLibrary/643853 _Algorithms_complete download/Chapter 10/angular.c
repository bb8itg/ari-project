// Demo of angular sampling
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <dirent.h>

CvScalar rainbow[10];
int THRESHOLD = 15;
count = 0;
FILE *master;

 
void lowerCase (char *s)
{
	while (*s)
	{
		if ( (*s > 'A') && (*s <= 'Z') ) *s = 'a' + (*s-'A');
		s++;
	}
}

// Scan a directory within the main path, looking for image file. Insert them into 
// the master list when one is seen.
void doSubdir (char *name, DIR *dir)
{
	  struct dirent *ent;
	  char str[12], *strx, s[256];
	  IplImage *img;

  while ((ent = readdir (dir)) != NULL) 
  {
		if (ent->d_name[0] == '.') continue;

	    if (ent->d_name[strlen(ent->d_name)-4] == '.')
	    {
	      strx = (char *)(&ent->d_name[strlen(ent->d_name)-3]);
		  strcpy (str, strx);
	      lowerCase (str);

		  if ( strcmp(str, "jpg")==0 ) printf ("File %s '%s' is JPEG.\n", name, ent->d_name);
		  else if ( strcmp(str, "gif")==0 ) printf ("File %s '%s' is GIF.\n", name, ent->d_name);
		  else if ( strcmp(str, "png")==0 ) printf ("File %s '%s' is PNG.\n", name, ent->d_name);
		  else if ( strcmp(str, "bmp")==0 ) printf ("File %s '%s' is BMP.\n", name, ent->d_name);
		  else if ( strcmp(str, "ppm")==0 ) printf ("File %s '%s' is PPM.\n", name, ent->d_name);
		  else if ( strcmp(str, "pgm")==0 ) printf ("File %s '%s' is PGM.\n", name, ent->d_name);
		  else if ( strcmp(str, "pbm")==0 ) printf ("File %s '%s' is PBM.\n", name, ent->d_name);
		  else if ( strcmp(str, "tga")==0 ) printf ("File %s '%s' is Targa.\n", name, ent->d_name);
		  else if ( strcmp(str, "tif")==0 ) printf ("File %s '%s' is TIFF.\n", name, ent->d_name);
		  else if ( strcmp(str, "pcx")==0 ) printf ("File %s '%s' is PCX.\n", name, ent->d_name);
		  else continue;

		  // Display the image
			sprintf (s, "%s\\%s", name, ent->d_name);
			img = cvLoadImage (s, 1);
			cvShowImage("Image Dataset", img );
			cvWaitKey(1);
			cvReleaseImage (&img);
		  count++;
	    }
  }
}

float angle_2pt (int r1, int c1, int r2, int c2)
{
	double atan(), fabs();
	double x, dr, dc, conv;

	conv = 180.0/3.1415926535;
	dr = (double)(r2-r1); dc = (double)(c2-c1);

/*	Compute the raw angle based of Drow, Dcolumn		*/
	if (dr==0 && dc == 0) x = 0.0;
	else if (dc == 0) x = 90.0;
	else {
		x = fabs(atan (dr/dc));
		x = x * conv;
	}

/*	Adjust the angle according to the quadrant		*/
	if (dr <= 0) {			/* upper 2 quadrants */
	  if (dc < 0) x = 180.0 - x;	/* Left quadrant */
	} else if (dr > 0) {		/* Lower 2 quadrants */
	  if (dc < 0) x = x + 180.0;	/* Left quadrant */
	  else x = 360.0-x;		/* Right quadrant */
	}

	return (float)x;
}

void angularRegion (IplImage *x, int q)
{
	float a1, a2, a;		// Angles: start, end
	int i,j;			// pixels
	CvScalar s;			// Colors.grey
	int centeri, centerj;

	centeri = (int)(x->height/2.0f); centerj = (int)(x->width/2.0f);
	a1 = q*45.0f;
	a2 = (q+1)*45.0f;	// Degrees

	s.val[0] = 255; s.val[1] = 128; s.val[2] = 89; s.val[3] = 255;
	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			a = angle_2pt (centeri, centerj, i, j);
			if ( (a>=a1) && (a<=a2) ) cvSet2D (x, i,j,s);
		}
}

int main(int argc, char *argv[])
{
  IplImage *img=0; 
  int i;
  char path[256];

	strcpy (path, "c:\\AIPCV\\pic1.jpg");
	cvNamedWindow("Sampling", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("Sampling", 100, 100);

// Display the image
	img = cvLoadImage (path, 1);
	cvShowImage("Sampling", img );
	cvWaitKey(0);
	
	for (i=0; i<9; i++)
	{
		angularRegion (img, i);
		cvShowImage("Sampling", img );
		cvWaitKey(0);
	}


	return 0;
}


