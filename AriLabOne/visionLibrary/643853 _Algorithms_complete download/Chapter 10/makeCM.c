// Create a master list of image data: color moments
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <dirent.h>

int THRESHOLD = 15;
count = 0;
FILE *master;
char name[256];

// Maintain the master image collection
void initializeMaster ()
{
	master = fopen ("C:\\AIPCV\\master.txt", "r");
}

IplImage *nextImage ()
{
	char filename[128];
	IplImage *img = 0;
	int k=0;

	while (img == 0)
	{
		k = fscanf (master, "%s", name);
		k = fscanf (master, "%s", filename);
		if (k <= 0) return 0;

		strcat (name, "\\");
		strcat (name, filename);
		printf ("Image is %s%s\n", name, filename);
		img = cvLoadImage(name, 1);
		if (img) return img;
		printf ("----------------------Failed! ------------------\n");
//	img = cvLoadImage("C:\\AIPCV\\png4\\35\\35_r45.png", 1);
	}
	return 0;
}

void freeImage (IplImage **p)
{
	cvReleaseImage(p);
}

isGrey (CvScalar s)
{
	int d1, d2, d3;

	d1 = (int)(s.val[0] - s.val[1]); if (d1<0) d1 = -d1;
	d2 = (int)(s.val[1] - s.val[2]); if (d2<0) d2 = -d2;
	d3 = (int)(s.val[0] - s.val[2]); if (d3<0) d3 = -d3;
	if (d1 < d2) d1 = d2;
	if (d1 < d3) d1 = d3;
	if (d3 < THRESHOLD) return 1;
	else return 0;
}

void lowerCase (char *s)
{
	while (*s)
	{
		if ( (*s > 'A') && (*s <= 'Z') ) *s = 'a' + (*s-'A');
		s++;
	}
}

// Scan a directory within the main path, looking for image file. Insert them into 
// the master list when one is seen.
void doSubdir (char *name, DIR *dir)
{
	  struct dirent *ent;
	  char str[12], *strx;

  while ((ent = readdir (dir)) != NULL) 
  {

	    if (ent->d_name[strlen(ent->d_name)-4] == '.')
	    {
	      strx = (char *)(&ent->d_name[strlen(ent->d_name)-3]);
		  strcpy (str, strx);
	      lowerCase (str);

		  if ( strcmp(str, "jpg")==0 ) printf ("File %s '%s' is JPEG.\n", name, ent->d_name);
		  else if ( strcmp(str, "gif")==0 ) printf ("File %s '%s' is GIF.\n", name, ent->d_name);
		  else if ( strcmp(str, "png")==0 ) printf ("File %s '%s' is PNG.\n", name, ent->d_name);
		  else if ( strcmp(str, "bmp")==0 ) printf ("File %s '%s' is BMP.\n", name, ent->d_name);
		  else if ( strcmp(str, "ppm")==0 ) printf ("File %s '%s' is PPM.\n", name, ent->d_name);
		  else if ( strcmp(str, "pgm")==0 ) printf ("File %s '%s' is PGM.\n", name, ent->d_name);
		  else if ( strcmp(str, "pbm")==0 ) printf ("File %s '%s' is PBM.\n", name, ent->d_name);
		  else if ( strcmp(str, "tga")==0 ) printf ("File %s '%s' is Targa.\n", name, ent->d_name);
		  else if ( strcmp(str, "tif")==0 ) printf ("File %s '%s' is TIFF.\n", name, ent->d_name);
		  else if ( strcmp(str, "pcx")==0 ) printf ("File %s '%s' is PCX.\n", name, ent->d_name);
		  else continue;

//		  insert (name, ent->d_name);
		  count++;
	    }
  }
}

// Compute the mean color over all pixels. Avoid black.
void meanColor (IplImage *x, CvScalar *t)
{
	double r=0.0, g=0.0, b=0.0;
	int i,j,k=0;
	CvScalar s;

	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			if ( isGrey(s) && s.val[1] < 20 ) continue;
			r += s.val[2];
			g += s.val[1];
			b += s.val[0];
			k++;
		}
		t->val[0] = b/k;
		t->val[1] = g/k;
		t->val[2] = r/k;
}

// Compute the Standard deviation of color over all pixels. Avoid black.
void sdColor (IplImage *x, CvScalar mean, CvScalar *t)
{
	double r=0.0, g=0.0, b=0.0;
	int i,j,k=0;
	CvScalar s;

	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			if ( isGrey(s) && s.val[1] < 20 ) continue;
			r += (s.val[2]- mean.val[2])*(s.val[2]- mean.val[2]);
			g += (s.val[1]- mean.val[1])*(s.val[1]- mean.val[1]);
			b += (s.val[0]- mean.val[0])*(s.val[0]- mean.val[0]);
			k++;
		}
		k--;
		t->val[0] = sqrt(b/k);
		t->val[1] = sqrt(g/k);
		t->val[2] = sqrt(r/k);
}

// Compute skewness of color components over the image. Avoid black.
// Compute the mean color over all pixels. Avoid black.
void skewColor (IplImage *x, CvScalar mean, CvScalar sd, CvScalar *t)
{
	double r=0.0, g=0.0, b=0.0;
	int i,j,k=0;
	CvScalar s;

	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			if ( isGrey(s) && s.val[1] < 20 ) continue;
			r += (s.val[2]- mean.val[2])*(s.val[2]- mean.val[2])*(s.val[2]- mean.val[2]);
			g += (s.val[1]- mean.val[1])*(s.val[1]- mean.val[1])*(s.val[1]- mean.val[1]);
			b += (s.val[0]- mean.val[0])*(s.val[0]- mean.val[0])*(s.val[0]- mean.val[0]);
			k++;
		}
		k--;

		t->val[0] = b/(k*sd.val[0]*sd.val[0]*sd.val[0]);
		t->val[1] = g/(k*sd.val[1]*sd.val[1]*sd.val[1]);
		t->val[2] = r/(k*sd.val[2]*sd.val[2]*sd.val[2]);
}

int main(int argc, char *argv[])
{
	IplImage *img=0, *hsv=0, *img2=0; 
	int count=0;
	CvScalar mean, sd, skew;

	FILE *master2;
	
	initializeMaster ();
	master2 = fopen ("C:\\AIPCV\\master_cm.txt", "w");

   // create a window
//	cvNamedWindow("win1", CV_WINDOW_AUTOSIZE); 
//	cvMoveWindow("win1", 100, 100);

 	img2 = cvCreateImage( cvSize(256, 256), IPL_DEPTH_8U, 3);

  while (img  = nextImage())
  {
// Measure color features
	  meanColor (img, &mean);			// Overall average color
	  sdColor   (img, mean, &sd);
	  skewColor (img, mean, sd, &skew);

//	  cvShowImage("win1", img );
//	  cvWaitKey(0);

	  // Save results to a file: mean color
	  fprintf (master2, "%s %lf %lf %lf ", name,mean.val[0], mean.val[1], mean.val[2]);
	  fprintf (master2, "%lf %lf %lf ",  sd.val[0], sd.val[1], sd.val[2]);
	  fprintf (master2, "%lf %lf %lf\n", skew.val[0], skew.val[1], skew.val[2]);

	  cvReleaseImage (&img);
  }
	fclose (master);
  return 0;
}