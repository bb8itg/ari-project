// Auto-search using the master file - test of methods.
// Selects about 2000 images from the ALOI set as test images against
// the entire data set. Computes success rates - allows us to 
// experiement with variations on the methods relatively quickly.

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <dirent.h>

// Types of color feature. Index into feature arraty.
#define MEAN 0
#define QUAD 1
#define HEX  2
#define PROTO 3
#define HUE  4

CvScalar rainbow[10];		//Prototype colors
int THRESHOLD = 15;			// RGB within this value of each other are grey
count = 0;
FILE *out, *master;
char name[256];
int height=0, width=0;
int currentBin = -1;		// Which image directory (object) are we searching?
int meanSuccess=0, quadSuccess=0, hexSuccess=0, protoSuccess=0, hueSuccess=0;
int overallSuccess = 0;

struct quadtree
{
	struct quadtree *next[4];
	int uli, ulj, lri, lrj;
	float r, g, b;
};
typedef struct quadtree * qtptr;

struct featureVector 
{
	char name[256];		// image file name;
	double mean[3];		// Mean colors
	float qvec[12];		// First level quad tree
	float hvec[48];		// Second level quad tree
	float pvec[30];		// Prototype color histogram
	float hhisto[256];	// Hue histogram
};
typedef struct featureVector * fptr;


// A bucket data structure. Names and floats are added to the bucket, which
// keeps the best N values (best being smallest in this case)
struct bucketStructure 
{
	int N, n;			// Maximum Size, current size;
	float *value;		// Contents: in this case, a set of distances from a target.
	char **names;		// Names of files that correspond to values. Images.
};
typedef struct bucketStructure *bucket;   // pointer to a bucket

isGrey (CvScalar s)
{
	int d1, d2, d3;

	d1 = (int)(s.val[0] - s.val[1]); if (d1<0) d1 = -d1;
	d2 = (int)(s.val[1] - s.val[2]); if (d2<0) d2 = -d2;
	d3 = (int)(s.val[0] - s.val[2]); if (d3<0) d3 = -d3;
	if (d1 < d2) d1 = d2;
	if (d1 < d3) d1 = d3;
	if (d3 < THRESHOLD) return 1;
	else return 0;
}

// Maintain the master image collection, set up constants
void initialize ()
{
	rainbow[0].val[0] = 0;   rainbow[0].val[1] = 0;   rainbow[0].val[2] = 170;  // Red
	rainbow[1].val[0] = 0;   rainbow[1].val[1] = 85;  rainbow[1].val[2] = 170;  // Orange
	rainbow[2].val[0] = 0;   rainbow[2].val[1] = 170; rainbow[2].val[2] = 170;  // Yellow
	rainbow[3].val[0] = 0;   rainbow[3].val[1] = 170; rainbow[3].val[2] = 0;    // Green
	rainbow[4].val[0] = 170; rainbow[4].val[1] = 0;   rainbow[4].val[2] = 0;    // Blue
	rainbow[5].val[0] = 170; rainbow[5].val[1] = 0;   rainbow[5].val[2] = 85;   // indigo
	rainbow[6].val[0] = 0;   rainbow[6].val[1] = 170; rainbow[6].val[2] = 85;   // Magenta
	rainbow[7].val[0] = 25;  rainbow[7].val[1] = 25;  rainbow[7].val[2] = 25;   // Black
	rainbow[8].val[0] = 240; rainbow[8].val[1] = 240; rainbow[8].val[2] = 240;  // White
	rainbow[9].val[0] = 128; rainbow[9].val[0] = 128; rainbow[9].val[0] = 128;  // Grey
}

void freeImage (IplImage **p)
{
	cvReleaseImage(p);
}


int match (CvScalar s)
{
	int i, k=-1;
	double v1, v2, v3;
	double d = 1.0e32, dmin = 1.0e32;

		for (i=0; i<10; i++)
		{
			v1 = s.val[0] - rainbow[i].val[0];
			v2 = s.val[1] - rainbow[i].val[1];
			v3 = s.val[2] - rainbow[i].val[2];
			d = v1*v1 + v2*v2 + v3*v3;
			if (d < dmin)
			{
				dmin = d;
				k = i;
			}
		}
		return k;
}

void lowerCase (char *s)
{
	while (*s)
	{
		if ( (*s > 'A') && (*s <= 'Z') ) *s = 'a' + (*s-'A');
		s++;
	}
}

void meanColor (IplImage *x, CvScalar *t)
{
	double r=0.0, g=0.0, b=0.0;
	int i,j,k=0;
	CvScalar s;

	for (i=0; i<x->height; i++)
		for (j=0; j<x->width; j++)
		{
			s = cvGet2D (x, i, j);
			if ( isGrey(s) && s.val[1] < 20 ) continue;
			r += s.val[2];
			g += s.val[1];
			b += s.val[0];
			k++;
		}
		t->val[0] = b/k;
		t->val[1] = g/k;
		t->val[2] = r/k;
}

qtptr newQuad (int uli, int ulj, int lri, int lrj)
{
	qtptr p;

	p = (qtptr)malloc (sizeof (struct quadtree));
	p->lri = lri; p->lrj = lrj;
	p->uli = uli; p->ulj = ulj;
	p->r = p->b = p->g = 0.0;
	p->next[0] = p->next[1] = p->next[2] = p->next[3] = (qtptr)0;
	return p;
}

void freeQuadTree (qtptr p)
{
	if (p->next[0]) freeQuadTree (p->next[0]);
	if (p->next[1]) freeQuadTree (p->next[1]);
	if (p->next[2]) freeQuadTree (p->next[2]);
	if (p->next[3]) freeQuadTree (p->next[3]);
	free (p);
}

qtptr makeQuadTree (IplImage *im, int uli, int ulj, int lri, int lrj)
{
	qtptr a, b, c, d;		// Quads: a b
							//        c d
	CvScalar s;
	int di, dj;
	qtptr p;

	// A single pixel - bottom of the tree.
//	printf ("Making quad tree (%d,%d) to (%d,%d)\n", uli, ulj, lri, lrj);
	di = lri - uli; dj = lrj - ulj;
	if ( (uli==lri) && (ulj==lrj) )
	{
		a = newQuad (uli, ulj, lri, lrj);
		s = cvGet2D (im, uli, ulj);
		a->r = (float)s.val[2]; a->g = (float)s.val[1]; a->b = (float)s.val[0];
		a->uli = a->lri = uli;	a->lrj = a->ulj = ulj;
//		printf ("Single pixel: (%lf,%lf, %lf)\n", s.val[2], s.val[1], s.val[0]);
		return a;
	}

// 2x1 pixel groups
	if ((uli==lri) && (dj==1))	// Horizontal
	{
		a = newQuad (uli, ulj, uli, ulj);
		s = cvGet2D (im, uli, ulj);
		a->r = (float)s.val[2]; a->g = (float)s.val[1]; a->b = (float)s.val[0];
		a->uli = a->lri = uli;	a->lrj = a->ulj = ulj;

		b = newQuad (uli, lrj, uli, lrj);
		s = cvGet2D (im, uli, lrj);
		b->r = (float)s.val[2]; b->g = (float)s.val[1]; b->b = (float)s.val[0];
		b->uli = b->lri = uli;	b->lrj = b->ulj = lrj;

		p = newQuad (uli, ulj, lri, lrj);
		p->next[0] = a; p->next[1] = b;
		p->r = (a->r+b->r)/2.0f;  p->g = (a->g+b->g)/2.0f; p->b = (a->b+b->b)/2.0f;
		return p;
	}

	if ((di==1) && (ulj==lrj))  // Vertical
	{
		a = newQuad (uli, ulj, uli, ulj);
		s = cvGet2D (im, uli, ulj);
		a->r = (float)s.val[2]; a->g = (float)s.val[1]; a->b = (float)s.val[0];
		a->uli = a->lri = uli;	a->lrj = a->ulj = ulj;

		c = newQuad (lri, lrj, lri, lrj);
		s = cvGet2D (im, lri, lrj);
		c->r = (float)s.val[2]; c->g = (float)s.val[1]; c->b = (float)s.val[0];
		c->uli = a->lri = uli;	c->lrj = c->ulj = ulj;

		p = newQuad (uli, ulj, lri, lrj);
		p->next[0] = a; p->next[2] = c;
		p->r = (a->r+c->r)/2.0f;  p->g = (a->g+c->g)/2.0f; p->b = (a->b+c->b)/2.0f;
		return p;
	}

	di /= 2; dj /= 2;
	a = makeQuadTree (im, uli, ulj, uli+di, ulj+dj);
	b = makeQuadTree (im, uli, ulj+dj+1, uli+di, lrj);
	c = makeQuadTree (im, uli+di+1, ulj, lri, ulj+dj);
	d = makeQuadTree (im, uli+di+1, ulj+dj+1, lri, lrj);
	p = newQuad (uli, ulj, lri, lrj);
	p->next[0] = a; p->next[1] = b; p->next[2] = c; p->next[3] = d;
	p->r = (float)(a->r+b->r+c->r+d->r)/4.0f;
	p->g = (float)(a->g+b->g+c->g+d->g)/4.0f;
	p->b = (float)(a->b+b->b+c->b+d->b)/4.0f;
	return p;
}

qtptr colorQuadTree (IplImage *img)
{
	qtptr p;

	p = makeQuadTree (img, 0, 0, img->height-1, img->width-1);
	return p;
}

void quadColors (qtptr qt, float *qvec)
{
	int i = 0;

	qvec[i++] = qt->next[0]->r;
	qvec[i++] = qt->next[0]->g;
	qvec[i++] = qt->next[0]->b;

	qvec[i++] = qt->next[1]->r;
	qvec[i++] = qt->next[1]->g;
	qvec[i++] = qt->next[1]->b;

	qvec[i++] = qt->next[2]->r;
	qvec[i++] = qt->next[2]->g;
	qvec[i++] = qt->next[2]->b;

	qvec[i++] = qt->next[3]->r;
	qvec[i++] = qt->next[3]->g;
	qvec[i++] = qt->next[3]->b;

}

void hexColors (qtptr qt, float *hvec)
{
	quadColors (qt->next[0], &(hvec[0])  );
	quadColors (qt->next[1], &(hvec[12]) );
	quadColors (qt->next[2], &(hvec[24]) );
	quadColors (qt->next[3], &(hvec[36]) );
}

void protoColor (IplImage *img, float *pvec)
{
	CvScalar  s;
	int i, j, k, n=0;

	for (i=0; i<48; i++) pvec[i] = 0;

	for (i=0; i<img->height; i++)
		for (j=0; j<img->width; j++)
		{
			s = cvGet2D (img, i, j);
			if (isGrey (s))
			{
				if (s.val[0] < 15) 
				{
					s.val[0] = s.val[1] = s.val[2] = 0;
					continue;
				} else if (s.val[0] >= 240)
				{
					s.val[0] = s.val[1] = s.val[2] = 255;
				} else
				{
				   s.val[0] = s.val[1] = s.val[2] = 128;
				}
			} 
			k = match (s);
			pvec[k] += 1; n++;
			s.val[0] = rainbow[k].val[0];
			s.val[1] = rainbow[k].val[1];
			s.val[2] = rainbow[k].val[2];
			cvSet2D (img, i, j, s);
		}
		for (i=0; i<10; i++) pvec[i] /= (float)n;
}

void hueHistogram (IplImage *img, float *hhisto)
{
	CvSize size;
	int depth;
	IplImage *hsv;
	int i,j, k, n=0;
	CvScalar s;

	size = cvGetSize(img);
	depth = img->depth;
	hsv = cvCreateImage(size, depth, 3);
	cvZero(hsv);
 
    // Convert from Red-Green-Blue to Hue-Saturation-Value 
   	cvCvtColor( img, hsv, CV_BGR2HSV );

	for (i=0; i<180; i++) hhisto[i] = 0;
	for (i=0; i<hsv->height; i++)
		for (j=0; j<hsv->width; j++)
		{
			s = cvGet2D (hsv, i, j);
			if (isGrey(s)) continue;  // no hue
			k = (int)s.val[0];
			hhisto[k] += 1; n++;
		}
	for (i=0; i<180; i++) hhisto[i] /= n;
	freeImage (&hsv);
}

struct featureVector g_fv;
fptr getMasterRecord (FILE *f)
{
	int i, k;

		k = fscanf (f, "%s", g_fv.name); if (k<=0) return 0;
		for (i=0; i<3; i++)
		  k = fscanf (f, "%lf", &(g_fv.mean[i]));			// Mean
		if (k<=0) return 0;

		for (i=0; i<12; i++)                      
		  k = fscanf (f, "%f", &(g_fv.qvec[i]));	// Quad colors
		if (k<=0) return 0;

		for (i=0; i<48; i++)
			k = fscanf (f, "%f", &(g_fv.hvec[i]));	// Hex colors
		if (k<=0) return 0;

		for (i=0; i<30; i++)
			k = fscanf (f, "%f", &(g_fv.pvec[i]));	// Prototype colors
		if (k<=0) return 0;

		for (i=0; i<256; i++)
			k = fscanf (f, "%f", &(g_fv.hhisto[i])); // Hue histogram
		if (k<=0) return 0;

		return &g_fv;
}

struct featureVector g_if;
fptr getImageFeatures (IplImage *img)
{
	CvScalar mean;
	float qvec[12], hvec[48], pvec[48], hhisto[256];
	qtptr qt;
	int i;

		meanColor (img, &mean);			// Overall average color
		for (i=0; i<3; i++)
		 g_if.mean[i] = mean.val[i];
		
		qt = colorQuadTree (img);			// Quad tree                     
		quadColors (qt, qvec);			// four colors of level 1
		for (i=0; i<12; i++) g_if.qvec[i] = qvec[i];
		hexColors (qt, hvec);				// 16 colors of level 2
		for (i=0; i<48; i++) g_if.hvec[i] = hvec[i];
		freeQuadTree (qt);
		protoColor (img, pvec);			// Histogram based on prototype colors.
		for (i=0; i<30; i++) g_if.pvec[i] = pvec[i];
		hueHistogram (img, hhisto);		// Histogram of hue only
		for (i=0; i<256; i++) g_if.hhisto[i] = hhisto[i];
		return &g_if;
}

// Euclidean distance between two 3 dimensional double precision vectors
float distance3d (double *a, double *b)
{
	int i;
	double d=0.0;

	for (i=0; i<3; i++)
		d += (a[i]-b[i])*(a[i]-b[i]);
	return (float)sqrt (d);
}

// Euclidean distance between two N dimenstional floating point vectors
float distanceNf (float *a, float *b, int N)
{
	int i;
	float d = 0.0f;

	for (i=0; i<N; i++)
		d += ((a[i]-b[i])*(a[i]-b[i]));
	return (float)sqrt((double)d);
}

// Compare two feature sets 'a' and 'b'; compute distances by feature type
float distances[12];
float *compareFV (fptr a, fptr b)
{
	//	Compare means
	distances[MEAN]  = distance3d (a->mean, b->mean);
	distances[QUAD]  = distanceNf (a->qvec, b->qvec, 12);
	distances[HEX]   = distanceNf (a->hvec, b->hvec, 48);
	distances[PROTO] = distanceNf (a->pvec, b->pvec, 30);
	a->hhisto[0] = 0; b->hhisto[0] = 0;
	distances[HUE]   = distanceNf (a->hhisto, b->hhisto, 179);
	return distances;
}

// Allocate and initialize a new bucket data structure
bucket newBucket (int N)
{
	bucket b;
	int i;

	b = (bucket)malloc (sizeof(struct bucketStructure));
	b->N = N; b->n = 0;
	b->value = (float *)malloc(sizeof(float)*N);
	b->names = (char **)malloc (sizeof(char *) * N);
	for (i=0; i<N; i++)
	{
		b->value[i] = -1.0e4F;
		b->names[i] = (char *)malloc (256);
	}
	return b;
}

void clearBucket (bucket b)
{
	int i;

	b->N = 10; b->n = 0;
	for (i=0; i<10; i++)
	{
		b->value[i] = -1.0e4F;
		b->names[i][0] = (char)0;
	}
}

// Save the best (smallest) N values into this bucket
void keepBest (float value, bucket b, char *name)
{
	int i, j;
	float t;
	char  s[256];

	if (b->n < b->N)  // In this case, the bucket is not full. Keep the new item.
	{
		b->value[b->n] = value; 
		strcpy (b->names[b->n], name);
		b->n++;
		if (b->n == b->N)		// This filled the bucket. Sort it by 'value'.
		{
			for (i=0; i<b->N; i++)
			{
				for (j=i+1; j<b->N; j++)
				{
					if (b->value[j] < b->value[i]) // Swap
					{
						t = b->value[i]; b->value[i] = b->value[j]; b->value[j] = t;
						strcpy(s, b->names[i]); strcpy(b->names[i],b->names[j]); strcpy(b->names[j],s);
					}
				}
			}
		}
		return;
	}

	for (i=0; i<b->N; i++)		// Look at all items in this bucket
	{
		if (value < b->value[i])	// Insert new item here
		{
			for (j=b->N-1; j>i; j--)
			{
				b->value[j] = b->value[j-1]; 
				strcpy(b->names[j], b->names[j-1]);
			}
			b->value[i] = value; 
			strcpy (b->names[i], name);

			return;
		}
	}
}

int whichBin (char *s)
{
	int i, j=0, k=0, n=0;
	char sn[24];

	for (i=strlen(s)-1; i>0; i--)
		if (s[i] == '\\') { j = i; break; }

	for (i=j-1; i>0; i--)
		if (s[i] == '\\') { k = i; break; }
	k = k+1;
	for (i=k; i<j; i++) sn[n++] = s[i];
	sn[n] = (char)0;
	sscanf (sn, "%d", &i);
	return i;
}

void search (fptr a)
{
	fptr b;
	FILE *f;
	float *fv;
	static bucket meanBucket=0, quadBucket=0, hexBucket=0, protoBucket=0, hueBucket=0;
	int i, k, vote=0, means=0, quads=0, hexs=0, hues=0, protos=0;

	f = fopen ("C:\\AIPCV\\master3.txt", "r");
	if (f == 0)
	{
		printf ("Master file cannot be found.   C:\\AIPCV\\master3.txt\n\n");
		exit (0);
	}

	if (meanBucket == 0)
	{
		meanBucket = newBucket (10);		// Buckets contain the top 10 matches.
		quadBucket = newBucket (10);
		hexBucket = newBucket (10);
		protoBucket = newBucket (10);
		hueBucket = newBucket (10);
	}

	b = getMasterRecord (f);
	while (b)
	{
		fv = compareFV (a, b);
		keepBest (fv[MEAN],  meanBucket,  b->name);
		keepBest (fv[QUAD],  quadBucket,  b->name);
		keepBest (fv[HEX],   hexBucket,   b->name);
		keepBest (fv[PROTO], protoBucket, b->name);
		keepBest (fv[HUE],   hueBucket,   b->name);

		b = getMasterRecord (f);
	}
	fclose (f);

// Look at MEAN bucket results
		vote = 0;
		for (i=0; i<10; i++)			// For each item in the bucket...
		{
			k = whichBin (meanBucket->names[i]);
			if (i==0 && meanBucket->value[i] < 0.0001)  // First entry is the original?
				fprintf (out, "Y ");
			else fprintf (out, "n ");
			if (k == currentBin) vote += 1;		// Count successes
			else fprintf (out, "(%d/%d) ", i, k);	// Print errors
		}
		fprintf (out, "MEAN=%d  ", vote);
		if (vote >= 5) { means = 1; meanSuccess += 1; }
		else means = 0;

// Look at QUAD bucket results
		vote = 0;
		for (i=0; i<10; i++)			// For each item in the bucket...
		{
			k = whichBin (quadBucket->names[i]);
			if (i==0 && quadBucket->value[i] < 0.0001)  // First entry is the original?
				fprintf (out, "Y ");
			else fprintf (out, "n ");
			if (k == currentBin) vote += 1;		// Count successes
			else fprintf (out, "(%d/%d) ", i, k);	// Print errors
		}
		fprintf (out, "QUAD=%d  ", vote);
		if (vote >= 5) { quads = 1; quadSuccess += 1; }
		else quads = 0;

// Look at HEX bucket
		vote = 0;
		for (i=0; i<10; i++)			// For each item in the bucket...
		{
			k = whichBin (hexBucket->names[i]);
			if (i==0 && hexBucket->value[i] < 0.0001)  // First entry is the original?
				fprintf (out, "Y ");
			else fprintf (out, "n ");
			if (k == currentBin) vote += 1;		// Count successes
			else fprintf (out, "(%d/%d) ", i, k);	// Print errors
		}
		fprintf (out, "HEX=%d  ", vote);
		if (vote >= 5) { hexs = 1; hexSuccess += 1; }
		else hexs = 0;

// Look at PROTO bucket
		vote = 0;
		for (i=0; i<10; i++)			// For each item in the bucket...
		{
			k = whichBin (protoBucket->names[i]);
			if (i==0 && protoBucket->value[i] < 0.0001)  // First entry is the original?
				fprintf (out, "Y ");
			else fprintf (out, "n ");
			if (k == currentBin) vote += 1;		// Count successes
			else fprintf (out, "(%d/%d) ", i, k);	// Print errors
		}
		fprintf (out, "proto=%d  ", vote);
		if (vote >= 5) { protos = 1; protoSuccess += 1; }
		else protos = 0;

// Look at HUE bucket
		vote = 0;
		for (i=0; i<10; i++)			// For each item in the bucket...
		{
			k = whichBin (hueBucket->names[i]);
			if (i==0 && hueBucket->value[i] < 0.0001)  // First entry is the original?
				fprintf (out, "Y ");
			else fprintf (out, "n ");
			if (k == currentBin) vote += 1;		// Count successes
			else fprintf (out, "(%d/%d) ", i, k);	// Print errors
		}
		fprintf (out, "proto=%d  ", vote);
		if (vote >= 5) { hues = 1; hueSuccess += 1; }
		else hues = 0;

		k = (hues + protos + hexs + quads + means);
		fprintf (out, "overall=%d\n", k);
		if (k >= 3) overallSuccess += 1;
		fflush (out);

	clearBucket(meanBucket);
	clearBucket(quadBucket);
	clearBucket(hexBucket);
	clearBucket(protoBucket);
	clearBucket(hueBucket);
}

int main(int argc, char *argv[])
{
	IplImage *img=0, *hsv=0, *img2=0; 
	int count=0, more=1, nsamples=0, ii=0;
	char filename[256], path[256], file[256];
	fptr b;
	FILE *f;

	printf ("----------- Image Based Query (AIPCV 2010) ------------------\n");
	printf ("------ Searching for each image in the data collection ------\n");

	// All file names in the data set lie on mater.txt
	f = fopen ("c:\\aipcv\\master.txt", "r");
	if (f == NULL)
	{
		printf ("-- ERROR: can't open the master name file.\n");
		return 1;
	}
	out = fopen ("c:\\aipcv\\allout.txt", "w");

	initialize ();

	do
	{
		for (ii=0; ii<40; ii++)		// Skip some.
		{
			more = fscanf (f, "%s", path);		// Read directory path name
			more = fscanf (f, "%s", file);		// Read the file name
		}
		if (more <= 0) break;

		sprintf (filename, "%s\\%s", path, file);	// Build the complete name.
		fprintf (out, "%s ", file);
		currentBin = whichBin (filename);
		printf ("File %s bin %d\n", filename, currentBin);

		img = cvLoadImage (filename, 1);
		if (img)
		{
			height = img->height; width = img->width;
			b = getImageFeatures(img);		// Calculate target's color features
			search (b);						// Match against the master file
		}
		else printf ("Can't open the target image file '%s'.\n", filename);
	} while (more);
  return 0;
}