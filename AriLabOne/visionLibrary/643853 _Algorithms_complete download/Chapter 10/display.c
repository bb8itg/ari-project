// Display data collection image files
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <dirent.h>

CvScalar rainbow[10];
int THRESHOLD = 15;
count = 0;
FILE *master;

 
void lowerCase (char *s)
{
	while (*s)
	{
		if ( (*s > 'A') && (*s <= 'Z') ) *s = 'a' + (*s-'A');
		s++;
	}
}

// Scan a directory within the main path, looking for image file. Insert them into 
// the master list when one is seen.
void doSubdir (char *name, DIR *dir)
{
	  struct dirent *ent;
	  char str[12], *strx, s[256];
	  IplImage *img;

  while ((ent = readdir (dir)) != NULL) 
  {
		if (ent->d_name[0] == '.') continue;

	    if (ent->d_name[strlen(ent->d_name)-4] == '.')
	    {
	      strx = (char *)(&ent->d_name[strlen(ent->d_name)-3]);
		  strcpy (str, strx);
	      lowerCase (str);

		  if ( strcmp(str, "jpg")==0 ) printf ("File %s '%s' is JPEG.\n", name, ent->d_name);
		  else if ( strcmp(str, "gif")==0 ) printf ("File %s '%s' is GIF.\n", name, ent->d_name);
		  else if ( strcmp(str, "png")==0 ) printf ("File %s '%s' is PNG.\n", name, ent->d_name);
		  else if ( strcmp(str, "bmp")==0 ) printf ("File %s '%s' is BMP.\n", name, ent->d_name);
		  else if ( strcmp(str, "ppm")==0 ) printf ("File %s '%s' is PPM.\n", name, ent->d_name);
		  else if ( strcmp(str, "pgm")==0 ) printf ("File %s '%s' is PGM.\n", name, ent->d_name);
		  else if ( strcmp(str, "pbm")==0 ) printf ("File %s '%s' is PBM.\n", name, ent->d_name);
		  else if ( strcmp(str, "tga")==0 ) printf ("File %s '%s' is Targa.\n", name, ent->d_name);
		  else if ( strcmp(str, "tif")==0 ) printf ("File %s '%s' is TIFF.\n", name, ent->d_name);
		  else if ( strcmp(str, "pcx")==0 ) printf ("File %s '%s' is PCX.\n", name, ent->d_name);
		  else continue;

		  // Display the image
			sprintf (s, "%s\\%s", name, ent->d_name);
			img = cvLoadImage (s, 1);
			cvShowImage("Image Dataset", img );
			cvWaitKey(1);
			cvReleaseImage (&img);
		  count++;
	    }
  }
}

int main(int argc, char *argv[])
{
  IplImage *img=0; 
  int mean=0,count=0;
  DIR *dir, *dir2;
  struct dirent *ent;
  char str[12], *strx, path[256], name2[256];

	strcpy (path, "c:\\AIPCV\\png4");
	dir = opendir (path);
	cvNamedWindow("Image Dataset", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("Image Dataset", 100, 100);

	if (dir != NULL) 
	{

// Examine all the files and directories within the image main directory 
		while ((ent = readdir (dir)) != NULL) 
		{
			if (ent->d_name[0] == '.') continue;

// Open the file as a directory.
			sprintf (name2, "%s\\%s", path, ent->d_name);
			dir2 = opendir (name2);

			if (dir2 > 0) 
			{
				doSubdir (name2, dir2);  // It worked. Scan for image files.
			} else 
			{
				if (ent->d_name[strlen(ent->d_name)-4] == '.')  // It was not a directory, just a file.
				{
					strx = (char *)(&ent->d_name[strlen(ent->d_name)-3]);
					strcpy (str, strx);
					lowerCase (str);

// Is it an image file? (Using the name of the file only)
					if ( strcmp(str, "jpg")==0 ) printf ("File '%s' is JPEG.\n", ent->d_name);
					else if ( strcmp(str, "png")==0 ) printf ("File '%s' is PNG.\n", ent->d_name);
					else if ( strcmp(str, "bmp")==0 ) printf ("File '%s' is BMP.\n", ent->d_name);
					else if ( strcmp(str, "ppm")==0 ) printf ("File '%s' is PPM.\n", ent->d_name);
					else if ( strcmp(str, "pgm")==0 ) printf ("File '%s' is PGM.\n", ent->d_name);
					else if ( strcmp(str, "pbm")==0 ) printf ("File '%s' is PBM.\n", ent->d_name);
					else if ( strcmp(str, "tga")==0 ) printf ("File '%s' is Targa.\n", ent->d_name);
					else if ( strcmp(str, "tif")==0 ) printf ("File '%s' is TIFF.\n", ent->d_name);
					else if ( strcmp(str, "pcx")==0 ) printf ("File '%s' is PCX.\n", ent->d_name);
					else continue;

// Display the image
					img = cvLoadImage (name2, 1);
					cvShowImage("Image Dataset", img );
					cvWaitKey(1);
					cvReleaseImage (&img);
				}
			}
		}
		closedir (dir);
	} else 
	{
// could not open directory
		perror ("");
		return 1;
	}

	return 0;
}
