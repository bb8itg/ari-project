// Create a master list of image files.
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <dirent.h>

CvScalar rainbow[10];
int THRESHOLD = 15;
count = 0;
FILE *master;

void insert (char *s1, char *s2)
{
	fprintf (master, "%s %s\n", s1, s2);
}

int match (CvScalar s)
{
	int i, k=0;
	double v1, v2, v3;
	double d = 1.0e32, dmin = 1.0e32;

		for (i=0; i<10; i++)
		{
			v1 = s.val[0] - rainbow[i].val[0];
			v2 = s.val[1] - rainbow[i].val[1];
			v3 = s.val[2] - rainbow[i].val[2];
			d = v1*v1 + v2*v2 + v3*v3;
			if (d < dmin)
			{
				dmin = d;
				k = i;
			}
		}
		return k;
}

isGrey (CvScalar s)
{
	int d1, d2, d3;

	d1 = s.val[0] - s.val[1]; if (d1<0) d1 = -d1;
	d2 = s.val[1] - s.val[2]; if (d2<0) d2 = -d2;
	d3 = s.val[0] - s.val[2]; if (d3<0) d3 = -d3;
	if (d1 < d2) d1 = d2;
	if (d1 < d3) d1 = d3;
	if (d3 < THRESHOLD) return 1;
	else return 0;
}

void lowerCase (char *s)
{
	while (*s)
	{
		if ( (*s > 'A') && (*s <= 'Z') ) *s = 'a' + (*s-'A');
		s++;
	}
}

// Scan a directory within the main path, looking for image file. Insert them into 
// the master list when one is seen.
void doSubdir (char *name, DIR *dir)
{
	  struct dirent *ent;
	  char str[12], *strx;

  while ((ent = readdir (dir)) != NULL) 
  {

	    if (ent->d_name[strlen(ent->d_name)-4] == '.')
	    {
	      strx = (char *)(&ent->d_name[strlen(ent->d_name)-3]);
		  strcpy (str, strx);
	      lowerCase (str);

		  if ( strcmp(str, "jpg")==0 ) printf ("File %s '%s' is JPEG.\n", name, ent->d_name);
		  else if ( strcmp(str, "gif")==0 ) printf ("File %s '%s' is GIF.\n", name, ent->d_name);
		  else if ( strcmp(str, "png")==0 ) printf ("File %s '%s' is PNG.\n", name, ent->d_name);
		  else if ( strcmp(str, "bmp")==0 ) printf ("File %s '%s' is BMP.\n", name, ent->d_name);
		  else if ( strcmp(str, "ppm")==0 ) printf ("File %s '%s' is PPM.\n", name, ent->d_name);
		  else if ( strcmp(str, "pgm")==0 ) printf ("File %s '%s' is PGM.\n", name, ent->d_name);
		  else if ( strcmp(str, "pbm")==0 ) printf ("File %s '%s' is PBM.\n", name, ent->d_name);
		  else if ( strcmp(str, "tga")==0 ) printf ("File %s '%s' is Targa.\n", name, ent->d_name);
		  else if ( strcmp(str, "tif")==0 ) printf ("File %s '%s' is TIFF.\n", name, ent->d_name);
		  else if ( strcmp(str, "pcx")==0 ) printf ("File %s '%s' is PCX.\n", name, ent->d_name);
		  else continue;

		  insert (name, ent->d_name);
		  count++;
	    }
  }
}

int main(int argc, char *argv[])
{
  IplImage *img=0, *hsv=0; 
  int height,width,step,channels;
  uchar *data;
  int i,j,mean=0,count=0, depth, k;
  CvSize size;
  double c;
  DIR *dir, *dir2;
  struct dirent *ent;
  CvScalar s;
  char str[12], *strx, path[256], name2[256];

strcpy (path, "c:\\AIPCV\\png4");
dir = opendir (path);
if (dir != NULL) 
{

	master = fopen ("c:\\AIPCV\\master.txt", "w");

// print all the files and directories within directory 
  while ((ent = readdir (dir)) != NULL) 
  {
	  sprintf (name2, "%s\\%s", path, ent->d_name);
	  dir2 = opendir (name2);
	  if (dir2 > 0) 
	  {
		  doSubdir (name2, dir2);
	  } else 
	  {
	    if (ent->d_name[strlen(ent->d_name)-4] == '.')
	    {
	      strx = (char *)(&ent->d_name[strlen(ent->d_name)-3]);
		  strcpy (str, strx);
	      lowerCase (str);

		  if ( strcmp(str, "jpg")==0 ) printf ("File '%s' is JPEG.\n", ent->d_name);
		  else if ( strcmp(str, "gif")==0 ) printf ("File '%s' is GIF.\n", ent->d_name);
		  else if ( strcmp(str, "png")==0 ) printf ("File '%s' is PNG.\n", ent->d_name);
		  else if ( strcmp(str, "bmp")==0 ) printf ("File '%s' is BMP.\n", ent->d_name);
		  else if ( strcmp(str, "ppm")==0 ) printf ("File '%s' is PPM.\n", ent->d_name);
		  else if ( strcmp(str, "pgm")==0 ) printf ("File '%s' is PGM.\n", ent->d_name);
		  else if ( strcmp(str, "pbm")==0 ) printf ("File '%s' is PBM.\n", ent->d_name);
		  else if ( strcmp(str, "tga")==0 ) printf ("File '%s' is Targa.\n", ent->d_name);
		  else if ( strcmp(str, "tif")==0 ) printf ("File '%s' is TIFF.\n", ent->d_name);
		  else if ( strcmp(str, "pcx")==0 ) printf ("File '%s' is PCX.\n", ent->d_name);
	    }
	  }
  }
  closedir (dir);
} else 
{
// could not open directory
  perror ("");
  return 1;
}
	fclose (master);
  return 0;
}
/*


  // load an image  
  img=cvLoadImage("C:/AIPCV/pic1.jpg", 1);
  if(!img)
  {
    printf("Could not load image file\n");
    return 0;
  }

  // get the image data
  height    = img->height;
  width     = img->width;
  step      = img->widthStep;
  channels  = img->nChannels;
  data      = (uchar *)img->imageData;
  printf("Processing a %dx%d image with %d channels\n",height,width,channels); 

  // create a window
  cvNamedWindow("win1", CV_WINDOW_AUTOSIZE); 
  cvMoveWindow("win1", 100, 100);

  // show the image
  cvShowImage("win1", img );
  cvWaitKey(0);

   size = cvGetSize(img);
   depth = img->depth;
   hsv = cvCreateImage(size, depth, 3);
   cvZero(hsv);
 
    // Convert from Red-Green-Blue to Hue-Saturation-Value 
   cvCvtColor( img, hsv, CV_BGR2HSV );

  // Set rainbow colors in RGB
/*  rainbow[0].val[0] = 0;   rainbow[0].val[1] = 0;   rainbow[0].val[2] = 200;  //Red
  rainbow[1].val[0] = 0;   rainbow[1].val[1] = 165; rainbow[1].val[2] = 255;  // Orange
  rainbow[2].val[0] = 0;   rainbow[2].val[1] = 255; rainbow[2].val[2] = 255;  // Yellow
  rainbow[3].val[0] = 0;   rainbow[3].val[1] = 200; rainbow[3].val[2] = 0;    // Green
  rainbow[4].val[0] = 200; rainbow[4].val[1] = 0;   rainbow[4].val[2] = 0;    // Blue
  rainbow[5].val[0] = 0; rainbow[5].val[1] = 255;   rainbow[5].val[2] = 255;   // Cyan
  rainbow[6].val[0] = 255; rainbow[6].val[1] = 0; rainbow[6].val[2] = 255;  // Magenta
  rainbow[7].val[0] = 0;   rainbow[7].val[1] = 0;   rainbow[7].val[2] = 0;    // Black
  rainbow[8].val[0] = 255; rainbow[8].val[1] = 255; rainbow[8].val[2] = 255;  // White 

  rainbow[0].val[0] = 0;   rainbow[0].val[1] = 0;   rainbow[0].val[2] = 170;  // Red
  rainbow[1].val[0] = 0;   rainbow[1].val[1] = 85;  rainbow[1].val[2] = 170;  // Orange
  rainbow[2].val[0] = 0;   rainbow[2].val[1] = 170; rainbow[2].val[2] = 170;  // Yellow
  rainbow[3].val[0] = 0;   rainbow[3].val[1] = 170; rainbow[3].val[2] = 0;    // Green
  rainbow[4].val[0] = 170; rainbow[4].val[1] = 0;   rainbow[4].val[2] = 0;    // Blue
  rainbow[5].val[0] = 170; rainbow[5].val[1] = 0;   rainbow[5].val[2] = 85;   // indigo
  rainbow[6].val[0] = 0;   rainbow[6].val[1] = 170; rainbow[6].val[2] = 85;  // Magenta
  rainbow[7].val[0] = 0;   rainbow[7].val[1] = 0;   rainbow[7].val[2] = 0;    // Black
  rainbow[8].val[0] = 255; rainbow[8].val[1] = 255; rainbow[8].val[2] = 255;  // White
  rainbow[9].val[0] = 128; rainbow[9].val[0] = 128; rainbow[9].val[0] = 128;  // Grey
  

  

  for (i=0; i<img->height; i++)
		 for (j=0; j<img->width; j++)
		 {
			s = cvGet2D (img, i, j);
			if (isGrey (s))
			{
				s.val[0] = s.val[1] = s.val[2] = 128;
			} else
			{
				k = match (s);

				s.val[0] = rainbow[k].val[0];
				s.val[1] = rainbow[k].val[1];
				s.val[1] = rainbow[k].val[2];
			}

			/*
			c = s.val[0];			// Hue
			if ((c < 16) || (c >= 240)) s.val[0] = 0;
			else if ((c >=16 && c<48)) s.val[0] = 32;
			else if ((c>=48)&&(c<80)) s.val[0] = 64;
			else if ((c>=80) && (c<112)) s.val[0] = 96;
			else if ((c>=112) && (c<144)) s.val[0] = 128;
			else if ((c>=144) && (c<176)) s.val[0] = 160;
			else if ((c>=176) && (c<208)) s.val[0] = 192;
			else if ((c>=208) && (c<240)) s.val[0] = 224;
			s.val[1] = 255; s.val[2] = 255; 
			cvSet2D (img, i, j, s);
		 }
//    cvCvtColor( hsv, img, CV_HSV2BGR );

	cvNamedWindow( "New Colors", CV_WINDOW_AUTOSIZE);
	cvShowImage( "New Colors", img );
    cvWaitKey(0);


  return 0;
}
*/