// Create a master list of grey image files.
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>
#include <dirent.h>

int THRESHOLD = 15;
count = 0;
FILE *master;

void insert (char *s1, char *s2)
{
	fprintf (master, "%s %s\n", s1, s2);
} 

void lowerCase (char *s)
{
	while (*s)
	{
		if ( (*s > 'A') && (*s <= 'Z') ) *s = 'a' + (*s-'A');
		s++;
	}
}

// Scan a directory within the main path, looking for image file. Insert them into 
// the master list when one is seen.
void doSubdir (char *name, DIR *dir)
{
	  struct dirent *ent;
	  char str[12], *strx;

  while ((ent = readdir (dir)) != NULL) 
  {

	    if (ent->d_name[strlen(ent->d_name)-4] == '.')
	    {
	      strx = (char *)(&ent->d_name[strlen(ent->d_name)-3]);
		  strcpy (str, strx);
	      lowerCase (str);

		  if ( strcmp(str, "jpg")==0 ) printf ("File %s '%s' is JPEG.\n", name, ent->d_name);
		  else if ( strcmp(str, "gif")==0 ) printf ("File %s '%s' is GIF.\n", name, ent->d_name);
		  else if ( strcmp(str, "png")==0 ) printf ("File %s '%s' is PNG.\n", name, ent->d_name);
		  else if ( strcmp(str, "bmp")==0 ) printf ("File %s '%s' is BMP.\n", name, ent->d_name);
		  else if ( strcmp(str, "ppm")==0 ) printf ("File %s '%s' is PPM.\n", name, ent->d_name);
		  else if ( strcmp(str, "pgm")==0 ) printf ("File %s '%s' is PGM.\n", name, ent->d_name);
		  else if ( strcmp(str, "pbm")==0 ) printf ("File %s '%s' is PBM.\n", name, ent->d_name);
		  else if ( strcmp(str, "tga")==0 ) printf ("File %s '%s' is Targa.\n", name, ent->d_name);
		  else if ( strcmp(str, "tif")==0 ) printf ("File %s '%s' is TIFF.\n", name, ent->d_name);
		  else if ( strcmp(str, "pcx")==0 ) printf ("File %s '%s' is PCX.\n", name, ent->d_name);
		  else continue;

		  insert (name, ent->d_name);
		  count++;
	    }
  }
}

int main(int argc, char *argv[])
{
  IplImage *img=0, *hsv=0; 
  int mean=0,count=0;
  DIR *dir, *dir2;
  struct dirent *ent;
  char str[12], *strx, path[256], name2[256];

strcpy (path, "c:\\AIPCV\\grey");
dir = opendir (path);
if (dir != NULL) 
{

	master = fopen ("c:\\AIPCV\\greyFiles.txt", "w");

// print all the files and directories within directory 
  while ((ent = readdir (dir)) != NULL) 
  {
	  sprintf (name2, "%s\\%s", path, ent->d_name);
	  dir2 = opendir (name2);
	  if (dir2 > 0) 
	  {
		  doSubdir (name2, dir2);
	  } else 
	  {
	    if (ent->d_name[strlen(ent->d_name)-4] == '.')
	    {
	      strx = (char *)(&ent->d_name[strlen(ent->d_name)-3]);
		  strcpy (str, strx);
	      lowerCase (str);

		  if ( strcmp(str, "jpg")==0 ) printf ("File '%s' is JPEG.\n", ent->d_name);
		  else if ( strcmp(str, "gif")==0 ) printf ("File '%s' is GIF.\n", ent->d_name);
		  else if ( strcmp(str, "png")==0 ) printf ("File '%s' is PNG.\n", ent->d_name);
		  else if ( strcmp(str, "bmp")==0 ) printf ("File '%s' is BMP.\n", ent->d_name);
		  else if ( strcmp(str, "ppm")==0 ) printf ("File '%s' is PPM.\n", ent->d_name);
		  else if ( strcmp(str, "pgm")==0 ) printf ("File '%s' is PGM.\n", ent->d_name);
		  else if ( strcmp(str, "pbm")==0 ) printf ("File '%s' is PBM.\n", ent->d_name);
		  else if ( strcmp(str, "tga")==0 ) printf ("File '%s' is Targa.\n", ent->d_name);
		  else if ( strcmp(str, "tif")==0 ) printf ("File '%s' is TIFF.\n", ent->d_name);
		  else if ( strcmp(str, "pcx")==0 ) printf ("File '%s' is PCX.\n", ent->d_name);
	    }
	  }
  }
  closedir (dir);
} else 
{
// could not open directory
  perror ("");
  return 1;
}
	fclose (master);
  return 0;
}
