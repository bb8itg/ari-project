// Capture.c
//
// Example showing how to connect to a webcam and capture
// video frames
#include "stdafx.h"
#include "stdio.h"
#include "string.h"
#include "cv.h"
#include "highgui.h"

int main(int argc, char ** argv)
{
  CvCapture * camera    = 0;
  IplImage *  frame = 0;
  int         i, n=0;
  char        filename[50];
  char		  c;

  // Initialize video capture
  camera = cvCaptureFromCAM( CV_CAP_ANY );
  if( !camera )
  {
    fprintf(stderr, "failed to initialize video capture\n");
    return -1;
  }

 // create a window
  cvNamedWindow("mainWin", CV_WINDOW_AUTOSIZE); 
  cvMoveWindow("mainWin", 100, 100);

  // Capture three video frames and write them as files
  for(i=0; i<600; i++)
  {
    frame = cvQueryFrame( camera );
    if( !frame )
    {
      fprintf(stderr, "failed to get a video frame\n");
    }

// wait for a key
    c = cvWaitKey(100);

  // show the image
	 cvShowImage("mainWin", frame);

	 if (c>0)
	 {
		 sprintf(filename, "H:/AIPCV/VideoFrame%d.jpg", n++);
		if( !cvSaveImage(filename, frame) )
		{
			fprintf(stderr, "failed to write image file %s\n", filename);
		} else fprintf (stderr, "Saved VideoFrame%d.jpg\n", n-1);
     }
  }
  // Terminate video capture and free capture resources
     cvReleaseCapture( &camera );
// wait for a key
     cvWaitKey(0);

  return 0;
}