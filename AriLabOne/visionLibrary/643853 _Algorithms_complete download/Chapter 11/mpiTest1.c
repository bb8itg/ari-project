/* Full scale MPI program that can run in parallel */
#include <mpi.h> 
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <windows.h>

void master (int size)
{
    int partner, i, more=100, more1=100;
	MPI_Status stat;
	unsigned char data[1024];
	LARGE_INTEGER start, stop, quantum;
	double xtime;

	for (i=0; i<1024; i++) data[i] = i;

	QueryPerformanceCounter(&start);
	printf ("Procesor 0, starting deal...\n"); fflush(stdout);
	while (more)
	{
		for (partner = 1; partner < size; partner++)
		{
			if (more) 
			{
				printf ("dealing %d\n", more); fflush (stdout);
				MPI_Send (data, 1024, MPI_UNSIGNED_CHAR, partner, 1, MPI_COMM_WORLD);
				more--;
			}
			else break;
		}

		for (partner = 1; partner < size; partner++)
		{
			if (more1)
			{
				MPI_Recv (data, 1024, MPI_UNSIGNED_CHAR, partner, 1, MPI_COMM_WORLD, &stat);
				more1--;
			}
			else break;
		}
	}
	for (partner = 1; partner < size; partner++)
	{
		MPI_Send (data, 1, MPI_UNSIGNED_CHAR, partner, 0, MPI_COMM_WORLD);
	}
	printf ("Procesor 0, ending deal...\n"); fflush(stdout);
	QueryPerformanceCounter(&stop);
	QueryPerformanceFrequency( &quantum ) ;
	xtime = (double)(stop.QuadPart - start.QuadPart)/(double)(quantum.QuadPart);
	printf ("Done. Time = %lf \n", xtime); fflush(stdout);

}

void slave (int my_rank, int size)
{
	int i, j;
	MPI_Status stat;
	unsigned char data[1024];

	do
	{
		MPI_Recv(data, 1024, MPI_UNSIGNED_CHAR, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
		printf ("Processor %d Received packet from 0.\n", my_rank); fflush(stdout);
		printf ("Tag is %d\n", stat.MPI_TAG);

		if(stat.MPI_TAG == 0) break;
		for (j=0; j<1000; j++)	// Simulated work load - put real work here.
		{
			for (i=0; i<1024; i++)
				data[i] = data[i]*data[i];
			for (i=0; i<1024; i++)
				data[i] = (unsigned char)sqrt( (double)data[i] );
		}

		MPI_Send(data, 1024, MPI_UNSIGNED_CHAR, 0,1,MPI_COMM_WORLD);
		printf ("Processor %d sent packet to 0.\n", my_rank); fflush(stdout);
	} while (i);
	printf ("Processor %d is stopping.\n", my_rank); fflush(stdout);
}

int main(int argc, char **argv) {
  
  int my_rank; 
  int size;

  MPI_Init(0, 0); /*Start MPI */
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
  MPI_Comm_size(MPI_COMM_WORLD, &size); 
  printf ("Initialization complete.\n");
  
  if (my_rank ==0)		// Master
	master (size);
  else				    // Slave
    slave (my_rank, size);

  if (my_rank == 0) printf("That is all for now!\n");
  MPI_Finalize();  /* EXIT MPI */
  return 0;
}
