/* example of the use of clock() to time code */
#include "mpi.h" 
#include <stdio.h> 
#include <windows.h>
#include <math.h>
#include <time.h>

#define BUFSIZE 1024 
#define ITERS   1000

int main(int argc, char *argv[]) 
{ 
    int i, j, niter=0;
	double xtime, dat[BUFSIZE], y, z;
	clock_t cstart, cstop;


	for (niter=ITERS; niter < ITERS*1000; niter *= 2)
	{
		cstart = clock();
		for (j=0; j<niter; j++)
		{
			for (i=0; i<BUFSIZE; i++)
			{
				dat[i] = sin (3.1415926535 * i/1024.0);
			}
		}
		xtime = (double)(clock() - cstart)*CLOCKS_PER_SEC/1000.0;
		printf ("Iterations %d  Clock time is %lf = %lf\n", 
				niter, xtime, xtime/niter);
	}
	printf ("Returning.\n");
    return 0; 
} 