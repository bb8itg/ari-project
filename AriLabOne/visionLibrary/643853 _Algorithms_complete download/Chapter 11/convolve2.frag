// convolve2 - passes image width and height
varying vec2 TexCoord;

#define KERNEL_SIZE 9
float kernel[KERNEL_SIZE];

uniform sampler2D colorMap;
uniform float Width;
uniform float Height;

float dw;
float dh;

vec2 offset[KERNEL_SIZE];
						 
void main(void)
{
   vec4 sum = vec4(0.0);
   vec4 tmp;

	dw = 1.0/Width;
	dh= 1.0/Height;

   offset[0] = vec2(-dw, -dh); offset[1] = vec2(0.0, -dh); offset[2] = vec2(dw, -dh); 
   offset[3] = vec2(-dw, 0.0); offset[4] = vec2(0.0, 0.0); offset[5] = vec2(dw, 0.0);
   offset[6] = vec2(-dw, dh);  offset[7] = vec2(0.0, dh); offset[8] = vec2(dw, dh);

   //sharpening 
        kernel[0] = -1.0;  kernel[1] = -1.0;	kernel[2] = -1.0;
        kernel[3] = -1.0;  kernel[4] =  9.0;	kernel[5] = -1.0;
        kernel[6] = -1.0;  kernel[7] = -1.0;	kernel[8] = -1.0;
 
	tmp = texture2D(colorMap, TexCoord.st + offset[0]);
	sum += tmp * kernel[0];
	tmp = texture2D(colorMap, TexCoord.st + offset[1]);
	sum += tmp * kernel[1];
	tmp = texture2D(colorMap, TexCoord.st + offset[2]);
	sum += tmp * kernel[2];
	tmp = texture2D(colorMap, TexCoord.st + offset[3]);
	sum += tmp * kernel[3];
	tmp = texture2D(colorMap, TexCoord.st + offset[4]);
	sum += tmp * kernel[4];
	tmp = texture2D(colorMap, TexCoord.st + offset[5]);
	sum += tmp * kernel[5];
	tmp = texture2D(colorMap, TexCoord.st + offset[6]);
	sum += tmp * kernel[6];
	tmp = texture2D(colorMap, TexCoord.st + offset[7]);
	sum += tmp * kernel[7];
	tmp = texture2D(colorMap, TexCoord.st + offset[8]);
	sum += tmp * kernel[8];
 
   gl_FragColor = sum;
}