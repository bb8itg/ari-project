/* Full scale MPI program that can run in parallel */
#include <mpi.h> 
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include "cv.h"
#include "highgui.h"

#define BUFSIZE 1024 
#define ITERS   1000

/* The image header data structure      */
struct header 
{
	int nr, nc;             /* Rows and columns in the image */
	int oi, oj;             /* Origin */
};

/*      The IMAGE data structure        */
struct image 
{
		struct header *info;            /* Pointer to header */
		unsigned char **data;           /* Pixel values */
};

#define SQRT2 1.414213562
#define BLACK 0
#define WHITE 1

long seed = 132531;
typedef struct image * IMAGE;

void piksrt (unsigned char arr[], int n);
unsigned char med13(IMAGE data, int i, int j);
IMAGE get_image (char *name); 
void save_image (IMAGE x, char *name);
void display_image (IMAGE x);
int range (IMAGE im, int i, int j);
void print_se (IMAGE p);
IMAGE Input_PBM (char *fn);
IMAGE Output_PBM (IMAGE image, char *filename);
void get_num_pbm (FILE *f, char *b, int *bi, int *res);
void pbm_getln (FILE *f, char *b);
void pbm_param (char *s);
struct image  *newimage (int nr, int nc);
void freeimage (struct image  *z);
void sys_abort (int val, char *mess);
void CopyVarImage (IMAGE *a, IMAGE *b);
void Display (IMAGE x);
float ** f2d (int nr, int nc);
void srand32 (long k);
double drand32 ();
void copy (IMAGE *a, IMAGE b);

struct image  *newimage (int nr, int nc)
{
	struct image  *x;                /* New image */
	int i;
	unsigned char *p;

	if (nr < 0 || nc < 0) 
	{
		printf ("Error: Bad image size (%d,%d)\n", nr, nc);
		return 0;
	}

/*      Allocate the image structure    */
	x = (struct image  *) malloc( sizeof (struct image) );
	if (!x) 
	{
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}

/*      Allocate and initialize the header      */

	x->info = (struct header *)malloc( sizeof(struct header) );
	if (!(x->info)) 
	{
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}
	x->info->nr = nr;       x->info->nc = nc;
	x->info->oi = x->info->oj = 0;

/*      Allocate the pixel array        */

	x->data = (unsigned char **)malloc(sizeof(unsigned char *)*nr); 

/* Pointers to rows */
	if (!(x->data)) 
	{
		printf ("Out of storage in NEWIMAGE.\n");
		return 0;
	}

	x->data[0] = (unsigned char *)malloc (nr*nc);
	p = x->data[0];
	if (x->data[0]==0)
	  {
		printf ("Out of storage. Newimage  \n");
		exit(1);
	  }

	for (i=1; i<nr; i++) 
	  x->data[i] = (p+i*nc);

	return x;
}

void freeimage (struct image  *z)
{
/*      Free the storage associated with the image Z    */

	if (z != 0) 
	{
	   free (z->data[0]);
	   free (z->info);
	   free (z->data);
	   free (z);
	}
}

float ** f2d (int nr, int nc)
{
	float **x;
	int i;

	x = (float **)calloc ( nr, sizeof (float *) );
	if (x == 0)
	{
	  fprintf (stderr, "Out of storage: F2D.\n");
	  exit (1);
	}

	for (i=0; i<nr; i++)
	{  
	  x[i] = (float *) calloc ( nc, sizeof (float)  );
	  if (x[i] == 0)
	  {
	    fprintf (stderr, "Out of storage: F2D %d.\n", i);
	    exit (1);
	  }
	}
	return x;
}

void free2d (float **x, int nr)
{
	int i;

	for (i=0; i<nr; i++)
		free(x[i]);
	free (x);
}

/* Small system random number generator */
double drand32 ()
{
	static long a=16807L, m=2147483647L,
		    q=127773L, r = 2836L;
	long lo, hi, test;

	hi = seed / q;
	lo = seed % q;
	test = a*lo -r*hi;
	if (test>0) seed = test;
	else seed = test + m;

	return (double)seed/(double)m;
}

void srand32 (long k)
{
	seed = k;
}
/*      Check that a pixel index is in range. Return TRUE(1) if so.     */

int range (IMAGE im, int i, int j)
{
	if ((i<0) || (i>=im->info->nr)) return 0;
	if ((j<0) || (j>=im->info->nc)) return 0;
	return 1;
}

/*      PRINT_SE - Print a structuring element to the screen    */

void print_se (IMAGE p)
{
	int i,j;

	printf ("\n=====================================================\n");
	if (p == NULL)
	  printf (" Structuring element is NULL.\n");
	else 
	{
	  printf ("Structuring element: %dx%d origin at (%d,%d)\n",
		p->info->nr, p->info->nc, p->info->oi, p->info->oj);
	  for (i=0; i<p->info->nr; i++)
	  {
	    printf ("	");
	    for (j=0; j<p->info->nc; j++)
	      printf ("%4d ", p->data[i][j]);
	    printf ("\n");
	  }
	}
	printf ("\n=====================================================\n");
}

IMAGE Input_PBM (char *fn)
{
	int i,j,k,n,m,bi, b;
	unsigned char ucval;
	int val;
	long here;
	char buf1[256];
	FILE *f;
	IMAGE im;

	strcpy (buf1, fn);
	f = fopen (buf1, "r");
	if (f==NULL)
	{
	  printf ("Can't open the PBM file named '%s'\n", buf1);
	  return 0;
	}

	pbm_getln (f, buf1);
	if (buf1[0] == 'P')
	{
	  switch (buf1[1])
	  {
case '1':       k=1; break;
case '2':       k=2; break;
case '3':       k=3; break;
case '4':       k=4; break;
case '5':       k=5; break;
case '6':       k=6; break;
default:        printf ("Not a PBM/PGM/PPM file.\n");
		return 0;
	  }
	}
	bi = 2;

	get_num_pbm (f, buf1, &bi, &m);         /* Number of columns */
	get_num_pbm (f, buf1, &bi, &n);         /* Number of rows */
	if (k!=1 && k!=4) get_num_pbm (f, buf1, &bi, &b);      /* Max value */
	else b = 1;

	fprintf (stderr,"\nPBM file class %d size %d columns X %d rows Max=%d\n",
		k, m, n, b);
	
/* Binary file? Re-open as 'rb' */        
	if (k>3)
	{
	  here = ftell (f);
	  fclose (f);
	  f = fopen (fn, "rb");       
	  here++;
	  if (fseek(f, here, 0) != 0) 
	  {
	    printf ("Input_PBM: Sync error, file '%s'. Use ASCII PGM.\n",fn);
	    exit (3);
	  }
	}

/* Allocate the image */
	if (k==3 || k==6)       /* Colour */
	  sys_abort (0, "Colour image.");
	else 
	{
	  im = (IMAGE)newimage (n, m);
	  for (i=0; i<n; i++)
	    for (j=0; j<m; j++)
	      if (k<3)
	      {
		fscanf (f, "%d", &val);
		im->data[i][j] = (unsigned char)val;
	      } else {
		fscanf (f, "%c", &ucval);
		im->data[i][j] = ucval;
	      }
	}
	return im;
}

IMAGE Output_PBM (IMAGE image, char *filename)
{
	FILE *f;
	int i,j,k, perline;
	char buf1[64];

	strcpy (buf1, filename);
	if (image->info->nc > 20) perline = 20;
	 else perline = image->info->nc-1;
	f = fopen (buf1, "w");
	if (f == 0) sys_abort (0, "Can't open output file.");

	fprintf (f, "P2\n%d %d %d\n", image->info->nc, image->info->nr, 255);
	k = 0;
	for (i=0; i<image->info->nr; i++)
	  for (j=0; j<image->info->nc; j++)
	  {
		fprintf (f, "%d ", image->data[i][j]);
		k++;
		if (k > perline)
		{
		  fprintf (f, "\n");
		  k = 0;
		}
	  }
	fprintf (f, "\n");
	fclose (f);
	return image;
}

void get_num_pbm (FILE *f, char *b, int *bi, int *res)
{
	int i;
	char str[80];

	while (b[*bi]==' ' || b[*bi]=='\t' || b[*bi]=='\n')
	{
	  if (b[*bi] == '\n') 
	  {
	    pbm_getln (f, b);
	    *bi = 0;
	  } else
	  *bi += 1;
	}

	i = 0;
	while (b[*bi]>='0' && b[*bi]<='9')
	  str[i++] = b[(*bi)++];
	str[i] = '\0';
	sscanf (str, "%d", res);
}

/* Get the next non-comment line from the PBM file f into the
   buffer b. Look for 'pragmas' - commands hidden in the comments */

void pbm_getln (FILE *f, char *b)
{
	int i;
	char c;

/* Read the next significant line (non-comment) from f into buffer b */
	do
	{

/* Read the next line */
	  i = 0;
	  do
	  {
	    fscanf (f, "%c", &c);
	    b[i++] = c;
	    if (c == '\n') b[i] = '\0';
	  } while (c != '\n');

/* If a comment, look for a special parameter */
	  if (b[0] == '#') pbm_param (b);

	} while (b[0]=='\n' || b[0] == '#');
}

/*      Look for a parameter hidden in a comment        */
void pbm_param (char *s)
{
	int i,j;
	char key[24];

/* Extract the key word */
	for (i=0; i<23; i++)
	{
	  j = i;
	  if (s[i+1] == ' ' || s[i+1] == '\n') break;
	  key[i] = s[i+1];
	}
	key[j] = '\0';

/* Convert to lower case */
	for (i=0; i<j; i++)
	  if ( (key[i]>='A') && (key[i]<='Z') )
		key[i] = (char) ( (int)key[i] - (int)'A' + (int)'a' );

}

void sys_abort (int val, char *mess)
{
	fprintf (stderr, "**** System library ABORT %d: %s ****\n", 
			val, mess);
	exit (2);
}

void copy (IMAGE *a, IMAGE b)
{
	CopyVarImage (a, &b);
}

void CopyVarImage (IMAGE *a, IMAGE *b)
{
	int i,j;

	if (a == b) return;
	if (*a) freeimage (*a);
	*a = newimage ((*b)->info->nr, (*b)->info->nc);
	if (*a == 0) sys_abort (0, "No more storage.\n");

	for (i=0; i<(*b)->info->nr; i++)
	  for (j=0; j< (*b)->info->nc; j++)
	    (*a)->data[i][j] = (*b)->data[i][j];
	(*a)->info->oi = (*b)->info->oi;
	(*a)->info->oj = (*b)->info->oj;
}

void piksrt (unsigned char arr[], int n)
{
	int i,j;
	unsigned char a;

	for (j=1; j<n; j++) {
	   a = arr[j];
	   i = j - 1;
	   while (i>=0 && arr[i]>a) {
		arr[i+1] = arr[i];
		i--;
	   }
	   arr[i+1] = a;
	}
}

unsigned char med13(IMAGE im, int ii, int jj)
{
        int  k1=13;
        unsigned char dat[20];

                                                            dat[0] = im->data[ii-2][jj]; 
		                     dat[1] = im->data[ii-1][jj-1]; dat[2] = im->data[ii-1][jj]; dat[3] = im->data[ii-1][jj+1]; 
dat[4] = im->data[ii][jj-2]; dat[5] = im->data[ii][jj-1];   dat[6] = im->data[ii][jj];   dat[7] = im->data[ii][jj+1];   dat[8] = im->data[ii][jj+2];
                             dat[9] = im->data[ii+1][jj-1]; dat[10]= im->data[ii+1][jj]; dat[11]= im->data[ii+1][jj+1];
		                                                    dat[12] =im->data[ii+2][jj];

		piksrt (dat, 13);
		return dat[6];
}

void master (int size, IMAGE im)
{
    int partner, i, j, more=100, more1=100;
	int nr, nc, n, b1[2], rstart, rend;
	MPI_Status stat;
	unsigned char *data;
	LARGE_INTEGER start, stop, quantum;
	double xtime;
	char name[128];

	nr = im->info->nr; nc = im->info->nc;
	if (size-1 > 1)
		n = (nr+(size-1)*4)/(size-1);  // How many rows per processor
	else n = nr;
	data = malloc (n*nc);     // Buffer for n rows
	b1[0] = n; b1[1] = nc;

	QueryPerformanceCounter(&start);
	printf ("Procesor 0, starting deal...\n"); fflush(stdout);

		j = 0;
		for (partner = 1; partner < size; partner++)
		{
			rstart = j; rend = j+n+2; if (rend>=nr) rend = nr-1;
			b1[0] = (rend-rstart+1);
			MPI_Send (b1, 2, MPI_INT, partner, 1, MPI_COMM_WORLD);
			printf ("Dealing %d having %d rows and %d columns from %d to %d\n", partner, n, nc, rstart, rend); 
			fflush (stdout);
			MPI_Send (im->data[rstart], (rend-rstart+1)*nc, MPI_UNSIGNED_CHAR, partner, 1, MPI_COMM_WORLD);
			j = rend-3;
		}

		j = 0;
		for (partner = 1; partner < size; partner++)
		{
			rstart = j; rend = j+n+2; if (rend>=nr) rend = nr-1;
			printf ("Getting %d having %d rows and %d columns\n", partner, n, nc); 
			fflush (stdout);
			MPI_Recv (im->data[rstart], (rend-rstart+1)*nc, MPI_UNSIGNED_CHAR, partner, 1, MPI_COMM_WORLD, &stat);
			j = rend-3;
			sprintf  (name, "H:\\AIPCV\\stage%1d.pgm", partner);
			Output_PBM (im, name);
		}

	printf ("Procesor 0, ending deal...\n"); fflush(stdout);
	QueryPerformanceCounter(&stop);
	QueryPerformanceFrequency( &quantum ) ;
	xtime = (double)(stop.QuadPart - start.QuadPart)/(double)(quantum.QuadPart);
	printf ("Done. Time = %lf \n", xtime); fflush(stdout);
}

void slave (int my_rank, int size)
{
	int i, j, p[2], iter;
	MPI_Status stat;
	IMAGE im1, im2;

	MPI_Recv (p, 2, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
	printf ("Partner %d is given sizes: %d rows by %d columns.\n", my_rank, p[0], p[1]);

	im1 = newimage (p[0], p[1]);			// p[0] is Nrows, p[1] is Ncols.
	im2 = newimage (p[0], p[1]);

	MPI_Recv(im1->data[0], (4+p[0])*p[1], MPI_UNSIGNED_CHAR, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
	printf ("Processor %d Received pixels from 0.\n", my_rank); fflush(stdout);
	printf ("Tag is %d\n", stat.MPI_TAG);

	for (i=0; i<p[0]; i++)
		for (j=0; j<p[1]; j++)
			im2->data[i][j] = 0;

	for (iter = 0; iter<5; iter++)
	{
		for (i=2; i<p[0]-2; i++)		// Each assigned row, missing top and bottom 2.
			for (j=2; j<p[1]-2; j++)	// Each assigned column, missing first and last 2.
				im2->data[i][j] = med13 (im1, i, j);
		for (i=0; i<p[0]; i++)
			for (j=0; j<p[1]; j++)
				im1->data[i][j] = im2->data[i][j];
	}

	MPI_Send(im2->data[0], p[0]*p[1], MPI_UNSIGNED_CHAR, 0,1,MPI_COMM_WORLD);
	printf ("Processor %d sent packet to 0.\n", my_rank); fflush(stdout);
	printf ("Processor %d is stopping.\n", my_rank); fflush(stdout);
}

int main(int argc, char **argv) {
  
	int my_rank; 
	int size;
	IMAGE inImage;

  inImage = Input_PBM ("H:\\AIPCV\\noise.pgm");

  MPI_Init(0, 0); /*Start MPI */
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank); 
  MPI_Comm_size(MPI_COMM_WORLD, &size); 
  printf ("Initialization complete.\n");
  
 if (my_rank ==0)		// Master
	master (size, inImage);
  else				    // Slave
    slave (my_rank, size);

  if (my_rank == 0) printf("That is all for now!\n");
  MPI_Finalize();  /* EXIT MPI */
  return 0;
}
