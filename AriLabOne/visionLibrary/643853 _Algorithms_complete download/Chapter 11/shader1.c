#include <stdlib.h>
#include <stdio.h>
#include <GL/glew.h>
#include <stdlib.h>
#include <stdio.h>
#include <GL/glew.h>
#include <windows.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include "cv.h"
#include "highgui.h"

/*  Create checkerboard texture  */
int Width;
int Height;
static GLubyte *targetImage, *dupImage;
IplImage *image;
static GLuint texName, dupName;

char *readShader (char *name) 
{
	FILE *f;
	char *buffer = NULL, b=NULL;
	int n=0;
	int count=4096;

	if (name != NULL) 
	{
		f = fopen(name, "r");
		if (f != NULL)
		{
			buffer = (char *)malloc (count);
			n = fread (buffer, 1, count, f);	// Read a buffer full
			if (n < count) 
			{
				buffer[n] = '\0';
				return buffer;	
			}
			printf ("ERROR: Shader code file is too big!\n");
			exit (0);
		} 
		printf ("Can't open shader code file %s\b", name);
		exit (0);
	}
	printf ("File name passed to 'readShader' cannot be null.\n");
	exit(0);
}

void getImage (void)
{
	int i, j;
	GLubyte *p;
	CvScalar s;

	image = cvLoadImage("pic3.jpg", 1);
	Width = image->width; Height = image->height;
	cvNamedWindow("win1", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("win1", 100, 100);
	cvShowImage("win1", image );
	cvWaitKey(0);

	targetImage = (GLubyte *)malloc (image->width*image->height*4);
	dupImage = (GLubyte *)malloc (image->width*image->height*4);
	p = targetImage;

	for (i = 0; i < image->height; i++) 
	{
		for (j = 0; j < image->width; j++) 
		{
			s = cvGet2D (image, i, j);
			*p++ = (GLubyte) s.val[2];
			*p++ = (GLubyte) s.val[1];
			*p++ = (GLubyte) s.val[0];
			*p++ = (GLubyte) 255; 
      }
   }
}

void init(void)
{    
   glClearColor (0.0, 0.0, 0.0, 0.0);
   glShadeModel(GL_FLAT);
   glEnable(GL_DEPTH_TEST);

   getImage();                             // Read or create target image
   glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

   glGenTextures(1, &texName);
   glBindTexture(GL_TEXTURE_2D, texName);
//   glGenTextures(1, &dupName);
//   glBindTexture(GL_TEXTURE_2D, dupName);

   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
   glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, targetImage);
}

void display(void)
{
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glEnable(GL_TEXTURE_2D);
//   glActiveTexture (GL_TEXTURE0);
   glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
   glBindTexture(GL_TEXTURE_2D, texName);

   glBegin(GL_QUADS);

    glTexCoord2f(0.0, 1.0); glVertex3f(-1.0, -1.0, 0.0);
    glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, 1.0, 0.0);
    glTexCoord2f(1.0, 0.0); glVertex3f(1.0, 1.0, 0.0);
    glTexCoord2f(1.0, 1.0); glVertex3f(1.0, -1.0, 0.0);

   glEnd();
   glFlush();
   glDisable(GL_TEXTURE_2D);
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 1.0, 30.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, -3.6);
}

void keyboard (unsigned char key, int x, int y)
{
   switch (key) 
   {
      case 27:  exit(0);
                break;
      default:  break;
   }
}

void initializeShaders() 
{
	GLuint v,f, p;
	char *vs = NULL,*fs = NULL;
	GLint flag;
	GLsizei siz;
	char *log;

	v = glCreateShader(GL_VERTEX_SHADER);
	f = glCreateShader(GL_FRAGMENT_SHADER);

	vs = readShader("convolve.vert");
	printf ("Vertex shader:\n%s\n", vs);
	fs = readShader("convolve.frag");
	printf ("Fragment shader:\n%s\n", fs);

	const char * ff = fs;
	const char * vv = vs;

	glShaderSource(v, 1, &vv, NULL);
	glShaderSource(f, 1, &ff, NULL);

	free(vs); free(fs);

	glCompileShader(v);
	glGetShaderiv (v, GL_COMPILE_STATUS, &flag);
	if(flag == GL_FALSE)
	{
		printf ("Vertex shader program failed to compile.\n");
		log = (char *)malloc (2048);
		glGetShaderInfoLog (v, 2048, &siz, log);
		printf ("LOG: %s", log);
		free(log);
		exit(0);
	}

	glCompileShader(f);
	glGetShaderiv (f, GL_COMPILE_STATUS, &flag);
	if(flag == GL_FALSE)
	{
		printf ("Fragment shader program failed to compile.\n");
		log = (char *)malloc (2048);
		glGetShaderInfoLog (f, 2048, &siz, log);
		printf ("LOG: %s", log);
		free (log);
		exit(0);
	}

	p = glCreateProgram();
	glAttachShader(p,f);
	glAttachShader(p,v);

	glLinkProgram(p);
	glUseProgram(p);
}

int main(int argc, char** argv)
{
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH);
   glutInitWindowSize(250, 250);
   glutInitWindowPosition(100, 100);
   glutCreateWindow("OpenGL Shader 1");

	glutDisplayFunc(display);
	glutIdleFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);

	glewInit();
	if (glewIsSupported("GL_VERSION_2_0"))
		printf(" OpenGL 2.0 is supported.\n");
	else 
	{
		printf("OpenGL 2.0 is not supported - quitting.\n");
		exit(0);
	}

	initializeShaders();
	init();		// Initialize texture/image

	glutMainLoop();
	return 0; 
}
