/* Timing C code using QueryPerformanceCounter */
#include "mpi.h" 
#include <stdio.h> 
#include <windows.h>
#include <math.h>
#include <time.h>

#define BUFSIZE 1024 
#define ITERS   1000

int main(int argc, char *argv[]) 
{ 
    int i, j, niter=1000; 
	LARGE_INTEGER start, stop, quantum;
	double xtime, dat[BUFSIZE];


	for (niter=ITERS; niter<ITERS*1000; niter *= 2)
	{
		QueryPerformanceCounter(&start);
		for (j=0; j<niter; j++)
		{
			for (i=0; i<BUFSIZE; i++)
			{
				dat[i] = sin (3.1415926535 * i/1024.0);
			}
		}
		QueryPerformanceCounter(&stop);
		QueryPerformanceFrequency( &quantum ) ;
		xtime = (double)(stop.QuadPart - start.QuadPart)/(double)(quantum.QuadPart);
		printf ("Done. Time = %lf    Iterations %d  per iteration %lf\n", xtime, niter, xtime/niter * 1000);
	}

	printf ("Returning.\n");
    return 0; 
} 