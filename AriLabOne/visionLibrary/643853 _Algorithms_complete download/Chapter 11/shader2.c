#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "cv.h"
#include "highgui.h"
#include <windows.h>

int whichMode = 0;
float amount = 1.0;
int wWidth = 512, wHeight = 640;
static GLuint texName;
double cpuTime = 0.0;
char *texData1; 
char *frameBuf;
double xtime = 0.0, ntime = 0.0;
IplImage *image;
int Width, Height;
GLuint shaderProgram;
GLuint   tex1, frame;
GLuint p;

void resize (int w, int h) 
{
	float rat = 0.0;

	if(h <= 0) h = 1;
	rat = 1.0* wWidth / wHeight;

	// Reset the coordinate system before modifying
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	// Set the viewport to be the entire window
    glViewport(0, 0, wWidth, wHeight);

	// Set the correct perspective.
	gluPerspective(45, rat, 1, 1000);
	glMatrixMode(GL_MODELVIEW);
}

void render (void) 
{
    GLint texLoc;
	int k;
	LARGE_INTEGER start, stop, quantum;

	QueryPerformanceCounter(&start);

	glLoadIdentity();
	gluLookAt(0.0,0.0,5.0,  0.0,0.0,-1.0,  0.0f,1.0f,0.0f);
//    glBindTexture(GL_TEXTURE_2D, frame);
//    glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, wWidth, wHeight);
        
    // read the texture back out of the buffer here ///
//    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, frameBuf);
              
    glBindTexture(GL_TEXTURE_2D, tex1);
    glUseProgram(shaderProgram);

	// always using texture 0
    texLoc = glGetUniformLocation(shaderProgram, "colorMap");
	printf ("Name 'colorMap' is at %d in shader program.\n", texLoc);
	glUniform1i(texLoc, 0);

	k = glGetUniformLocation(shaderProgram, "tts");
	printf ("Error: Name 'tts' is at %d in shader program.\n", k);
	glUniform1i(k, 1);

//	    texLoc = glGetUniformLocation(shaderProgram, "colorMap");
//    glUniform1i(texLoc, 1);

//    glColor3f(1.0, 1.0, 1.0);
    glBegin( GL_QUADS );
        glTexCoord2f( 0.0f, 1.0f );    glVertex3f( -2.0f, -2.0f, 0.0f );
		glTexCoord2f( 0.0f, 0.0f );    glVertex3f( -2.0f,  2.0f, 0.0f );
		glTexCoord2f( 1.0f, 0.0f );    glVertex3f(  2.0f,  2.0f, 0.0f );
        glTexCoord2f( 1.0f, 1.0f );    glVertex3f(  2.0f, -2.0f, 0.0f );
   glEnd();

   
		QueryPerformanceCounter(&stop);
		QueryPerformanceFrequency( &quantum ) ;
		xtime += (double)(stop.QuadPart - start.QuadPart)/(double)(quantum.QuadPart);
		ntime += 1;

	printf ("Sampled over %lf trials GLSL took %lf seconds per image.\n", ntime, xtime/ntime);
	printf ("CPU took %lf seconds.\n", cpuTime);
	glutSwapBuffers();
}

void keys(unsigned char key, int x, int y) 
{
	if (key == 27) exit(0);
}

char *readShader (char *name) 
{
	FILE *f;
	char *buffer = NULL, b=NULL;
	int n=0;
	int count=4096;

	if (name != NULL) 
	{
		f = fopen(name, "r");
		if (f != NULL)
		{
			buffer = (char *)malloc (count);
			n = fread (buffer, 1, count, f);	// Read a buffer full
			if (n < count) 
			{
				buffer[n] = '\0';
				return buffer;	
			}
			printf ("ERROR: Shader code file is too big!\n");
			exit (0);
		} 
		printf ("Can't open shader code file %s\n", name);
		exit (0);
	}
	printf ("File name passed to 'readShader' cannot be null.\n");
	exit(0);
}

GLuint initializeShaders() 
{
	GLuint vShader,fShader, program;
	char *vSource = NULL,*fSource = NULL;
	GLint flag;
	GLsizei siz;
	char *log;

	vShader = glCreateShader(GL_VERTEX_SHADER);
	fShader = glCreateShader(GL_FRAGMENT_SHADER);

	vSource = readShader("C:\\AIPCV\\chap10\\convolve.vert");
	printf ("Vertex shader:\n%s\n", vSource);
	fSource = readShader("C:\\AIPCV\\chap10\\convolve.frag");
	printf ("Fragment shader:\n%s\n", fSource);

//	const char * ff = fs;
//	const char * vv = vs;

	glShaderSource(vShader, 1, (const GLchar **)(&vSource), NULL);
	glShaderSource(fShader, 1, (const GLchar **)(&fSource), NULL);

	free(vSource); free(fSource);

	glCompileShader(vShader);
	glGetShaderiv (vShader, GL_COMPILE_STATUS, &flag);
	if(flag == GL_FALSE)
	{
		printf ("Vertex shader program failed to compile.\n");
		log = (char *)malloc (2048);
		glGetShaderInfoLog (vShader, 2048, &siz, log);
		printf ("LOG: %s", log);
		free(log);
		exit(0);
	}

	glCompileShader(fShader);
	glGetShaderiv (fShader, GL_COMPILE_STATUS, &flag);
	if(flag == GL_FALSE)
	{
		printf ("Fragment shader program failed to compile.\n");
		log = (char *)malloc (2048);
		glGetShaderInfoLog (fShader, 2048, &siz, log);
		printf ("LOG: %s", log);
		free (log);
		exit(0);
	}

	program = glCreateProgram();
	glAttachShader(program,fShader);
	glAttachShader(program,vShader);

	glLinkProgram(program);
	glUseProgram(program);
	return program;
}

GLuint setupTexture(char *texData, int height, int width)
{
	GLuint textn;

	glGenTextures(1, &textn);
    glBindTexture(GL_TEXTURE_2D, textn);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, height, width, 0, GL_RGB,
        GL_UNSIGNED_BYTE, (const GLvoid *) texData);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	return(textn);
}

GLubyte *getImage (char *name, int *height, int *width)
{
	int i, j;
	GLubyte *p;
	CvScalar s;
	GLubyte *targetImage;

	image = cvLoadImage(name, 1);
	Width = image->width; Height = image->height;
	cvNamedWindow("win1", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("win1", 100, 100);
	cvShowImage("win1", image );
	cvWaitKey(0);

	targetImage = (GLubyte *)malloc (image->width*image->height*4);
	p = targetImage;

	for (i = 0; i < image->height; i++) 
	{
		for (j = 0; j < image->width; j++) 
		{
			s = cvGet2D (image, i, j);
			*p++ = (GLubyte) s.val[2];
			*p++ = (GLubyte) s.val[1];
			*p++ = (GLubyte) s.val[0];
      }
   }
	*width = Width; *height = Height;
	return targetImage;
}

void doFilter ()
{
	int i, j, ii, jj, k;
	float map[3][3] = {-1, -1, -1, -1, 9, -1, -1, -1, -1};
	float sum[4], d;
	CvScalar s;
	LARGE_INTEGER start, stop, quantum;
	double xtime = 0.0;

	QueryPerformanceCounter(&start);

	for (i=1; i<image->height-1; i++)
		for (j=1; j<image->width-1; j++)
		{
			sum[0] = 0.0f; sum[1] = 0.0f; 
			sum[2] = 0.0f; 
			for (ii=-1; ii<=1; ii++)
				for (jj=-1; jj<=1; jj++)
				{
					s = cvGet2D (image, i+ii, j+jj);
					sum[0] += s.val[0]*map[ii+1][jj+1]; sum[1] += s.val[1]*map[ii+1][jj+1];
					sum[2] += s.val[2]*map[ii+1][jj+1]; 
				}
			for (ii=0; ii<3; ii++)
			{
				if (sum[ii] < 0) sum[ii] = 0;
				else if (sum[ii] > 255) sum[ii] = 255;
				s.val[ii] = sum[ii];
			}
			s.val[3] = 255;
			cvSet2D (image, i-1, j-1, s);
		}

		QueryPerformanceCounter(&stop);
		QueryPerformanceFrequency( &quantum ) ;
		xtime = (double)(stop.QuadPart - start.QuadPart)/(double)(quantum.QuadPart);
		printf ("CPU Done. Time = %lf  \n", xtime);
		cpuTime = xtime;

	cvNamedWindow("CPU", CV_WINDOW_AUTOSIZE); 
	cvMoveWindow("CPU", 100, 100);
	cvShowImage("CPU", image );
	cvWaitKey(0);
}


int main(int argc, char **argv) 
{
	int height, width;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize( wWidth, wHeight );
	glutInitWindowPosition(200,200);
	glutCreateWindow("OpenGL Shader 1");

	glutDisplayFunc(render);
	glutIdleFunc   (render);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keys);

	glewInit();
	if (!glewIsSupported("GL_VERSION_2_0"))
//	  printf(" OpenGL 2.0 is supported.\n");
//	else 
	{
		printf("OpenGL 2.0 is not supported.\n");
		exit(0);
	}

	glClearColor(0.0,0.0,0.0,1.0);
    
	texData1 = (char *)getImage ("C:\\AIPCV\\chap10\\pic3.jpg", &height, &width);
    tex1 = setupTexture(texData1, height, width);
    frameBuf = (char *)malloc (height*width*4);
    frame = setupTexture(frameBuf, height, width);

	if ( !(GLEW_ARB_vertex_shader && GLEW_ARB_fragment_shader) )
//		printf("Ready for GLSL\n");
//	else 
	{
		printf("No GLSL support\n");
		exit(1);
	}
	shaderProgram = initializeShaders ();

// Execute and time the code on the CPU.
	doFilter ();
	

	glutMainLoop();

	cvWaitKey (0);
	return 0;
}

